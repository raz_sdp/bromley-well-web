/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : carers

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2014-09-06 12:14:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `prequalifying _questions`
-- ----------------------------
DROP TABLE IF EXISTS `prequalifying _questions`;
CREATE TABLE `prequalifying _questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of prequalifying _questions
-- ----------------------------
INSERT INTO `prequalifying _questions` VALUES ('1', 'Do you have a parent or brother/sister with a physical/learning disability, mental health condition or a long term illness?', '2014-09-04 09:10:45', '2014-09-04 09:10:45');
INSERT INTO `prequalifying _questions` VALUES ('2', 'Do you help a family member with every day household chores?', '2014-09-04 09:10:56', '2014-09-04 09:10:56');
INSERT INTO `prequalifying _questions` VALUES ('3', 'Does the support you offer stop you from having time to your self?', '2014-09-04 09:11:05', '2014-09-04 09:11:05');
INSERT INTO `prequalifying _questions` VALUES ('4', 'Does your caring impact on your school attendance and achievement?', '2014-09-04 09:11:13', '2014-09-04 09:11:13');
INSERT INTO `prequalifying _questions` VALUES ('5', 'Do you often worry about the person who is unwell?', '2014-09-04 09:11:25', '2014-09-04 09:11:25');

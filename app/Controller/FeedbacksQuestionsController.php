<?php
App::uses('AppController', 'Controller');

/**
 * FeedbacksQuestions Controller
 *
 * @property FeedbacksQuestion $FeedbacksQuestion
 * @property UsersOption $UsersOption
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FeedbacksQuestionsController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->FeedbacksQuestion->recursive = 0;
        $this->set('feedbacksQuestions', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        if (!$this->FeedbacksQuestion->exists($id)) {
            throw new NotFoundException(__('Invalid feedbacks question'));
        }
        $options = array('conditions' => array('FeedbacksQuestion.' . $this->FeedbacksQuestion->primaryKey => $id));
        $this->set('feedbacksQuestion', $this->FeedbacksQuestion->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->FeedbacksQuestion->create();
            if ($this->FeedbacksQuestion->save($this->request->data)) {
                $this->Session->setFlash(__('The feedbacks question has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The feedbacks question could not be saved. Please, try again.'));
            }
        }
        $questions = $this->FeedbacksQuestion->Question->find('list');
        $feedbacks = $this->FeedbacksQuestion->Feedback->find('list');
        $this->set(compact('questions', 'feedbacks'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if (!$this->FeedbacksQuestion->exists($id)) {
            throw new NotFoundException(__('Invalid feedbacks question'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->FeedbacksQuestion->save($this->request->data)) {
                $this->Session->setFlash(__('The feedbacks question has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The feedbacks question could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('FeedbacksQuestion.' . $this->FeedbacksQuestion->primaryKey => $id));
            $this->request->data = $this->FeedbacksQuestion->find('first', $options);
        }
        $questions = $this->FeedbacksQuestion->Question->find('list');
        $feedbacks = $this->FeedbacksQuestion->Feedback->find('list');
        $this->set(compact('questions', 'feedbacks'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null)
    {
        $this->FeedbacksQuestion->id = $id;
        if (!$this->FeedbacksQuestion->exists()) {
            throw new NotFoundException(__('Invalid feedbacks question'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->FeedbacksQuestion->delete()) {
            $this->Session->setFlash(__('The feedbacks question has been deleted.'));
        } else {
            $this->Session->setFlash(__('The feedbacks question could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $this->FeedbacksQuestion->recursive = 0;
        $this->set('feedbacksQuestions', $this->Paginator->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->FeedbacksQuestion->exists($id)) {
            throw new NotFoundException(__('Invalid feedbacks question'));
        }
        $options = array('conditions' => array('FeedbacksQuestion.' . $this->FeedbacksQuestion->primaryKey => $id));
        $this->set('feedbacksQuestion', $this->FeedbacksQuestion->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->FeedbacksQuestion->create();
            if ($this->FeedbacksQuestion->save($this->request->data)) {
                $this->Session->setFlash(__('The feedbacks question has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The feedbacks question could not be saved. Please, try again.'));
            }
        }
        $questions = $this->FeedbacksQuestion->Question->find('list');
        $feedbacks = $this->FeedbacksQuestion->Feedback->find('list');
        $this->set(compact('questions', 'feedbacks'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null)
    {
        if (!$this->FeedbacksQuestion->exists($id)) {
            throw new NotFoundException(__('Invalid feedbacks question'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->FeedbacksQuestion->save($this->request->data)) {
                $this->Session->setFlash(__('The feedbacks question has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The feedbacks question could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('FeedbacksQuestion.' . $this->FeedbacksQuestion->primaryKey => $id));
            $this->request->data = $this->FeedbacksQuestion->find('first', $options);
        }
        $questions = $this->FeedbacksQuestion->Question->find('list');
        $feedbacks = $this->FeedbacksQuestion->Feedback->find('list');
        $this->set(compact('questions', 'feedbacks'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->FeedbacksQuestion->id = $id;
        if (!$this->FeedbacksQuestion->exists()) {
            throw new NotFoundException(__('Invalid feedbacks question'));
        }
        //print_r($id); exit;
        $this->request->onlyAllow('post', 'delete');
        if ($this->FeedbacksQuestion->delete()) {
            $this->Session->setFlash(__('The response has been deleted.'), 'default', array('class' => 'valid'));
        } else {
            $this->Session->setFlash(__('The response could not be deleted. Please, try again.'), 'default', array('class' => 'error'));
        }
        return $this->redirect($this->referer());
    }

    public function admin_response($category_id = 1, $question_id = 1)
    {
        $this->FeedbacksQuestion->recursive = 2;
        $this->paginate = array('all',
            'limit' => 20,
            'conditions' => array(
                //"FeedbacksQuestion.answer IS NOT NULL AND Question.is_deleted = 0 AND Question.category_id = '$category_id' AND Question.id = '$question_id'"
                "Question.is_deleted = 0 AND Question.category_id = '$category_id' AND Question.id = '$question_id'"
            ),
            'order' => array('FeedbacksQuestion.created' => 'DESC'),
            // 'fields' => array('FeedbacksQuestion.id', 'FeedbacksQuestion.question_id',
            //      'FeedbacksQuestion.feedback_id', 'FeedbacksQuestion.answer',
            //      'FeedbacksQuestion.created', 'FeedbacksQuestion.modified',
            //      'Question.id', 'Question.category_id', 'Question.question','Question.is_deleted', 'Feedback.user_id'),
        );
        $question = $this->FeedbacksQuestion->Question->find('list', array(
            'joins' => array(
                array(
                    'table' => 'feedbacks_questions',
                    'alias' => 'FeedbacksQuestion',
                    'type' => 'LEFT',
                    'conditions' => array("FeedbacksQuestion.question_id = Question.id AND Question.category_id = '$category_id'"),
                ),
            ),
            'conditions' => array('FeedbacksQuestion.answer IS NOT NULL OR FeedbacksQuestion.answer_int IS NOT NULL'),
            'order' => 'Question.created'
        ));
        //$feedbacksQuestions = $this->Paginator->paginate();
        $this->set(compact('feedbacksQuestions', 'question', 'question_id', 'category_id'));
    }

    public function admin_richter($category_id = 1, $question_id = 1)
    {
        $this->FeedbacksQuestion->recursive = 2;
         $question = $this->FeedbacksQuestion->Question->find('list',array(
         		'joins' => array(
         			array(
         				'table' => 'feedbacks_questions',
         				'alias' => 'FeedbacksQuestion',
         				'type' => 'LEFT',
         				'conditions' => array("FeedbacksQuestion.question_id = Question.id AND Question.category_id = '$category_id'"),
         			),
         		),
         		'conditions' => array('FeedbacksQuestion.answer IS NOT NULL OR FeedbacksQuestion.answer_int IS NOT NULL'),
                       'order' => 'Question.created'
         	));
        $question = $this->FeedbacksQuestion->Question->find('list', array(
            'conditions' => array("Question.category_id = '$category_id' AND Question.is_deleted = 0"),
            'order' => 'Question.created ASC'));

//        $this->loadModel('Assesment');
//
//        $question = $this->Assesment->find('list');
        //$feedbacksQuestions = $this->Paginator->paginate();
        $this->set(compact('feedbacksQuestions', 'question', 'question_id', 'category_id'));
    }

    public function admin_caring($category_id = 2, $question_id = 17)
    {
        $this->FeedbacksQuestion->recursive = 2;
        $question = $this->FeedbacksQuestion->Question->find('list', array(
            'conditions' => array("Question.category_id = '$category_id' AND Question.is_deleted = 0"),
            'order' => 'Question.created ASC'));
        // $feedbacksQuestions = $this->Paginator->paginate();
        $this->set(compact('feedbacksQuestions', 'question', 'question_id', 'category_id'));
    }

    public function admin_young($category_id = 3, $question_id = 26, $user_id = null)
    {
        Configure::write('debug', 0);
        $this->FeedbacksQuestion->recursive = 2;
        $question = $this->FeedbacksQuestion->Question->find('list', array(
            'conditions' => array("Question.category_id = '$category_id' AND Question.id NOT IN (32,33,34,35) AND Question.is_deleted = 0"),
            'order' => 'Question.created ASC'));
        //$feedbacksQuestions = $this->Paginator->paginate();
        $this->set(compact('feedbacksQuestions', 'question', 'question_id', 'category_id','user_id'));
    }

    public function admin_graph_index($user_id, $category_id = 1, $question_id = 1)
    {
        Configure::write('debug', 0);
        //call graph_data from view by ajax calling
        $option_list = [];
        if($category_id==1) {
            $this->loadModel('Assesment');
            $this->loadModel('Option');
            $questions = $this->Assesment->find('list');
            $query = [
                'conditions' => [
                    'Option.assesment_id' => $question_id
                ]
            ];
            $option_list = $this->Option->find('list', $query);
        } else {
            $questions = $this->FeedbacksQuestion->Question->find('list', array(
                'conditions' => array('Question.category_id' => $category_id, 'Question.is_deleted' => 0, 'Question.id NOT IN (32, 33, 34, 35)'),
                'order' => 'Question.created ASC'
            ));
        }
        $this->set(compact('questions', 'question_id', 'param', 'category_id','option_list'));
    }

    public function admin_users_feedback($user_id, $category_id = 1, $question_id = 1, $from = null, $to = null)
    {
        $this->autoRender = false;
        if($category_id==1) {
            $this->loadModel('Option');
            $this->loadModel('UsersOption');
            $query = [
                'fields' => ['Option.title'],
                'conditions' => [
                    'Option.assesment_id' => $question_id
                ]
            ];
            $options = $this->Option->find('all', $query);
            $option_list = Hash::extract($options, '{n}.Option.title');
            $graphData = $this->UsersOption->usersFeedback($user_id, $question_id, $from, $to);
            $answer_data = Hash::extract($graphData, '{n}.UsersOption');
            //pr($answer_data);die;

        } else {
            $graphData = $this->FeedbacksQuestion->usersFeedback($user_id, $category_id, $question_id, $from, $to);
            $answer_data = Hash::extract($graphData, '{n}.FeedbacksQuestion');
        }
        $modified_ans = [];
        #$this->_setTrace($answer_data);
        foreach ($answer_data as $key => $new_answer) {
            #$word = $this->addOrdinalNumberSuffix($key + 1);   date_format(date_create($new_answer['created']), 'd/m/Y');
            //$modified_ans[$key]['submission'] = date_format(date_create($new_answer['created'], 'd/m/Y H:i:s'));
            //print_r($new_answer['created']);
            //print_r(date_format(date_create($new_answer['created']), 'd/m/Y H:i:s'));
            $modified_ans[$key]['submission'] = date_format(date_create($new_answer['created']), 'd/m/Y H:i:s');
            if($category_id == 1){
                if ($new_answer['option_id'] == 1) {
                    $modified_ans[$key]['answer_data'] = $option_list[0];
                    $modified_ans[$key]['answer_int'] = 1;
                } elseif ($new_answer['option_id'] == 2) {
                    $modified_ans[$key]['answer_data'] = $option_list[1];
                    $modified_ans[$key]['answer_int'] = 2;
                } else {
                    $modified_ans[$key]['answer_data'] = $option_list[2];
                    $modified_ans[$key]['answer_int'] = 3;
                }
            }
        elseif($category_id == 2) {
                // if ($new_answer['answer_int'] == 1) {
                //     $modified_ans[$key]['never'] = 1;
                //     $modified_ans[$key]['some_time'] = 0;
                //     $modified_ans[$key]['lot_time'] = 0;
                // } elseif ($new_answer['answer_int'] == 2) {
                //     $modified_ans[$key]['never'] = 0;
                //     $modified_ans[$key]['some_time'] = 2;
                //     $modified_ans[$key]['lot_time'] = 0;
                // } else {
                //     $modified_ans[$key]['never'] = 0;
                //     $modified_ans[$key]['some_time'] = 0;
                //     $modified_ans[$key]['lot_time'] = 3;
                // }
                if ($new_answer['answer_int'] == 1) {
                    $modified_ans[$key]['answer_data'] = 'Never';
                    $modified_ans[$key]['answer_int'] = 1;
                } elseif ($new_answer['answer_int'] == 2) {
                    $modified_ans[$key]['answer_data'] = 'Some of the time';
                    $modified_ans[$key]['answer_int'] = 1;
                } else {
                    $modified_ans[$key]['answer_data'] = 'A lot of the time';
                    $modified_ans[$key]['answer_int'] = 1;
                }
                //$modified_ans[$key]['answer_int'] = intval($new_answer['answer_int']);
            } else {
                $modified_ans[$key]['answer_int'] = intval($new_answer['answer_int']);
            }
            //$modified_ans[$key]['answer_int'] = intval($new_answer['answer_int']);
            if (!empty($new_answer['answer_score'])) {
                $modified_ans[$key]['answer_score'] = intval($new_answer['answer_score']);
            }
            #$answer []= Set::insert($new_answer,'submission',$word.' Submission');
        }
        die(json_encode(array('answer' => $modified_ans,'option_list' => $option_list)));
    }

    public function admin_avg_feedback($category_id, $question_id, $from = null, $to = null, $user_id = null)
    {
        Configure::write('debug', 0);
        $this->autoRender = false;
        $result = array();
        if ($category_id == 2) {
            //if ($category_id == 2) {
                $graphData = $this->FeedbacksQuestion->avgFeedbackCat2($category_id, $question_id, $from, $to);
                #$this->_setTrace($graphData);
                $data = array();

                foreach ($graphData as $key => $new_answer) {

                    $month = date('F', strtotime($new_answer['FeedbacksQuestion']['created']));
                    if(empty($data[$month]))
                        $data[$month] = array();

                    if ($new_answer['FeedbacksQuestion']['answer_int'] == 1) {
                        $data[$month]['never'] = intval($new_answer[0]['answer_count']);
                        //$modified_ans[$key]['some_time'] = 0;
                        //$modified_ans[$key]['lot_time'] = 0;
                    } elseif ($new_answer['FeedbacksQuestion']['answer_int'] == 2) {
                        //$modified_ans[$key]['never'] = 0;
                        $data[$month]['some_time'] = intval($new_answer[0]['answer_count']);
                        //$modified_ans[$key]['lot_time'] = 0;
                    } else {
                        //$modified_ans[$key]['never'] = 0;
                        //$modified_ans[$key]['some_time'] = 0;
                        $data[$month]['lot_time'] = intval($new_answer[0]['answer_count']);
                    }
                }

                $modified_ans = array();
                foreach($data as $month => $val){
                    $modified_ans[] = array('month' => $month, 'never' => intval($val['never']));
                    $modified_ans[] = array('month' => $month, 'some_time' => intval($val['some_time']));
                    $modified_ans[] = array('month' => $month, 'lot_time' => intval($val['lot_time']));
                }
            //}
        } else {
            $graphData = $this->FeedbacksQuestion->avgFeedback($category_id, $question_id, $from, $to, $user_id);
            //print_r($graphData); exit;
            //$this->_setTrace($graphData,false);
           // pr($graphData);die;
             foreach ($graphData as $key => $new_answer) {
                $modified_ans[$key]['answer_int'] = floatval($new_answer[0]['answer_int']);
                $modified_ans[$key]['feedback_count'] = intval(@$new_answer[0]['feedback_count']);

                $month = date('F', strtotime($new_answer['FeedbacksQuestion']['created']));
                $modified_ans[$key]['month'] = $month;
                #$answer []= Set::insert($new_answer,'submission',$word.' Submission');
            }
        }
        //print_r($modified_ans); exit;
        #$this->_setTrace($modified_ans);
        die(json_encode(array('answer' => $modified_ans)));
    }

    public function admin_graph_data()
    {
        $this->autoRender = false;
        $graphData = $this->FeedbacksQuestion->feedbackData();
        if (!empty($graphData)) {
            //$this->_setTrace($graphData);
            $avg_result = Hash::combine($graphData, '{n}.0.month', '{n}.0.avg_result');
            //$avgData = array();
            $submission = array();
            //$months = array();

            foreach ($avg_result as $month => $val) {
                $submission[] = array('month' => $month, 'result' => intval(array_pop(Hash::extract($graphData, '{n}.0[month=' . $month . '].submission'))));

                /*if (empty($val['INVOLVEMENT']))
                    $val['INVOLVEMENT'] = "0";
                if (empty($val['ADVOCACY']))
                    $val['ADVOCACY'] = "0";
                if (empty($val['MOTIVATION']))
                    $val['MOTIVATION'] = "0";
                $avgData[] = am(array(
                    'month' => $month
                ), $val = array(
                    'INVOLVEMENT' => abs($val['INVOLVEMENT']),
                    'ADVOCACY' => abs($val['ADVOCACY']),
                    'MOTIVATION' => abs($val['MOTIVATION'])
                ));*/
            }
            die(json_encode(array('avg' => $avg_result, 'submission' => $submission)));

        } else {
            /*for ($i = 0; $i < 3; $i++) {
                $data[$i]['month'] = 0;
                $data[$i]['INVOLVEMENT'] = 0;
                $data[$i]['ADVOCACY'] = 0;
                $data[$i]['MOTIVATION'] = 0;
            }*/
            die(json_encode(array('avg' => array(), 'submission' => array())));
        }
    }

    // for Self Assesment
    public function admin_carer_response($feedback_id = null)
    {
        $this->FeedbacksQuestion->recursive = 0;
        $feedbacksQuestions = $this->FeedbacksQuestion->find('all', array(
            'recursive' => 0,
            'conditions' => array('FeedbacksQuestion.feedback_id' => $feedback_id),
            'order' => 'FeedbacksQuestion.question_id ASC'
        ));
        $category_id = $feedbacksQuestions[0]['Question']['category_id'];
        $category = $this->FeedbacksQuestion->Feedback->Category->findById($category_id, 'name');
        $user_obj = $this->FeedbacksQuestion->Feedback->User->findById($feedbacksQuestions[0]['Feedback']['user_id'], 'name');
        $this->set(compact('feedbacksQuestions', 'category_id', 'user_obj', 'category'));
    }

    // for How Caring affects me
    public function admin_carer_response_caring($feedback_id = null)
    {

        $this->FeedbacksQuestion->recursive = 0;
        $feedbacksQuestions = $this->FeedbacksQuestion->find('all', array(
            'recursive' => 0,
            'conditions' => array('FeedbacksQuestion.feedback_id' => $feedback_id),
            'order' => 'FeedbacksQuestion.question_id ASC'
        ));
        //print_r($feedbacksQuestions);
        $category_id = $feedbacksQuestions[0]['Question']['category_id'];
        $category = $this->FeedbacksQuestion->Feedback->Category->findById($category_id, 'name');
        $user_obj = $this->FeedbacksQuestion->Feedback->User->findById($feedbacksQuestions[0]['Feedback']['user_id'], 'name');
        $this->set(compact('feedbacksQuestions', 'category_id', 'user_obj', 'category'));
    }

    // for Young Carer Feedback Form
    public function admin_young_carer_response($feedback_id = null)
    {
        //$this->FeedbacksQuestion->recursive = 2;
        $feedbacksQuestions = $this->FeedbacksQuestion->find('all', array(
            'recursive' => 0,
            'conditions' => array('FeedbacksQuestion.feedback_id' => $feedback_id),
            'order' => 'FeedbacksQuestion.question_id ASC'
        ));
        //print_r($feedbacksQuestions);
        $category_id = $feedbacksQuestions[0]['Question']['category_id'];
        $category = $this->FeedbacksQuestion->Feedback->Category->findById($category_id, 'name');
        $user_obj = $this->FeedbacksQuestion->Feedback->User->findById($feedbacksQuestions[0]['Feedback']['user_id'], 'name');
        $this->set(compact('feedbacksQuestions', 'category_id', 'user_obj', 'category'));
    }

    public function admin_wordcloud_richter($question_id)
    {
        Configure::write('debug', 0);
        $word_cloud_data = $this->FeedbacksQuestion->find('all', array(
            'conditions' => array('FeedbacksQuestion.question_id' => $question_id),
            'fields' => array('FeedbacksQuestion.answer')
        ));
        //print_r($word_cloud_data); exit;
        $data = Hash::extract($word_cloud_data, '{n}.FeedbacksQuestion.answer');
        $words = array();
        foreach ($data as $d) {
            $words = am($words, explode(' ', preg_replace('/[^a-zA-Z 0-9]+/', '', $d)));
        }
        #pr($words);die;
        $preprositions = array('is', 'the', 'for', 'a', 'or', 'on', 'so', 'and', 'to', 'very',
            'it', 'in', 'at', 'before', 'about', 'on', 'with', 'without', 'above', 'before',
            'or', 'so', 'after', 'even', 'if', 'only', 'as', 'though', 'then', 'now', 'since',
            'once', 'provided', 'that', 'unless', 'this', 'til', 'until', 'when', 'who', 'where',
            'why', 'rather', 'than', 'lest', 'just', 'yet', 'without', 'within', 'via', 'upon',
            'under', 'towards', 'toward', 'over', 'off', 'onto', 'like', 'except', 'during', 'despite'
        );
        $words = array_count_values(array_diff(array_filter($words), $preprositions));
        #pr($words);die;
        $counts = array_values($words);
        $max = max($counts);
        $allWords = array();
        foreach ($words as $key => $value) {
            $allWords[] = array(
                'text' => $key,
                'size' => ($value / $max) * 100
            );
        }
        //$min = min($counts);
        //pr($min);
        //die(json_encode($words));
        $questions = $this->FeedbacksQuestion->Question->find('list', array('conditions' => array('Question.category_id' => 1), 'order' => 'Question.created ASC'));
        $this->set(compact('allWords', 'questions', 'question_id'));
    }


    public function admin_wordcloud_young($question_id)
    {
        Configure::write('debug', 0);
        $word_cloud_data = $this->FeedbacksQuestion->find('all', array(
            'conditions' => array('FeedbacksQuestion.question_id' => $question_id),
            'fields' => array('FeedbacksQuestion.answer')
        ));
        //print_r($word_cloud_data); exit;
        $data = Hash::extract($word_cloud_data, '{n}.FeedbacksQuestion.answer');
        $words = array();
        foreach ($data as $d) {
            $words = am($words, explode(' ', preg_replace('/[^a-zA-Z 0-9]+/', '', $d)));
        }
        #pr($words);die;
        $preprositions = array('is', 'the', 'for', 'a', 'or', 'on', 'so', 'and', 'to', 'very',
            'it', 'in', 'at', 'before', 'about', 'on', 'with', 'without', 'above', 'before',
            'or', 'so', 'after', 'even', 'if', 'only', 'as', 'though', 'then', 'now', 'since',
            'once', 'provided', 'that', 'unless', 'this', 'til', 'until', 'when', 'who', 'where',
            'why', 'rather', 'than', 'lest', 'just', 'yet', 'without', 'within', 'via', 'upon',
            'under', 'towards', 'toward', 'over', 'off', 'onto', 'like', 'except', 'during', 'despite'
        );
        $words = array_count_values(array_diff(array_filter($words), $preprositions));
        #pr($words);die;
        $counts = array_values($words);
        $max = max($counts);
        $allWords = array();
        foreach ($words as $key => $value) {
            $allWords[] = array(
                'text' => $key,
                'size' => ($value / $max) * 100
            );
        }
        //$min = min($counts);
        //pr($min);
        //die(json_encode($words));
        $questions = $this->FeedbacksQuestion->Question->find('list', array('conditions' => array('Question.category_id' => 3, 'Question.ans_type' => 'text'), 'order' => 'Question.created ASC'));
        $this->set(compact('allWords', 'questions', 'question_id'));
    }

}

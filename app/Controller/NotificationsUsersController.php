<?php
App::uses('AppController', 'Controller');

/**
 * NotificationsUsers Controller
 *
 * @property NotificationsUser $NotificationsUser
 * @property Category $Category
 * @property Document $Document
 * @property Information $Information
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class NotificationsUsersController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->NotificationsUser->recursive = 0;
        $this->set('notificationsUsers', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        if (!$this->NotificationsUser->exists($id)) {
            throw new NotFoundException(__('Invalid notifications user'));
        }
        $options = array('conditions' => array('NotificationsUser.' . $this->NotificationsUser->primaryKey => $id));
        $this->set('notificationsUser', $this->NotificationsUser->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->NotificationsUser->create();
            if ($this->NotificationsUser->save($this->request->data)) {
                $this->Session->setFlash(__('The notifications user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The notifications user could not be saved. Please, try again.'));
            }
        }
        $users = $this->NotificationsUser->User->find('list');
        $notifications = $this->NotificationsUser->Notification->find('list');
        $this->set(compact('users', 'notifications'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if (!$this->NotificationsUser->exists($id)) {
            throw new NotFoundException(__('Invalid notifications user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->NotificationsUser->save($this->request->data)) {
                $this->Session->setFlash(__('The notifications user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The notifications user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('NotificationsUser.' . $this->NotificationsUser->primaryKey => $id));
            $this->request->data = $this->NotificationsUser->find('first', $options);
        }
        $users = $this->NotificationsUser->User->find('list');
        $notifications = $this->NotificationsUser->Notification->find('list');
        $this->set(compact('users', 'notifications'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null)
    {
        $this->NotificationsUser->id = $id;
        if (!$this->NotificationsUser->exists()) {
            throw new NotFoundException(__('Invalid notifications user'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->NotificationsUser->delete()) {
            $this->Session->setFlash(__('The notifications user has been deleted.'));
        } else {
            $this->Session->setFlash(__('The notifications user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $this->NotificationsUser->recursive = 0;
        $this->paginate = array('all',
            'limit' => 20,
            'conditions' => array(
                'NotificationsUser.is_deleted' => 0,
            ),
            'order' => array('NotificationsUser.created' => 'DESC'),
        );
        $this->set('notificationsUsers', $this->Paginator->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->NotificationsUser->exists($id)) {
            throw new NotFoundException(__('Invalid notifications user'));
        }
        $options = array('conditions' => array('NotificationsUser.' . $this->NotificationsUser->primaryKey => $id));
        $this->set('notificationsUser', $this->NotificationsUser->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->NotificationsUser->create();
            if ($this->NotificationsUser->save($this->request->data)) {
                $this->Session->setFlash(__('The notifications user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The notifications user could not be saved. Please, try again.'));
            }
        }
        $users = $this->NotificationsUser->User->find('list');
        $notifications = $this->NotificationsUser->Notification->find('list');
        $this->set(compact('users', 'notifications'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null)
    {
        if (!$this->NotificationsUser->exists($id)) {
            throw new NotFoundException(__('Invalid notifications user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->NotificationsUser->save($this->request->data)) {
                $this->Session->setFlash(__('The notifications user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The notifications user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('NotificationsUser.' . $this->NotificationsUser->primaryKey => $id));
            $this->request->data = $this->NotificationsUser->find('first', $options);
        }
        $users = $this->NotificationsUser->User->find('list');
        $notifications = $this->NotificationsUser->Notification->find('list');
        $this->set(compact('users', 'notifications'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->NotificationsUser->id = $id;
        if (!$this->NotificationsUser->exists()) {
            throw new NotFoundException(__('Invalid notifications user'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->NotificationsUser->delete()) {
            $this->Session->setFlash(__('The notifications user has been deleted.'));
        } else {
            $this->Session->setFlash(__('The notifications user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function g($user_id, $download_time = null)
    {
        //Configure::write('debug', 2);
        header('Content-Type: application/json');
        $this->loadModel('Category');
        $this->loadModel('Document');
        $this->loadModel('Information');
        $conditions = array();
        $this->request->data = date("Y-m-d H:i:s");
        if (!empty($download_time)) {
            $download_time = date("Y-m-d H:i:s", $download_time);
        } else {
            $download_time = "1970-01-01 00:00:00";
            $conditions = array('is_deleted' => 0);
        }
        $conditions = am($conditions, array('modified >=' => $download_time));
        $category_data = $this->Category->getCategory($conditions);
        $document_data = $this->Document->getDocument($conditions);
        $information_data = $this->Information->getInformation($download_time);         //causes of absent is_deleted
        //$notification_data = $this->NotificationsUser->getNotification($user_id, $download_time);
        $notification_data = $this->NotificationsUser->Notification->getNotification($user_id, $download_time);
        //print_r($notification_data); exit;
        #$this->_setTrace($information_data);        
        $this->set(compact('category_data', 'document_data', 'information_data', 'notification_data'));
    }

    public function notification_delete(){
        $this->autoRender = false; 
        $this->NotificationsUser->recursive = -1;
        if($this->request->is('post')){ 
            $user_check = $this->NotificationsUser->User->findByIdAndCarerid($this->request->data['NotificationsUser']['user_id'],$this->request->data['NotificationsUser']['carerid'], array('id'));
           if(!empty($user_check)){
                $notification_check = $this->NotificationsUser->findByUserIdAndNotificationId($this->request->data['NotificationsUser']['user_id'], $this->request->data['NotificationsUser']['notification_id'], array('id'));
                if(!empty($notification_check)) {
                    $this->NotificationsUser->id = $notification_check['NotificationsUser']['id'];
                    $data['NotificationsUser']['is_deleted'] = 1;
                    //if($this->NotificationsUser->delete()){
                } else{
                    $data['NotificationsUser']['user_id'] = $this->request->data['NotificationsUser']['user_id'];
                    $data['NotificationsUser']['notification_id'] = $this->request->data['NotificationsUser']['notification_id'];
                    $data['NotificationsUser']['is_deleted'] = 1;
                    $this->NotificationsUser->create();
                }
                if($this->NotificationsUser->save($data)){
                    echo json_encode(array('success' => true, 'msg' =>'Successfully Deleted'));
                } else echo json_encode(array('success' => false, 'msg' => 'Notification could not be deleted. Please, try again'));                
            }else echo json_encode(array('success' => false,'msg' => 'Invalid User.'));
        } else echo json_encode(array('success' => false,'msg' => 'Invalid Request.'));
    }

    public function notification_read(){
        $this->autoRender = false; 
        $this->NotificationsUser->recursive = -1;
        if($this->request->is('post')){ 
            $user_check = $this->NotificationsUser->User->findByIdAndCarerid($this->request->data['NotificationsUser']['user_id'],$this->request->data['NotificationsUser']['carerid'], array('id'));
           if(!empty($user_check)){
                $notification_check = $this->NotificationsUser->findByUserIdAndNotificationId($this->request->data['NotificationsUser']['user_id'],$this->request->data['NotificationsUser']['notification_id'], array('id'));
                if(!empty($notification_check)) {
                    $this->NotificationsUser->id = $notification_check['NotificationsUser']['id'];
                    $data['NotificationsUser']['is_read'] = 1;
                    //if($this->NotificationsUser->delete()){
                } else{
                    $data['NotificationsUser']['user_id'] = $this->request->data['NotificationsUser']['user_id'];
                    $data['NotificationsUser']['notification_id'] = $this->request->data['NotificationsUser']['notification_id'];
                    $data['NotificationsUser']['is_read'] = 1;
                    $this->NotificationsUser->create();
                }
                    if($this->NotificationsUser->save($data)){
                        echo json_encode(array('success' => true));
                    } else echo json_encode(array('success' => false, 'msg' => 'Notification could not be deleted. Please, try again'));
            }else echo json_encode(array('success' => false,'msg' => 'Invalid User.'));
        } else echo json_encode(array('success' => false,'msg' => 'Invalid Request.'));
    }
}
<?php
App::uses('AppController', 'Controller');
/**
 * Documents Controller
 *
 * @property Document $Document
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class DocumentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Document->recursive = 0;
		$this->set('documents', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
		$this->set('document', $this->Document->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Document->create();
			if ($this->Document->save($this->request->data)) {
				$this->Session->setFlash(__('The document has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		}
		$users = $this->Document->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Document->save($this->request->data)) {
				$this->Session->setFlash(__('The document has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
			$this->request->data = $this->Document->find('first', $options);
		}
		$users = $this->Document->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Document->id = $id;
		if (!$this->Document->exists()) {
			throw new NotFoundException(__('Invalid document'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Document->delete()) {
			$this->Session->setFlash(__('The document has been deleted.'));
		} else {
			$this->Session->setFlash(__('The document could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Document->recursive = 0;
		$this->paginate = array('all',
			'limit' => 10,
			'conditions' => array(
				'Document.is_deleted' => 0,
			),
			'order' => array('Document.created' => 'DESC'),
		);
		$this->set('documents', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
		$this->set('document', $this->Document->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if(!empty($this->request->data['Document']['file_upload'])){
				$this->autoRender = false;
				$image_upload_user = $this->_upload($this->request->data['Document']['filex'], 'documents');	
				echo json_encode(array('filename' => $image_upload_user));
				exit;
			}
            $this->request->data['Document']['user_id'] = $this->Auth->user('id');
            // $image_upload_user = $this->_upload($this->request->data['Document']['filex'], 'documents');
            // if (!empty($image_upload_user)) {
            //     $this->request->data['Document']['filename'] = $image_upload_user;
            //     $this->request->data['Document']['title'] = $this->request->data['Document']['title'];
            // }
            $this->Document->create();
			if ($this->Document->save($this->request->data)) {
				$this->Session->setFlash(__('The document has been saved.'), 'default', array('class' => 'valid'));
				return $this->redirect(array('action' => 'index', 'document'));
			} else {
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'), 'default', array('class' => 'error'));
			}
		}
		$users = $this->Document->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($param, $id = null) {
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Document->save($this->request->data)) {
				$this->Session->setFlash(__('The document has been saved.'), 'default', array('class' => 'valid'));
				return $this->redirect(array('action' => 'index', 'document'));
			} else {
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
			$this->request->data = $this->Document->find('first', $options);
		}
		$users = $this->Document->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Document->id = $id;
		if (!$this->Document->exists()) {
			throw new NotFoundException(__('Invalid document'));
		}
		$file_obj = $this->Document->findById($id, array('filename'));
		//print_r($file_obj['Document']['filename']); exit;
		@unlink(WWW_ROOT . 'files' . DS . 'documents' . DS . $file_obj['Document']['filename']);
		//$this->request->onlyAllow('post', 'delete');
		//if ($this->Document->delete()) {
		$data['Document']['is_deleted'] = 1;
		if ($this->Document->save($data)){
			$this->Session->setFlash(__('The document has been deleted.'), 'default', array('class' => 'valid'));
		} else {
			$this->Session->setFlash(__('The document could not be deleted. Please, try again.'), 'default', array('class' => 'error'));
		}
		return $this->redirect(array('action' => 'index', 'document'));
	}

    //upload file

    // private function _upload($file){
    //     //App::import('Vendor', 'phpthumb', array('file' => 'phpthumb' . DS . 'phpthumb.class.php'));

    //     if(is_uploaded_file($file['tmp_name'])){

    //         $ext  = strtolower(array_pop(explode('.',$file['name'])));
    //         $fileName = time() . rand(1,999) . '.' .$ext;
    //         if($ext == 'pdf'){

    //             $uploadFile = WWW_ROOT.'files'.DS.'documents'. DS .$fileName;
    //             if(move_uploaded_file($file['tmp_name'],$uploadFile)){
    //                 return $fileName;
    //             }
    //         }
    //     }
    // }
}

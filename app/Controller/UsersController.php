<?php
App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            $this->Session->setFlash(__('The user has been deleted.'));
        } else {
            $this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        Configure::write('debug', 0);
        $this->User->recursive = 0;
//		$this->set('users', $this->Paginator->paginate());

        $keyword = $this->request->params['named']['keyword'];

        $conditions = array(
            'User.role' => 'carer',
        );

        if (!empty($keyword))
            $conditions = am($conditions, array('User.name LIKE' => '%' . $keyword . '%'));

        $this->paginate = array(
            'conditions' => $conditions
        );

        $this->set('users', $this->Paginator->paginate());
    }

    public function admin_user()
    {
        $this->User->recursive = 0;
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'User.role' => 'admin'
            )
        );
        $this->set('users', $this->Paginator->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($param, $id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            //$this->request->data['User']['password'] = '17d3e0efe2fac09112646b1b857284b66b225210';
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'), 'default', array('class' => 'valid'));
                if ($this->request->data['User']['role'] == 'admin') {
                    return $this->redirect(array('action' => 'user', 'settings'));
                } else return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'error'));
            }
        }
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($param, $id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if (empty($this->request->data['User']['passwordx'])) {
                unset($this->request->data['User']['passwordx']);
            } else {
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['passwordx']);
            }
            //print_r($this->request->data['User']['role']); exit;
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'), 'default', array('class' => 'valid'));
                if ($this->request->data['User']['role'] == 'admin') {
                    return $this->redirect(array('action' => 'user', 'settings'));
                } else return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'error'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            $this->User->query('DELETE FROM notifications_users WHERE user_id = \'' . $id . ' \'
			');
            // $feedback_obj = $this->User->Feedback->findByUserId($id, array('Feedback.id'));
            // $feedback_id = $feedback_obj['Feedback']['id'];
            // $this->User->query('DELETE FROM feedbacks WHERE user_id = \'' . $id . ' \'
            // ');
            // $this->User->query('DELETE FROM feedbacks_questions WHERE feedback_id = \'' . $feedback_id . ' \'
            // ');
            $this->Session->setFlash(__('The user has been deleted.'), 'default', array('class' => 'valid'));
        } else {
            $this->Session->setFlash(__('The user could not be deleted. Please, try again.'), 'default', array('class' => 'error'));
        }
        return $this->redirect($this->referer());
    }

    /**
     * users logIn
     */
    public function login()
    {
        $this->autoRender = false;
        $this->recursive = -1;
        if ($this->request->is('post')) {
            $password = AuthComponent::password($this->request->data['User']['password']); //hashing
            $is_present = $this->User->find('first', array(
                    'conditions' => array('User.name' => $this->request->data['User']['name'], 'User.password' => $password, 'User.role' => 'carer')
                )
            );
            if (!empty($is_present)) {
                if (!empty($this->request->data['DeviceToken']['device_token'])) {
                    $this->_deviceToken(
                        $is_present['User']['id'],
                        $this->request->data['DeviceToken']['device_token'],
                        $this->request->data['DeviceToken']['device_type'],
                        $this->request->data['DeviceToken']['stage']
                    );
                }

                unset($is_present['User']['created'], $is_present['User']['modified'], $is_present['User']['password'], $is_present['User']['role']);
                echo json_encode(array(
                        'success' => true,
                        'login' => $is_present['User'],
                    )
                );
            } else echo json_encode(array('success' => false, 'msg' => 'Error Username or Password'));
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Invalid Request'));
            exit;
        }
    }

    public function register()
    {
        $this->autoRender = false;
        $this->User->recursive = -1;
        if ($this->request->is('post')) {

            $mobile = $this->request->data['User']['mobile'];
            $pattern = "/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/";
            $match = preg_match($pattern,$mobile);

            if ($match) {
                $is_present = $this->User->findByName($this->request->data['User']['name'], 'User.name');
                if (empty($is_present)) {
                    $carer_id_check = $this->User->findByCarerid($this->request->data['User']['carerid'], 'User.carerid');
                    if (empty($carer_id_check)) {
                        $this->request->data['User']['is_enabled'] = 0;
                        $this->request->data['User']['role'] = 'carer';
                        $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
                        $this->User->create();
                        if ($this->User->save($this->request->data)) {
                            $id = $this->User->id;
                            $this->_deviceToken(
                                $id,
                                $this->request->data['DeviceToken']['device_token'],
                                $this->request->data['DeviceToken']['device_type'],
                                $this->request->data['DeviceToken']['stage']
                            );
                            $user = array(
                                'id' => $this->User->id,
                                'name' => $this->request->data['User']['name'],
                                'mobile' => $this->request->data['User']['mobile'],
                                'carerid' => $this->request->data['User']['carerid'],
                            );
                            echo json_encode(array(
                                    'success' => true,
                                    'user' => $user,
                                )
                            );
                        }
                    } else {
                        echo json_encode(array('success' => false, 'msg' => 'Carer ID already registered, please try again or login with your existing details.'));
                    }
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'The username you have selected is already in use, please choose another'));
                }
            } else {
                echo json_encode(array('success' => false, 'msg' => 'Invalid Mobile Phone Number, please try again.'));
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Invalid request'));
        }
    }

    private function _deviceToken($id, $device_token, $device_type, $stage)
    {
        $this->loadModel('DeviceToken');
        $device_check = $this->DeviceToken->findByDeviceToken($device_token, array('id'));
        if (!empty($device_check)) {
            $this->DeviceToken->id = $device_check['DeviceToken']['id'];
            $this->DeviceToken->save(array('DeviceToken' => array(
                'user_id' => $id,
                'device_token' => $device_token,
                'device_type' => $device_type,
                'stage' => $stage,
            )));
        } else {
            $this->DeviceToken->create();
            $this->DeviceToken->save(array('DeviceToken' => array(
                'user_id' => $id,
                'device_token' => $device_token,
                'device_type' => $device_type,
                'stage' => $stage,
            )));
        }
    }

    //admin log In
    public function admin_login()
    {
        if ($this->request->is('post')) {
            //print_r(AuthComponent::password($this->request->data['User']['password']));exit;
            if ($this->Auth->login() && $this->Auth->user('role') == 'admin') {
                // logged in
                // did they select the remember me checkbox?
                if ($this->request->data['User']['remember_me'] == 1) {
                    // remove "remember me checkbox"
                    unset($this->request->data['User']['remember_me']);
                    // hash the user's password
                    $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
                    // write the cookie
                    $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
                }
                //if($this->Auth->login() && $this->Auth->User('role') == 'admin'){
                if ($this->request->is('ajax')) {
                    $this->autoRender = false;
                    echo json_encode(array('success' => true, 'user' => $this->Auth->user()));
                } else {
                    //$this->redirect($this->Auth->redirect());
                    $this->redirect(array('action' => 'dashboard', 'admin' => true));
                }
            } else {
                $this->Auth->logout();
                if ($this->request->is('ajax')) {
                    $this->autoRender = false;
                    echo json_encode(array('success' => false));
                } else {
                    $this->Session->setFlash(__('Incorrect username or password, please try again.'), 'default', array('class' => 'error'));
                }
            }
        }
    }

    //admin logout
    public function admin_logout()
    {
        // clear the cookie (if it exists) when logging out
        $this->Cookie->delete('remember_me_cookie');
        $this->redirect($this->Auth->logout());
    }

    public function admin_dashboard()
    {
        $carer = $this->User->find('count', array('conditions' => array('User.role' => 'carer')));
        $thoughts = $this->User->Thought->find('count');
        $document = $this->User->Document->find('count', array(
            'conditions' => array('Document.is_deleted' => 0)
        ));
        $notification = $this->User->Notification->find('count', array(
            'conditions' => array('Notification.is_deleted' => 0)
        ));
        // $complaint = $this->User->Complaint->find('count');
        $self_assessment = $this->User->Feedback->find('count', array('conditions' => array('Feedback.category_id' => '1')));
        $caring = $this->User->Feedback->find('count', array('conditions' => array('Feedback.category_id' => '2')));
        $young = $this->User->Feedback->find('count', array('conditions' => array('Feedback.category_id' => '3')));

        $this->set(compact('carer', 'complaint', 'document', 'notification', 'self_assessment', 'caring', 'young','thoughts'));

    }

    //enable new driver
    public function admin_enable_carer($id = null)
    {
        $this->autoRender = false;
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid User'));
        }
        $user = $this->User->findById($id, array('name'));
        $this->request->data['User']['is_enabled'] = 1;
        if ($this->User->save($this->request->data, false)) {
            $this->Session->setFlash(__('The carer has been enabled.'), 'default', array('class' => 'valid'));
            $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The carer could not be enabled. Please, try again.'), 'default', array('class' => 'error'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    //disable any user
    public function admin_disable_carer($id = null)
    {
        $this->autoRender = false;
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid User'));
        }
        $user = $this->User->findById($id, array('name'));
        $this->request->data['User']['is_enabled'] = 0;
        if ($this->User->save($this->request->data, false)) {
            $this->Session->setFlash(__('The carer has been disabled.'), 'default', array('class' => 'valid'));
            $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The carer could not be disabled. Please, try again.'), 'default', array('class' => 'error'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function check_enability($id, $carerid)
    {
        $this->autoRender = false;
        $this->User->recursive = -1;
        $is_present = $this->User->findByIdAndCarerid($id, $carerid, array('User.id', 'User.is_enabled'));
        //print_r($is_present['User']['is_enabled']); exit;
        if (!empty($is_present)) {
            if ($is_present['User']['is_enabled'] == 1) {
                die(json_encode(array('success' => true)));
            } else die(json_encode(array('success' => false, 'msg' => 'Sorry, your account is being verified.')));
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Invalid Carer'));
        }
    }

    //forgot password for CMS
    public function forgot_password()
    {
        if ($this->request->is('post')) {
            $mail = $this->request->data['User']['email'];
            $user = $this->User->findByEmail($mail);
            if (!empty($user)) {
                $key = Security::hash(String::uuid(), 'sha1', true);
                $data = array(
                    'id' => $user['User']['id'],
                    'forgot_password_token' => $key
                );
                $this->User->save($data);
                App::uses('CakeEmail', 'Network/Email');
                $email = new CakeEmail('smtp');
                $email->to($mail);
                $email->emailFormat('html');
                $email->subject('Password reset instructions');
                $body = 'http://' . env('SERVER_NAME') . '/users/reset_password/' . $key;
                if ($email->send($body)) {
                    $this->Session->setFlash(__('Please check your email for reset instructions.'), 'default', array('class' => 'valid'));
                } else {
                    $this->Session->setFlash(__('Something went wrong with activation mail. Please try later.'), 'default', array('class' => 'error'));
                }
            } else {
                $this->Session->setFlash(__('Invalid Email Id. No User Found'), 'default', array('class' => 'error'));
            }
        }
    }

    public function reset_password($resetkey = null)
    {
        if ($this->request->is('post')) {
            $resetkey = $this->params['reset_key'];
            if(!empty($resetkey)) {
                $this->User->recursive = -1;
                $user = $this->User->findByForgotPasswordToken($resetkey);
                $new_password = $this->request->data['User']['new_password'];
                $password_confirm = $this->request->data['User']['confirm_password'];
                if ($user['User']['forgot_password_token'] == $resetkey) {
                    if (($new_password == $password_confirm)) {
                        $data = array(
                            'User' => array(
                                'password' => AuthComponent::password($new_password),
                                'forgot_password_token' => null
                            )
                        );
                        $this->User->id = $user['User']['id'];
                        if ($this->User->save($data, false)) {
                            //$message = __('Your password has been reset');
                            $this->Session->setFlash(__('Password has been reset successfully'), 'default', array('class' => 'alert alert-success text-center'));
                            //$this->redirect(array('action' => 'reset_password', 'success'));
                        }
                    } else {
                        $this->Session->setFlash(__('Password Mismatched. Try again.'), 'default', array('class' => 'alert alert-danger text-center'));
                    }
                } else {
                    $this->Session->setFlash(__('Invalid Request.'), 'default', array('class' => 'alert alert-danger text-center'));
                }
            } else {
                $this->Session->setFlash(__('Invalid Request'), 'default', array('class' => 'alert alert-danger text-center'));
            }
        }
    }

    public function test()
    {
        $this->autoRender = false;
        $this->_push('test');
    }

    public function sendEmail()
    {
        $this->autoRender = false;
        App::import('Vendor', 'ses', array('file' => 'ses' . DS . 'ses.php'));
        $ses = new SimpleEmailService('AKIAJULLR4UQDRFARJ5A', 'nDwpsXh+UN77ivTYUgDvzo1crHdXlFi+zqhepRwZ');
        $this->loadModel('PrequalifyingQuestion');
        $question_list = $this->PrequalifyingQuestion->find('all');

        $questionAnswers = "";
        foreach ($question_list as $key => $data) {
            $answer = $this->request->data[$key]['answer_score'];
            $question = $data['PrequalifyingQuestion']['question'];
            if ($answer == 1) {
                $img_yes = 'http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/radio-active.png';
            } else {
                $img_yes = 'http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/radio.png';
            }
            if ($answer == 2) {
                $img_no = 'http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/radio-active.png';
            } else {
                $img_no = 'http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/radio.png';
            }

            $questionAnswers .= "
            <tr>
		        <td align='left' valign='top' bgcolor='#fff'>
		            <table border='0' cellpadding='10' cellspacing='0'  id='emailContainer'>
		                <tr>
		                    <td align='left' valign='top' style='padding:10px;''>
		                        <table border='0' cellpadding='20' cellspacing='0' width='100%'' id='emailHeader'>
		                            <tr>
		                                <td align='left' valign='middle'>
										<img src='http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/icon1.png'  style='vertical-align: middle;' alt=''/>" .
                $question
                . "</td>
		                            </tr>
		                        </table>
		                    </td>
		                </tr>
		            </table>
		        </td>
		    </tr>
			<tr>
			    <td align='center' valign='top' bgcolor='#fff'>
			        <table border='0' cellpadding='10' cellspacing='0'  id='emailContainer'>
			                <tr>
			                     <td align='center' valign='top' width='10%'>
				                      <div class='radio radio-align'>
									  <label>
									  	 Yes
									  	 <label></label>
									  	  <br>
									    	<img src='" . $img_yes . "' style='margin-top:10px'  alt=''/>
									  </label>
									</div>
				                    </td>
			                    <td align='center' valign='top'  width='10%'>
			                      <div class='radio radio-align'>
								  <label>
								  	No 
			                        <label></label>
								  	  <br>
								  	    <img src='" . $img_no . "' style='margin-top:10px'  alt=''/>
								  </label>
								</div>
			                    </td>
			                </tr>
			            </table>
			        </td>
			    </tr>
		    ";
        }
        $name = $this->request->data['name'];
        $mobile = $this->request->data['mobile'];
        $email = $this->request->data['email'];
        $info = $this->request->data['info'];
        $location = $this->request->data['to_email'];
        $array_key_city = ["0", "1", "2", "3"];
        $location_array = array('Nottingham City', 'Rushcliffe', 'Gedling', 'Broxtowe');
        if (in_array($location, $array_key_city)) {
            $location_text = $location_array[$location];
        } else {
            $location_text = $this->request->data['other_city'];
        }
        $staticAnswer = "
    	<tr>
            <td style='padding:10px' align='' valign='top' bgcolor='#fff'>
                <table border='0' cellpadding='10' cellspacing='0'  id='emailContainer'>
                    <tr>
                        <td align='' valign='top'>
                            <table border='0' cellpadding='20' cellspacing='0' width='100%'' id='emailHeader'>
                                <tr>
                                    <td style='font-weight: bold;' align='' valign='top'>Name</td>
                                    <td>$name</td>
                                <tr>
                                <tr>
                                    <td style='font-weight: bold;' align='' valign='top'>Mobile No</td>
                                    <td>$mobile</td>
                                </tr>
                                <tr>
                                    <td style='font-weight: bold;' align='' valign='top'>Email</td>
                                    <td>$email</td>
                                </tr>
                                <tr>
                                    <td style='font-weight: bold;' align='' valign='top'>Location</td>
                                    <td>$location_text</td>
                                </tr>
                                <tr>
                                    <td style='font-weight: bold;' align='' valign='top'>Aditional Information</td>
                                </tr>
                                <tr>
                                    <td>" . nl2br($info) . "</td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>";

        $m = new SimpleEmailServiceMessage();
        $m->addTo('r.lynch@carersfederation.co.uk');
        $m->setFrom('r.lynch@carersfederation.co.uk');
        $m->setSubject('Young Carers Pre-Qualifying Questionaire');

        $body = "<!DOCTYPE html>
<html>
<head>
<meta charset='UTF-8'>
<meta name='viewport' content='width=device-width, initial-scale=1'>
	<title></title>
</head>
<body>
        <table border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='bodyTable'>
    <tr>
        <td align='center' valign='top' bgcolor='#fe580d'>
            <table border='0' cellpadding='10' cellspacing='0'  id='emailContainer'>
                <tr>
                    <td align='center' valign='top'>
                        <table border='0' cellpadding='20' cellspacing='0' width='100%' id='emailHeader'>
                            <tr>
                                <td align='center' valign='top'>
                                   <h3><font color='#fff'>Are you a Young Carer?</h3>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>"
            . $questionAnswers . $staticAnswer . "
                </table>
                </body>
                </html>
                ";

        $plainTextBody = '';

        $m->setMessageFromString($plainTextBody, $body);
        //$ses->sendEmail($m);
        try {
            if ($ses->sendEmail($m)) die(json_encode(array('success' => true, 'msg' => 'Thank you!')));
        } catch (Exception $e) {

        }
        //print_r($ses->sendEmail($m));

        // print_r($content); exit;
        //    $this->autoRender = false;
        //    App::uses('CakeEmail', 'Network/Email');
        //    $subject = 'PreQualifying Questionaire';
        //    $this->loadModel('PrequalifyingQuestion');
        //    $question_list = $this->PrequalifyingQuestion->find('all');
        //    // if(empty($viewVars)){
        //    //   $viewVars['title'] = $subject;
        //    // }
        //    //$to = "aycnottingham@carersfederation.co.uk";
        //    $to = "justin@appinstitute.co.uk";
        // $viewVars['body'] = $this->request->data;
        //    $viewVars['questions'] = $question_list;
        //    $email = new CakeEmail('smtp');
        //    $email->template('default', 'default');
        //    $email->viewVars($viewVars);
        //    $email->emailFormat('both');
        //    $email->subject($subject);
        //    $email->to($to);
        //    try{
        //        if($email->send()) die(json_encode(array('success' => true, 'msg' => 'Thank you!')));
        //    } catch(Exception $e){

        //    }

    }

    /**
     * For Mobile apps
     */
    public function forget_password()
    {
        $this->autoRender = false;
        $this->autoLayout = false;
        $to = $this->request->data['User']['email'];
        $is_user = $this->User->findByEmail($to);

        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail();
        $email->config('smtp');
        $email->emailFormat('html');
        $email->subject("Bromley Well - Password reset instructions!");
        $email->to($to);

        if (!empty($is_user)) {
            $new_passkey = $this->_generate_password(9);
            $this->User->id = $is_user['User']['id'];
            $this->User->saveField('forgot_password_token', $new_passkey);
            try {
                $url = Router::url('/reset-password/'.$new_passkey, true);
                //die($url);
                $body = '<!DOCTYPE html>
                <html>
                <head>
                </head>

                <body style="font-family: Arial; font-size: 12px;">
                <div>
                Hi ' . @$is_user['User']['fullname'] .
                '<p>
                    You have requested a password reset, please follow the link below to reset your password.
                </p>
                <p>
                    <a href="'.$url.'">
                                Follow this link to reset your password.
                            </a>
                        </p>
                        <p>
                            Please ignore this email if you did not request a password change.
                        </p>
                        <br/>Regards,<br/><br/>The Bromley Well App Team
                    </div>
                    </body>
                    </html>';
            $email->send($body);

            } catch (Exception $e) {
                //echo($e);
                die(json_encode(array('success' => false, 'msg' => 'Incorrect email address.')));
            }
        }else {
            try {
                //die($url);
                $body = '<!DOCTYPE html>
                <html>
                <head>
                </head>

                <body style="font-family: Arial; font-size: 12px;">
                <div>
                Dear App User,<p>
                            This email is not registered , please use a registered email to reset your password.
                        </p>

                        <br/>Regards,<br/><br/>The Bromley Well App Team
                    </div>
                    </body>
                    </html>';
                $email->send($body);

            } catch (Exception $e) {
                //echo($e);
                die(json_encode(array('success' => false, 'msg' => 'Incorrect email address.')));
            }
        }
        die(json_encode(array('success' => true, 'msg' => 'Password reset link sent to your email.')));
    }

    private function _generate_password($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];

        $password = str_shuffle($password);

        if (!$add_dashes)
            return $password;

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }




}

<?php
App::uses('AppController', 'Controller');
/**
 * Feedbacks Controller
 *
 * @property Feedback $Feedback
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FeedbacksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function feedback_submission($user_id = null){
		$this->autoRender = false;
		// $this->request->data = array(
		//  	array(
		// 		'qustion_id' => 1,
		// 		'category_id' => 1,
		// 		'answer' => 'sdfg',
		// 		'answer_score' => 1,
		// 		'answer_cScore' => 5					
		// 		),
		//  array(
		// 		'qustion_id' => 20,
		// 		'category_id' => 2,
		// 		'answer' => '',
		// 		'answer_score' => 2,
		// 		'answer_cScore' => ''					
		// 	),
		// 'video' => 'xgfdf.mp4'			
		// 	);
		if($this->request->is('post')) {
		$this->request->data['Feedback']['video'] = $this->_upload($this->request->data['video'], 'videos');
			$feedback_data = array('Feedback' => array(
		                'user_id' => $user_id,
		                'video' => $this->request->data['Feedback']['video'],
		                'category_id' => $this->request->data['category_id']
	            	));
			$this->Feedback->create();
			$this->Feedback->save($feedback_data);
			$this->loadModel('FeedbacksQuestion');
		 	unset($this->request->data['video'], $this->request->data['category_id'], $this->request->data['Feedback']);
		 	foreach ($this->request->data as $key => $value) {
				$feedback_question_data = array(
					'FeedbacksQuestion' => array(
						'feedback_id' => $this->Feedback->id,
		                'question_id' => $value['qustion_id'],
		                'answer' => $value['answer'],
		                'answer_int' =>$value['answer_score'],
		                'answer_score' => $value['answer_cScore']
            		)
            	);  
            	$this->FeedbacksQuestion->create();
				$this->FeedbacksQuestion->save($feedback_question_data);			
			}
			$this->send_email_to_admin($user_id);
			echo json_encode(array('success' => true, 'msg' => 'Thank you, your feedback has been submitted.'));
		} else {
		echo json_encode(array('success' => false, 'msg' => 'Invalid Request.'));			
		}
		
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Feedback->recursive = 0;
		$this->set('feedbacks', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Feedback->exists($id)) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		$options = array('conditions' => array('Feedback.' . $this->Feedback->primaryKey => $id));
		$this->set('feedback', $this->Feedback->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Feedback->create();
			if ($this->Feedback->save($this->request->data)) {
				$this->Session->setFlash(__('The feedback has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The feedback could not be saved. Please, try again.'));
			}
		}
		$users = $this->Feedback->User->find('list');
		$questions = $this->Feedback->Question->find('list');
		$this->set(compact('users', 'questions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Feedback->exists($id)) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Feedback->save($this->request->data)) {
				$this->Session->setFlash(__('The feedback has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The feedback could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Feedback.' . $this->Feedback->primaryKey => $id));
			$this->request->data = $this->Feedback->find('first', $options);
		}
		$users = $this->Feedback->User->find('list');
		$questions = $this->Feedback->Question->find('list');
		$this->set(compact('users', 'questions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Feedback->id = $id;
		if (!$this->Feedback->exists()) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Feedback->delete()) {
			$this->Session->setFlash(__('The feedback has been deleted.'));
		} else {
			$this->Session->setFlash(__('The feedback could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Feedback->recursive = 0;
		$this->set('feedbacks', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Feedback->exists($id)) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		$options = array('conditions' => array('Feedback.' . $this->Feedback->primaryKey => $id));
		$this->set('feedback', $this->Feedback->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Feedback->create();
			if ($this->Feedback->save($this->request->data)) {
				$this->Session->setFlash(__('The feedback has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The feedback could not be saved. Please, try again.'));
			}
		}
		$users = $this->Feedback->User->find('list');
		$questions = $this->Feedback->Question->find('list');
		$this->set(compact('users', 'questions'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Feedback->exists($id)) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Feedback->save($this->request->data)) {
				$this->Session->setFlash(__('The feedback has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The feedback could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Feedback.' . $this->Feedback->primaryKey => $id));
			$this->request->data = $this->Feedback->find('first', $options);
		}
		$users = $this->Feedback->User->find('list');
		$questions = $this->Feedback->Question->find('list');
		$this->set(compact('users', 'questions'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Feedback->id = $id;
		if (!$this->Feedback->exists()) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		
		$video_obj = $this->Feedback->findById($id, array('video'));
		@unlink(WWW_ROOT . 'files' . DS . 'videos' . DS . $video_obj['Feedback']['video']);

		$this->request->onlyAllow('post', 'delete');
		if ($this->Feedback->delete()) {
			$this->Session->setFlash(__('The feedback has been deleted.'), 'default', array('class' => 'valid'));
		} else {
			$this->Session->setFlash(__('The feedback could not be deleted. Please, try again.'), 'default', array('class' => 'error'));
		}
		return $this->redirect($this->referer());
	}
    /*public function graph_data(){
        $data = $this->Feedback->feedbackData();
        $this->_setTrace($data);
    }*/

       public function admin_video_response($id = null, $category_id = 1) {
		$this->paginate = array('all',
			'limit' => 20,
			'conditions' => array(
				'Feedback.user_id' => $id,
				'Feedback.category_id' => $category_id,
				'Feedback.video IS NOT NULL' 
			),
			'order' => array('Feedback.created' => 'DESC'),
		);
		//$this->set('feedbacks', $this->Paginator->paginate());
		$category = $this->Feedback->Category->find('list');
		$feedbacks = $this->Paginator->paginate();
		$this->set(compact('feedbacks', 'category', 'category_id', 'id'));
	}

	public function admin_video($category_id = 1) {
		$this->Feedback->recursive = 0;
		$this->paginate = array('all',
			'limit' => 20,
			'conditions' => array(
				'Feedback.category_id' => $category_id,
				'Feedback.video IS NOT NULL' 
			),
			'order' => array('Feedback.created' => 'DESC'),
		);
		$category = $this->Feedback->Category->find('list');
		$feedbacks = $this->Paginator->paginate();
		$this->set(compact('feedbacks', 'category', 'category_id'));
	}


	public function admin_detail_view($id, $category_id = null){
        $this->recursive = 0;
//        $this->loadModel('UsersOption');
//        if(empty($category_id)) {
//            $options_1 = array(
//                'conditions' => [
//                    'UsersOption.user_id' => $id,
//                ],
//                'fields' => array('UsersOption.created', 'UsersOption.user_id', 'UsersOption.id', 'User.name' ),
//                'order' => array('UsersOption.created' => 'DESC'),
//            );
//            $assessment_data = $this->UsersOption->find('all', $options_1);
//
//            $options_2 = array(
//                'conditions' => array(
//                    'Feedback.user_id' => $id,
//                    'Feedback.category_id IS NOT NULL'
//                ),
//                'order' => array('Feedback.created' => 'DESC'),
//                'fields' => array('Feedback.category_id', 'Category.name', 'Feedback.created', 'Feedback.user_id', 'Feedback.id', 'User.name')
//            );
//            $other_data = $this->Feedback->find('all', $options_2);
//            $feedbacks = am($assessment_data, $other_data);
//        }
//		else {
//            if($category_id == 1){
//                $options = array(
//                    //'limit' => 10,
//                    'conditions' => [
//                        'UsersOption.user_id' => $id,
//                    ],
//                    'fields' => array('UsersOption.created', 'UsersOption.user_id', 'UsersOption.id', 'User.name' ),
//                    'order' => array('UsersOption.created' => 'DESC'),
//                );
//                $this->Paginator->settings = $options;
//                $feedbacks = $this->Paginator->paginate('UsersOption');
//                //pr($feedbacks);die;
//            } else {
//                $conditions = array(
//                    'Feedback.user_id' => $id,
//                    'Feedback.category_id IS NOT NULL'
//                );
//                $conditions = am($conditions, array('Feedback.category_id' => $category_id));
//                $options = array('all',
//                    //'limit' => 10,
//                    'conditions' => $conditions,
//                    'order' => array('Feedback.created' => 'DESC'),
//                    'fields' => array('Feedback.category_id', 'Category.name', 'Feedback.created', 'Feedback.user_id', 'Feedback.id', 'User.name')
//                );
//                $this->Paginator->settings = $options;
//                $feedbacks = $this->Paginator->paginate('Feedback');
//            }
//       }
//		//$this->set('feedbacks', $this->Paginator->paginate());
//		$category = $this->Feedback->Category->find('list');
//
//		$this->set(compact('feedbacks', 'category', 'category_id', 'id'));

        $conditions = array(
            'Feedback.user_id' => $id,
            'Feedback.category_id IS NOT NULL'
        );
        if(!$category_id == null){
            $conditions = am($conditions, array('Feedback.category_id' => $category_id));
        }

        $this->recursive = 0;
        $this->paginate = array('all',
            'limit' => 10,
            'conditions' => $conditions,
            'order' => array('Feedback.created' => 'DESC'),
            'fields' => array('Feedback.category_id', 'Category.name', 'Feedback.created', 'Feedback.user_id', 'Feedback.id', 'User.name', 'Feedback.video')
        );
        //$this->set('feedbacks', $this->Paginator->paginate());
        $category = $this->Feedback->Category->find('list');
        $feedbacks = $this->Paginator->paginate();
        $this->set(compact('feedbacks', 'category', 'category_id', 'id'));

	}

	 public function admin_wordcloud($user_id, $category_id){
        $word_cloud_data = $this->Feedback->getWordCloudData($user_id, $category_id);
        $data = Hash::extract($word_cloud_data, '{n}.Question.{n}.FeedbacksQuestion.answer');
        //$this->_setTrace($data);
        $words = array();
        foreach ($data as $d) {
            $words = am($words, explode(' ', preg_replace('/[^a-zA-Z 0-9]+/', '', $d)));
        }
        #pr($words);die;
        $preprositions = array('is', 'the', 'for', 'a', 'or', 'on', 'so', 'and', 'to', 'very',
            'it', 'in', 'at', 'before', 'about', 'on', 'with', 'without', 'above', 'before',
            'or', 'so', 'after', 'even', 'if', 'only', 'as', 'though', 'then', 'now', 'since',
            'once', 'provided', 'that', 'unless', 'this', 'til', 'until', 'when', 'who', 'where',
            'why', 'rather', 'than', 'lest', 'just', 'yet', 'without', 'within', 'via', 'upon',
            'under', 'towards', 'toward', 'over', 'off', 'onto', 'like', 'except', 'during', 'despite'
        );
        $words = array_count_values(array_diff(array_filter($words), $preprositions));
        #pr($words);die;
        $counts = array_values($words);
        $max = max($counts);
        $allWords = array();
        foreach ($words as $key => $value) {
            $allWords[] = array(
                'text' => $key,
                'size' => ($value/$max)*100
            );
        }
        //$min = min($counts);
        //pr($min);
        //die(json_encode($words));        
        $this->set(compact('allWords', 'category_id', 'user_id'));
    }

    public function admin_delete_submission($id){
    	$this->Feedback->id = $id;
		if (!$this->Feedback->exists()) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Feedback->delete()) {
			$feedback_obj = $this->Feedback->findByUserId($id, array('Feedback.id'));
			$feedback_id = $feedback_obj['Feedback']['id'];
			$this->Feedback->query('DELETE FROM feedbacks_questions WHERE feedback_id = \'' . $feedback_id . ' \'
			');
			$this->Session->setFlash(__('The submission has been deleted.'), 'default', array('class' => 'valid'));
		} else {
			$this->Session->setFlash(__('The submission could not be deleted. Please, try again.'), 'default', array('class' => 'error'));
		}
		return $this->redirect($this->referer());

    }

    public function send_email_to_admin($user_id = null){
        header('Content-Type: application/json');
        $this->autoRender = false;
  		App::import('Vendor', 'ses', array('file' => 'ses' . DS . 'ses.php'));
        $ses = new SimpleEmailService('AKIAJULLR4UQDRFARJ5A', 'nDwpsXh+UN77ivTYUgDvzo1crHdXlFi+zqhepRwZ');
        $this->autoRender= false;
        $this->loadModel('User');
        $user_obj = $this->User->findById($user_id);
        $name = $user_obj['User']['name'];
        $carer_id = $user_obj['User']['carerid'];
        $m = new SimpleEmailServiceMessage();
        $m->addTo('r.lynch@carersfederation.co.uk');
        // $m->addTo('belicia.evans.1@gmail.com');
        $m->setFrom('r.lynch@carersfederation.co.uk');
        $m->setSubject(' New Feedback from '.$name . ' - ' .$carer_id);

        $body = 'New feedback has been submitted by '.$name. ' - '. $carer_id;
        
        $plainTextBody = '';

        $m->setMessageFromString($plainTextBody,$body);
        try{
            $ses->sendEmail($m);
        } catch(Exception $e){
            
        }
    }
    public function award_info($user_id = null){    
		$this->autoRender = false;	
		$this->autoLayout = false;
		if ($this->Feedback->User->exists($user_id)) {
			$query = [
				'recursive' => -1,
				'fields' => ['Feedback.category_id', 'COUNT(Feedback.id) as assesment_count'],
				'conditions' => [
					'Feedback.user_id' => $user_id
				],
				'group' => ['Feedback.category_id']
			];
			$feedback_obj = $this->Feedback->find('all', $query);
			if(!empty($feedback_obj)) {
				$bronze_award = false;
				$silver_award = false;
				$gold_award = false;
				$carers_award = false;
				$silver_feedback_award = false;
				$gold_feedback_award = false;
				$total_award = 0;
				foreach ($feedback_obj as $key => $feedback) {
					if($feedback['Feedback']['category_id']=='1') {
						if($feedback[0]['assesment_count']==1) {
							$bronze_award = true;
							$total_award++;
						}
						if($feedback[0]['assesment_count']==2) {
							$bronze_award = true;
							$silver_award = true;
							$total_award = $total_award + 2;
						}
						if($feedback[0]['assesment_count']>=3) {
							$bronze_award = true;
							$silver_award = true;
							$gold_award = true;
							$total_award = $total_award + 3;
						}

						/*if($feedback[0]['assesment_count']>=5) {
							$total_award++;
						}*/

					}
					if($feedback['Feedback']['category_id']=='2') {
						if($feedback[0]['assesment_count']>=1) {
							$carers_award = true;
							$total_award++;
						}
					}
					if($feedback['Feedback']['category_id']=='3') {
						if($feedback[0]['assesment_count']>=5 && $feedback[0]['assesment_count']<9) {
							$silver_feedback_award = true;
							$total_award = $total_award + 1;
						}
						if($feedback[0]['assesment_count']>=9) {
							$silver_feedback_award = true;
							$gold_feedback_award = true;
							$total_award = $total_award + 2;
						}

						/*if($feedback[0]['assesment_count']>=5) {
							$total_award++;
						}*/
					}
				}
				$award_info = [
					'bronze_award' => $bronze_award,
					'silver_award' => $silver_award,
					'gold_award' => $gold_award,
					'carers_award' => $carers_award,
					'silver_feedback_award' => $silver_feedback_award,
					'gold_feedback_award' => $gold_feedback_award,
				];
				die(json_encode(['success' => true, 'award_info' => $award_info, 'total_award' => $total_award]));
			} else {
				die(json_encode(['success' => false, 'msg' => 'No Feedback Found.']));
			}						
		} else {
			die(json_encode(['success' => false, 'msg' => 'Invalid User.']));
		}		
    }

}

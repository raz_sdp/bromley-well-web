<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 * @property DeviceToken $DeviceToken
 */

class AppController extends Controller {
    public $components = array(
        'Session', 'Cookie', 'RequestHandler',
        'Auth' => array(
            'loginAction' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
            'loginRedirect' => array('controller' => 'users', 'action' => 'dashboard', 'admin' => true),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
            'authError' => 'You are not allowed',
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'email', 'password' => 'password')
                )
            )
        )
    );
    public function beforeFilter() {
        // set cookie options
        $this->Cookie->key = 'klgjs&*(#%(54646tqwdsuhf&*^Hjhsdgf5465464';
        $this->Cookie->httpOnly = true;
        if($this->params['admin']){
            $this->layout =  'admin';
        }

        if (!$this->Auth->loggedIn() && $this->Cookie->read('remember_me_cookie')) {
            $cookie = $this->Cookie->read('remember_me_cookie');
            $this->loadModel('User');
            $this->loadModel('User');
            $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.email' => $cookie['email'],
                    'User.password' => $cookie['password']
                )
            ));

            if ($user && !$this->Auth->login($user['User'])) {
                $this->redirect('/users/logout'); // destroy session & cookie
            }
        }

        $this->Auth->allow('display', 'reset_password', 'forgot_password','forget_password','g', 'complaint_entry', 'register','login', 'notification_delete', 'notification_read',
            'feedback_submission', 'check_enability', 'test', 'sendEmail','award_info','get_assesment','user_option_insert','submit_alert','get_all_alerts','submit_thought','testSMS','delete_alert');
    }


    /**
     * IT'S ALTERNATE ABOUT PRINT_R
     * Set Trace
     * @param $data
     * @param bool $die
     */
    public static function _setTrace($data = null, $die = true)
    {
        if (is_string($data)) {
            print $data;
        } else {

            print "<pre>";
            print_r($data);
            print "</pre>";
        }
        print "<hr />";
        if ($die) {
            exit();
        }
    }


    function _upload($file, $folder)
    {
        if (@is_uploaded_file($file['tmp_name'])) {
            $ext = strtolower(array_pop(explode('.', $file['name'])));
            $rnd = time() . rand(1, 999);
            $fileName = $rnd . '.' . $ext;
            $uploadFile = WWW_ROOT . 'files' . DS . $folder . DS . $fileName;
            //$dest = WWW_ROOT . 'files' . DS . 'files' . DS . $fileNameRotated;
            if (move_uploaded_file($file['tmp_name'], $uploadFile)) {
                return $fileName;
            }
        }
    }


    // function _push($getMessage, $devices = null, $notification_id = null)
    // {
    //     //if($_SERVER['HTTP_HOST'] == 'localhost') return true;
    //     App::import('Vendor', 'UA', array('file' => 'UA' . DS . 'urbanairship.php'));
    //     $this->loadModel('DeviceToken');

    //     if (empty($devices)) {
    //         $all_devices = $this->DeviceToken->getEnableUser();
    //     }
    //     if (!empty($all_devices)) {
    //         foreach ($all_devices as $device) {
    //             if ( trim($device['DeviceToken']['stage']) == 'development' || trim($device['DeviceToken']['stage']) == 'test') {
    //                 $APP_MASTER_SECRET = 'NK3Q4cwVQ5CSZPCj2nQ4wA';
    //                 $APP_KEY = 'BUPZRxPpQ9uiSFQggE47dQ';
    //             } else {
    //                 $APP_MASTER_SECRET = 'vYD0z5lrRliS384bkj0Y1A';
    //                 $APP_KEY = '06RbLJuKSO6MdBEhmZAsqA';
    //             }
    //             $airship = new Airship($APP_KEY, $APP_MASTER_SECRET);

    //             $device_token = trim($device['DeviceToken']['device_token']);
    //             if (!empty($device_token)) {
    //                 if (trim($device['DeviceToken']['device_type']) == 'ios') {
    //                     $message = array('aps' => array(
    //                         'alert' => $getMessage,
    //                         'sound' => 'default',
    //                         'badge' => '+1',
    //                         'notification_id' => empty($notification_id) ? '' : $notification_id
    //                     )
    //                     );
    //                     $res = $airship->push($message, $device_token);
    //                 } else if (trim($device['DeviceToken']['device_type']) == 'android') {
    //                     $message = array('apids' => array($device_token),
    //                         'android' => array(
    //                             'alert' => $getMessage
    //                         )
    //                     );
    //                     $res = $airship->push($message);
    //                 }
    //             //    $this->_entry_user_notification($device['DeviceToken']['user_id'], $notification_id);
    //             }
    //         }
    //     }
    //     // exit;
    // }

    function _push($getMessage, $devices = null, $notification_id = null){
      //print_r('expression');
        $certificate_path_development = WWW_ROOT . 'files' . DS . 'push_certificate' . DS . 'dev.pem';
        $certificate_path_production = WWW_ROOT . 'files' . DS . 'push_certificate' . DS . 'prod.pem';

        $this->loadModel('DeviceToken');
        if (empty($devices)) {
            $all_devices = $this->DeviceToken->getEnableUser();
        }
        // Put your private key's passphrase here:carer
        $passphrase = '1234567890';

        $push_url_development = 'ssl://gateway.sandbox.push.apple.com:2195';
        $push_url_production = 'ssl://gateway.push.apple.com:2195';

        $android_device_tokens = array();
        if (!empty($all_devices)) {
          foreach($all_devices as $device) {
            //$deviceToken = '6e84be109c31dd64e93485c54454e75527035965a2f8be531fa5785b4814bea2'; 
            $deviceToken = trim($device['DeviceToken']['device_token']);
            if($device['DeviceToken']['device_type'] === 'ios'){
              $body['aps'] = array(
                      'alert' => $getMessage,
                      'sound' => 'default',
                      'badge' => '+1',
                      'notification_id' => empty($notification_id) ? '' : $notification_id
              );              
              if($device['DeviceToken']['stage'] != 'production'){
                   $this->_apns($certificate_path_development, $passphrase, $push_url_development, $deviceToken, $body);
              } else {
                   $this->_apns($certificate_path_production, $passphrase, $push_url_production, $deviceToken, $body);
              }
            } elseif($device['DeviceToken']['device_type'] === 'android'){
              array_push($android_device_tokens, $deviceToken);
            }
          }
        }
        //print_r($android_device_tokens);
        if(count($android_device_tokens) > 0) {         
          $this->_agcm($android_device_tokens, 'AIzaSyBn8aV09bitwtEzfZfGUgjZPmK8XHLX_Bg', $getMessage);
        }

    }

    private function _apns($certificate_path, $passphrase, $push_url, $deviceToken, $body){
       // Configure::write('debug', 2);
      $ctx = stream_context_create();
      stream_context_set_option($ctx, 'ssl', 'local_cert', $certificate_path);
      stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

      // Open a connection to the APNS server

      $fp = stream_socket_client($push_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
      if (!$fp) exit(0); //      exit("Failed to connect: $err $errstr" . PHP_EOL);
      //echo 'Connected to APNS' . PHP_EOL;

      // Encode the payload as JSON

      $payload = json_encode($body);

      // Build the binary notification

      $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

      // Send it to the server

      $result = fwrite($fp, $msg, strlen($msg));
      //echo $result;
      // if (!$result) echo 'Message not delivered' . PHP_EOL;
      // else echo 'Message successfully delivered' . PHP_EOL;

      // Close the connection to the server
      fclose($fp);
    }

    private function _agcm($registrationIds, $API_ACCESS_KEY, $getMessage){
      //define( 'API_ACCESS_KEY', 'AIzaSyBIB74xCLAT1veUkLkge9fd6c4J40QZ48o' );
        //print_r($registrationIds);
      //$registrationIds = array( $_GET['id'] );
       
      // prep the bundle
      $msg = array
      (
          'message'     => $getMessage,
        //'title'     => 'This is a title. title',
        //'subtitle'    => 'This is a subtitle. subtitle',
        //'tickerText'  => 'Ticker text here...Ticker text here...Ticker text here',
        //'vibrate' => 1,
        'sound'   => 1
      );
       
      $fields = array
      (
        'registration_ids'  => $registrationIds,
        'data'=> $msg
      );
       
      $headers = array
      (
        'Authorization: key=' . $API_ACCESS_KEY,
        'Content-Type: application/json'
      );
       
       /*
      $ch = curl_init();
      curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
      curl_setopt( $ch,CURLOPT_POST, true );
      curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
      curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
      curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
      $result = curl_exec($ch );
      curl_close( $ch );
      echo $result; */

      $options = array(
        'http'=>array(
          'method' => 'POST',
          'header' =>
            'Authorization: key=' . $API_ACCESS_KEY . "\r\n".
          'Content-Type: application/json',
          'content' => json_encode( $fields )
      ));

      $context = stream_context_create($options);

      $result = file_get_contents('https://android.googleapis.com/gcm/send', false, $context);

      //echo $result;
       
      //echo $result;
    }

    public function addOrdinalNumberSuffix($num) {
        if (!in_array(($num % 100),array(11,12,13))){
            switch ($num % 10) {
                // Handle 1st, 2nd, 3rd
                case 1:  return $num.'st';
                case 2:  return $num.'nd';
                case 3:  return $num.'rd';
            }
        }
        return $num.'th';
    }

    /**
     * @param $needle
     * @param $haystack
     * @return bool|int|string
     */
    function recursive_array_search($needle,$haystack) {
        foreach($haystack as $key=>$value) {
            $current_key=$key;
            if($needle===$value OR (is_array($value) && $this->recursive_array_search($needle,$value))) {
                return $current_key;
            }
        }
        return false;
    }
    public function send_email($mail, $text, $subject, $usr = null, $pass = null){
        App::import('Vendor', 'ses', array('file' => 'ses' . DS . 'ses.php'));
        $ses = new SimpleEmailService('AKIAIE5XEFETLSRTGEXA', 'YkYz0o99GbLaa3VWhMXdYjTFLtnUZ2+vNSj0XY2P', 'email-smtp.eu-west-1.amazonaws.com');
        $m = new SimpleEmailServiceMessage();
        $m->addTo($mail);
        $m->setFrom('justin@businessapps.co.uk');
        $m->setSubject($subject);
        //pr($m);die;
        //$body = $text;
        $body = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
            <html xmlns='http://www.w3.org/1999/xhtml'>
            <head>
                <meta name='viewport' content='width=device-width' />
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
                <title>Welcome Card</title>
            </head>
            <body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0' style='margin: 0px; padding: 10px; background-color: #f6f6f6;' bgcolor='#f6f6f6' width='100%'>
                <table class='body-wrap' align='center' bgcolor='#f6f6f6' width='600' cellpading='10'>
                    <tr>
                        <td></td>
                        <td class='container' bgcolor='#FFFFFF'>
                            <div class='content'>
                            <table>
                                <tr>
                                    <td>". $text."</td>
                                </tr>
                            </table>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </body>
            </html>";

        $plainTextBody = '';
        $m->setMessageFromString($plainTextBody,$body);
        try{
            $ses->sendEmail($m);
            die(json_encode(array('success' => true, 'msg' => 'Thank you! A password reset link has been sent to your email.')));
        } catch(Exception $e){
            pr($e);die;
        }

    }

    public function _sendSMS($phone_no = null, $sms = null){
        //Configure::write('debug', 2);
        require ROOT.'/vendor/autoload.php';
        error_reporting(E_ALL);
        ini_set("display_errors", 1);

        $params = array(
            'credentials' => array(
                'key' => 'AKIAIE5XEFETLSRTGEXA',
                'secret' => 'YkYz0o99GbLaa3VWhMXdYjTFLtnUZ2+vNSj0XY2P',
            ),
            'region' => 'eu-west-1', // < your aws from SNS Topic region
            'version' => 'latest'
        );
        $sns = new \Aws\Sns\SnsClient($params);

        $args = array(
            'MessageAttributes' => [
                    'AWS.SNS.SMS.SenderID' => [
                    'DataType' => 'String',
                    'StringValue' => 'BromleyWell'
                ]
             ],
            "SMSType" => "Promotional",
            "Message" => $sms,
            "PhoneNumber" => $phone_no
        );

        $result = $sns->publish($args);
        /*echo "<pre>";
        print_r($result);die;
        echo "</pre>";*/
    }
}

<?php
App::uses('AppController', 'Controller');

/**
 * Notifications Controller
 *
 * @property Notification $Notification
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class NotificationsController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->Notification->recursive = 0;
        $this->set('notifications', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        if (!$this->Notification->exists($id)) {
            throw new NotFoundException(__('Invalid notification'));
        }
        $options = array('conditions' => array('Notification.' . $this->Notification->primaryKey => $id));
        $this->set('notification', $this->Notification->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->Notification->create();
            if ($this->Notification->save($this->request->data)) {
                $this->Session->setFlash(__('The notification has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The notification could not be saved. Please, try again.'));
            }
        }
        $users = $this->Notification->User->find('list');
        $this->set(compact('users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if (!$this->Notification->exists($id)) {
            throw new NotFoundException(__('Invalid notification'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Notification->save($this->request->data)) {
                $this->Session->setFlash(__('The notification has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The notification could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Notification.' . $this->Notification->primaryKey => $id));
            $this->request->data = $this->Notification->find('first', $options);
        }
        $users = $this->Notification->User->find('list');
        $this->set(compact('users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null)
    {
        $this->Notification->id = $id;
        if (!$this->Notification->exists()) {
            throw new NotFoundException(__('Invalid notification'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Notification->delete()) {
            $this->Session->setFlash(__('The notification has been deleted.'));
        } else {
            $this->Session->setFlash(__('The notification could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $this->Notification->recursive = 0;
        $this->paginate = array('all',
            'limit' => 10,
            'conditions' => array(
                'Notification.is_deleted' => 0,
            ),
            'order' => array('Notification.created' => 'DESC'),
        );
        $this->set('notifications', $this->Paginator->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->Notification->exists($id)) {
            throw new NotFoundException(__('Invalid notification'));
        }
        $options = array('conditions' => array('Notification.' . $this->Notification->primaryKey => $id));
        $this->set('notification', $this->Notification->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->Notification->create();
            if ($this->Notification->save($this->request->data)) {
                if ($this->request->data['Notification']['is_sent'] == '1') {
                    $msg = (strlen($this->request->data['Notification']['notification']) > 110) ? substr($this->request->data['Notification']['notification'],0,110).'...' : $this->request->data['Notification']['notification'];
                    $this->_push($msg, null, $this->Notification->id);
                }
                $this->Session->setFlash(__('The notification has been saved.'), 'default', array('class' => 'valid'));
                return $this->redirect(array('action' => 'index','notification'));
            } else {
                $this->Session->setFlash(__('The notification could not be saved. Please, try again.'), 'default', array('class' => 'error'));
            }
        }
        $users = $this->Notification->User->find('list');
        $this->set(compact('users'));

    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($param , $id = null)
    {
        if (!$this->Notification->exists($id)) {
            throw new NotFoundException(__('Invalid notification'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Notification->save($this->request->data)) {
                if ($this->request->data['Notification']['is_sent'] == '1') {
                    $msg = (strlen($this->request->data['Notification']['notification']) > 100) ? substr($this->request->data['Notification']['notification'],0,100).'...' : $this->request->data['Notification']['notification'];
                    $this->_push($msg, null, $this->Notification->id);
                    //$this->_push($this->request->data['Notification']['notification'], null, $this->Notification->id);
                }
                $this->Session->setFlash(__('The notification has been saved.'), 'default', array('class' => 'valid'));
                return $this->redirect(array('action' => 'index','notification'));
            } else {
                $this->Session->setFlash(__('The notification could not be saved. Please, try again.'), 'default', array('class' => 'error'));
            }
        } else {
            $options = array('conditions' => array('Notification.' . $this->Notification->primaryKey => $id));
            $this->request->data = $this->Notification->find('first', $options);
        }
        $users = $this->Notification->User->find('list');
        $this->set(compact('users'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->Notification->id = $id;
        if (!$this->Notification->exists()) {
            throw new NotFoundException(__('Invalid notification'));
        }
        $data['Notification']['is_deleted'] = 1;
        if ($this->Notification->save($data)) {
            $find_notifications = $this->Notification->NotificationsUser->find('all', array(
                'conditions' => array('NotificationsUser.notification_id' => $id),
                'fields' => array('NotificationsUser.id')
            ));
            foreach ($find_notifications as $key => $find_notification) {
                $this->Notification->NotificationsUser->id = $find_notification['NotificationsUser']['id'];
                $data_notification['NotificationsUser']['is_deleted'] = 1;
                $this->Notification->NotificationsUser->save($data_notification);
            }
            $this->Session->setFlash(__('The notification has been deleted.'), 'default', array('class' => 'valid'));
        } else {
            $this->Session->setFlash(__('The notification could not be deleted. Please, try again.'), 'default', array('class' => 'error'));
        }
        return $this->redirect(array('action' => 'index', 'notification'));
    }
}

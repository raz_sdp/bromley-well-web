<?php
App::uses('AppController', 'Controller');

/**
 * Assesments Controller
 *
 * @property Assesment $Assesment
 * @property PaginatorComponent $Paginator
 */
class AssesmentsController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $this->Assesment->recursive = 0;
        $this->set('assesments', $this->Paginator->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->Assesment->exists($id)) {
            throw new NotFoundException(__('Invalid assesment'));
        }
        $options = array('conditions' => array('Assesment.' . $this->Assesment->primaryKey => $id));
        $this->set('assesment', $this->Assesment->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->Assesment->create();
            if ($this->Assesment->save($this->request->data)) {
                $this->Flash->success(__('The assesment has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The assesment could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null)
    {
        if (!$this->Assesment->exists($id)) {
            throw new NotFoundException(__('Invalid assesment'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Assesment->save($this->request->data)) {
                $this->Flash->success(__('The assesment has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The assesment could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Assesment.' . $this->Assesment->primaryKey => $id));
            $this->request->data = $this->Assesment->find('first', $options);
        }
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->Assesment->id = $id;
        if (!$this->Assesment->exists()) {
            throw new NotFoundException(__('Invalid assesment'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Assesment->delete()) {
            $this->Flash->success(__('The assesment has been deleted.'));
        } else {
            $this->Flash->error(__('The assesment could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_assesment()
    {
        $this->autoRender = false;
        $questionAndAns = [
            'Friends' => [
                'I am happy with the friendships I have',
                'I have some friends but would like more',
                "I'm not happy with the friendships I have"
            ],
            'Free time and fun' => [
                'I have lots of free time to do what I want and opportunities to have fun',
                'I have some time to myself to do what I want and have fun but would prefer more',
                'I have little or no time for myself, all my time is taken up caring or watching over the person I care for'
            ],
            'Health and body' => [
                'I feel healthy, I have a healthy lifestyle and a balanced diet',
                'I have some small health problems, would like to be fitter and eat a bit more healthily',
                "I have some health problems, I don't do much exercise, and my diet isn't great"
            ],
            ' Smoking, drugs and alcohol' => [
                'I have good knowledge about these issues',
                'I would like to know more about these issues',
                "I don't know much at all about these issues"
            ],
            'School work' => [
                'I do well in school, and always manage to do my homework on time',
                'I am able to do my schoolwork and homework but it is difficult and I would like to better',
                'I feel like I am not doing well at school and find homework hard'
            ],
            'Bullying' => [
                'I never feel bullied either in school or outside of school',
                'People can be mean to me sometimes, and sometimes it makes me feel bullied',
                "I feel bullied a lot of the time and don't know what to do about it"
            ],
            'Emotional life' => [
                'I cope well with my emotions and understand how I feel',
                'Sometimes I find it difficult to understand and control my emotions, my mood can change a lot',
                'I find it difficult to understand my emotions, I can get stressed, angry or anxious easily'
            ],
            'How does caring affect my life' => [
                "Caring is a part of my life, but I'm ok with this, and I get to have a break to do what I like when I need it",
                "Sometimes I get a break from caring and get todo what I would like todo but I don't always get to choose when",
                "Caring stops me doing things I'd like to do"
            ],
            'Happy and safe at home' => [
                'I always feel safe and happy at home',
                'I mostly feel happy and safe at home',
                "I don't feel safe or happy at home"
            ],
            'Cooking at home' => [
                'I can cook and quite enjoy it',
                'I can cook a few things but would like to feel more confident I can cook a few things but would like to feel more confident',
                "I can't cook and don't know how to cook"
            ]
        ];
        foreach ($questionAndAns as $key => $q) {
            $this->request->data['Assesment']['title'] = $key;
            $this->Assesment->create();
            $this->Assesment->save($this->request->data);
            $id = $this->Assesment->id;
            foreach ($q as $p) {
                $data['Option'] = [
                    'assesment_id' => $id,
                    'title' => $p
                ];
                $this->Assesment->Option->create();
                $this->Assesment->Option->save($data);

            }
        }
    }

    public function get_assesment()
    {
        $query = [
            'contain' => [
                'Option' => [
                    'fields' => [
                        'Option.id',
                        'Option.title'
                    ]
                ],
            ],
            'fields' => [
                'Assesment.id',
                'Assesment.title'
            ]
        ];
        $this->Assesment->Behaviors->load('Containable');
        $data = $this->Assesment->find('all', $query);
        die(json_encode($data));
    }
}

<?php
App::uses('AppController', 'Controller');
/**
 * Thoughts Controller
 *
 * @property Thought $Thought
 * @property PaginatorComponent $Paginator
 */
class ThoughtsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($user_id = null) {
		$this->Thought->recursive = 0;
		$conditions = [];
		if(!empty($user_id)) {
			$conditions = [
                'Thought.user_id' => $user_id
            ];
		}
		$query = [
            'conditions' => $conditions
        ];
        $this->Paginator->settings = $query;
        $thoughts = $this->Paginator->paginate('Thought');		
		$this->set(compact('thoughts'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Thought->exists($id)) {
			throw new NotFoundException(__('Invalid thought'));
		}
		$options = array('conditions' => array('Thought.' . $this->Thought->primaryKey => $id));
		$this->set('thought', $this->Thought->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Thought->create();
			if ($this->Thought->save($this->request->data)) {
				$this->Flash->success(__('The thought has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The thought could not be saved. Please, try again.'));
			}
		}
		$users = $this->Thought->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Thought->exists($id)) {
			throw new NotFoundException(__('Invalid thought'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Thought->save($this->request->data)) {
				$this->Flash->success(__('The thought has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The thought could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Thought.' . $this->Thought->primaryKey => $id));
			$this->request->data = $this->Thought->find('first', $options);
		}
		$users = $this->Thought->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null, $user_id = null) {
        if($id == 'all') {
            $res = $this->Thought->query('TRUNCATE TABLE thoughts;');
            if ($res) {
                $this->Session->setFlash(__('The thought has been deleted.'), 'default', array('class' => 'valid text-center'));
            } else {
                $this->Session->setFlash(__('The thought could not be deleted. Please, try again.'), 'default', array('class' => 'error text-center'));
            }
        } else {
            $this->Thought->id = $id;
            if (!$this->Thought->exists()) {
                throw new NotFoundException(__('Invalid thought'));
            }
            $this->request->allowMethod('post', 'delete');
            if ($this->Thought->delete()) {
                $this->Session->setFlash(__('The thought has been deleted.'), 'default', array('class' => 'valid text-center'));
            } else {
                $this->Session->setFlash(__('The thought could not be deleted. Please, try again.'), 'default', array('class' => 'error text-center'));
            }
        }

        return $this->redirect(array('action' => 'index', $user_id));
    }

    /**
     * Called from App, It's an API
     */
    public function submit_thought(){
        $this->autoLayout = false;
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $this->Thought->create();
            if ($this->Thought->save($this->request->data)) {
                die(json_encode(['success' => true, 'msg' => 'Thank you, your feedback has been submitted.']));
            } else {
                die(json_encode(['success' => false, 'msg' => 'Something went wrong.Please, try again.']));
            }
        }
    }
}

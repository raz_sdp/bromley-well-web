<?php
App::uses('AppController', 'Controller');
/**
 * PrequalifyingQuestions Controller
 *
 * @property PrequalifyingQuestion $PrequalifyingQuestion
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PrequalifyingQuestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PrequalifyingQuestion->recursive = 0;
		$this->set('prequalifyingQuestions', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PrequalifyingQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid prequalifying question'));
		}
		$options = array('conditions' => array('PrequalifyingQuestion.' . $this->PrequalifyingQuestion->primaryKey => $id));
		$this->set('prequalifyingQuestion', $this->PrequalifyingQuestion->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PrequalifyingQuestion->create();
			if ($this->PrequalifyingQuestion->save($this->request->data)) {
				$this->Session->setFlash(__('The prequalifying question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prequalifying question could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PrequalifyingQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid prequalifying question'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PrequalifyingQuestion->save($this->request->data)) {
				$this->Session->setFlash(__('The prequalifying question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prequalifying question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PrequalifyingQuestion.' . $this->PrequalifyingQuestion->primaryKey => $id));
			$this->request->data = $this->PrequalifyingQuestion->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PrequalifyingQuestion->id = $id;
		if (!$this->PrequalifyingQuestion->exists()) {
			throw new NotFoundException(__('Invalid prequalifying question'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PrequalifyingQuestion->delete()) {
			$this->Session->setFlash(__('The prequalifying question has been deleted.'));
		} else {
			$this->Session->setFlash(__('The prequalifying question could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function g(){
		$this->autoRender = false;		
		$all_questions = $this->PrequalifyingQuestion->find('all', array(
				'fields' => array('id', 'question'),
			));
		
		$all_questions = Hash::extract($all_questions, '{n}.PrequalifyingQuestion');
		//pr($all_questions);
		die(json_encode(array('prequalifying_question' => $all_questions)));
	}


}

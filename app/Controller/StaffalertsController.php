<?php
App::uses('AppController', 'Controller');
/**
 * Staffalerts Controller
 *
 * @property Staffalert $Staffalert
 * @property PaginatorComponent $Paginator
 */
class StaffalertsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($user_id = null) {
        $query = [
            'conditions' => [
                'Staffalert.user_id' => $user_id
            ]
        ];
        $this->Paginator->settings = $query;
        $staffalerts = $this->Paginator->paginate('Staffalert');
		$this->set(compact('staffalerts'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Staffalert->exists($id)) {
			throw new NotFoundException(__('Invalid staffalert'));
		}
		$options = array('conditions' => array('Staffalert.' . $this->Staffalert->primaryKey => $id));
		$this->set('staffalert', $this->Staffalert->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Staffalert->create();
			if ($this->Staffalert->save($this->request->data)) {
				$this->Flash->success(__('The staffalert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The staffalert could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Staffalert->exists($id)) {
			throw new NotFoundException(__('Invalid staffalert'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Staffalert->save($this->request->data)) {
				$this->Flash->success(__('The staffalert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The staffalert could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Staffalert.' . $this->Staffalert->primaryKey => $id));
			$this->request->data = $this->Staffalert->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null, $user_id = null) {
		$this->Staffalert->id = $id;
		if (!$this->Staffalert->exists()) {
			throw new NotFoundException(__('Invalid staffalert'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Staffalert->delete()) {
            $this->Session->setFlash(__('The staff Alert has been deleted.'), 'default', array('class' => 'valid text-center'));
		} else {
            $this->Session->setFlash(__('The staff Alert could not be deleted. Please, try again.'), 'default', array('class' => 'error text-center'));
		}
		return $this->redirect(array('action' => 'index', $user_id));
	}

    public function submit_alert(){
        //Configure::write('debug', 2);
        $this->autoLayout = false;
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $message = $this->request->data['Staffalert']['message'];
            $this->Staffalert->create();
            if ($this->Staffalert->save($this->request->data)) {
            	$admins = $this->Staffalert->User->find('all', [
                    'fields' => 'User.mobile',
                    'conditions' => [
                        'User.role' => 'admin'
                    ]
                ]);
                foreach($admins as $admin){                   
                    if(!empty($admin['User']['mobile'])) {
                        $mobile = preg_replace('/^0/','+44',$admin['User']['mobile']);                       
                        $this->_sendSMS($mobile,$message);
                    }  

                }                
                die(json_encode(['success' => true, 'msg' => 'Thank you, your Staff Alert has been submitted.']));
            } else {
                die(json_encode(['success' => false, 'msg' => 'Something went wrong.Please, try again.']));
            }
        }
    }
    /**
     * @param null $user_id
     */
    public function get_all_alerts($user_id = null){
        $this->autoLayout = false;
        $this->autoRender = false;
        $query = [
            'recursive' => -1,
            'fields' => [
                'Staffalert.id','Staffalert.message', 'Staffalert.created'
            ],
            'conditions' => [
                'Staffalert.user_id' => $user_id,
                'Staffalert.is_delete' => 0
            ]
        ];
        $data = $this->Staffalert->find('all', $query);
        $result = Hash::extract($data, '{n}.Staffalert');
        die(json_encode(['success' => true, 'result' => $result]));
    }   
     public function testSMS(){
        Configure::write('debug', 0);
        require ROOT.'/vendor/autoload.php';
        //App::import('Vendor', 'ses', array('file' => 'ses' . DS . 'ses.php'));
        error_reporting(E_ALL);
        ini_set("display_errors", 1);

        $params = array(
            'credentials' => array(
                'key' => 'AKIAIE5XEFETLSRTGEXA',
                'secret' => 'YkYz0o99GbLaa3VWhMXdYjTFLtnUZ2+vNSj0XY2P',
            ),
            'region' => 'eu-west-1', // < your aws from SNS Topic region
            'version' => 'latest'
        );
        $sns = new \Aws\Sns\SnsClient($params);

        $args = array(            
            'MessageAttributes' => [
                    'AWS.SNS.SMS.SenderID' => [
                    'DataType' => 'String',
                    'StringValue' => 'BromleyWell'
                ]
             ],
            //"SenderID" => "BromleyWell",
            "SMSType" => "Promotional",
            "Message" => "Test",
            "PhoneNumber" => '+8801724160299' //+447747620039
        );

        $result = $sns->publish($args);
        echo "<pre>";
        var_dump($result);
        echo "</pre>";
    }
    public function delete_alert($id){
       $this->Staffalert->id = $id;
        if($this->Staffalert->saveField('is_delete',1)){
            die(json_encode(['success' => true]));
        } else {
            die(json_encode(['success' => false]));
        }
    }
}

<?php
App::uses('AppController', 'Controller');
/**
 * Complaints Controller
 *
 * @property Complaint $Complaint
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ComplaintsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Complaint->recursive = 0;
		$this->set('complaints', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Complaint->exists($id)) {
			throw new NotFoundException(__('Invalid complaint'));
		}
		$options = array('conditions' => array('Complaint.' . $this->Complaint->primaryKey => $id));
		$this->set('complaint', $this->Complaint->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Complaint->create();
			if ($this->Complaint->save($this->request->data)) {
				$this->Session->setFlash(__('The complaint has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The complaint could not be saved. Please, try again.'));
			}
		}
		$users = $this->Complaint->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Complaint->exists($id)) {
			throw new NotFoundException(__('Invalid complaint'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Complaint->save($this->request->data)) {
				$this->Session->setFlash(__('The complaint has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The complaint could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Complaint.' . $this->Complaint->primaryKey => $id));
			$this->request->data = $this->Complaint->find('first', $options);
		}
		$users = $this->Complaint->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Complaint->id = $id;
		if (!$this->Complaint->exists()) {
			throw new NotFoundException(__('Invalid complaint'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Complaint->delete()) {
			$this->Session->setFlash(__('The complaint has been deleted.'));
		} else {
			$this->Session->setFlash(__('The complaint could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Complaint->recursive = 0;
		$this->paginate = array('all',
			'limit' => 10,
			'order' => 'Complaint.created DESC'
		);
		$this->set('complaints', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->autoRender = false;
		if (!$this->Complaint->exists($id)) {
			throw new NotFoundException(__('Invalid complaint'));
		}
		$options = array('conditions' => array('Complaint.' . $this->Complaint->primaryKey => $id));
		$complaint = $this->Complaint->find('first', $options);

		//$this->set('complaint', $this->Complaint->find('first', $options));
		echo json_encode($complaint);
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Complaint->create();
			if ($this->Complaint->save($this->request->data)) {
				$this->Session->setFlash(__('The complaint has been saved.'));
				return $this->redirect(array('action' => 'index', 'complaint'));
			} else {
				$this->Session->setFlash(__('The complaint could not be saved. Please, try again.'));
			}
		}
		$users = $this->Complaint->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Complaint->exists($id)) {
			throw new NotFoundException(__('Invalid complaint'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Complaint->save($this->request->data)) {
				$this->Session->setFlash(__('The complaint has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The complaint could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Complaint.' . $this->Complaint->primaryKey => $id));
			$this->request->data = $this->Complaint->find('first', $options);
		}
		$users = $this->Complaint->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Complaint->id = $id;
		if (!$this->Complaint->exists()) {
			throw new NotFoundException(__('Invalid complaint'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Complaint->delete()) {
			$this->Session->setFlash(__('The complaint has been deleted.'), 'default', array('class' => 'valid'));
		} else {
			$this->Session->setFlash(__('The complaint could not be deleted. Please, try again.'), 'default', array('class' => 'error'));
		}
		return $this->redirect(array('action' => 'index', 'complaint'));
	}

	public function complaint_entry(){ 
   		$this->autoRender = false;
   		if($this->request->is('post')){ 
   			//$this->load('User');
   			$this->Complaint->User->recursive = -1; 
   			$user_check = $this->Complaint->User->findByIdAndCarerid($this->request->data['Complaint']['user_id'], $this->request->data['Complaint']['carerid'], array('id'));
   			if(!empty($user_check)){
   				$this->Complaint->create();
				if ($this->Complaint->save($this->request->data)) {
					die(json_encode(array('success'=> true, 'msg' => 'Complaint Successfully Saved.')));
				} else {
					die(json_encode(array('success'=> false, 'msg' => 'The complaint could not be saved. Please, try again.')));
				}
   			}else die(json_encode(array('success'=> false, 'msg' => 'Invalid Id.')));
    	} else die(json_encode(array('success'=> false, 'msg' => 'Invalid request.')));
	}
}

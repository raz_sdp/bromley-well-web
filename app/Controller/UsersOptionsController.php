<?php
App::uses('AppController', 'Controller');
/**
 * UsersOptions Controller
 *
 * @property UsersOption $UsersOption
 * @property PaginatorComponent $Paginator
 */
class UsersOptionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->UsersOption->recursive = 0;
		$this->set('usersOptions', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->UsersOption->exists($id)) {
			throw new NotFoundException(__('Invalid users option'));
		}
		$options = array('conditions' => array('UsersOption.' . $this->UsersOption->primaryKey => $id));
		$this->set('usersOption', $this->UsersOption->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->UsersOption->create();
			if ($this->UsersOption->save($this->request->data)) {
				$this->Flash->success(__('The users option has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The users option could not be saved. Please, try again.'));
			}
		}
		$users = $this->UsersOption->User->find('list');
		$assesments = $this->UsersOption->Assesment->find('list');
		$options = $this->UsersOption->Option->find('list');
		$this->set(compact('users', 'assesments', 'options'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->UsersOption->exists($id)) {
			throw new NotFoundException(__('Invalid users option'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UsersOption->save($this->request->data)) {
				$this->Flash->success(__('The users option has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The users option could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UsersOption.' . $this->UsersOption->primaryKey => $id));
			$this->request->data = $this->UsersOption->find('first', $options);
		}
		$users = $this->UsersOption->User->find('list');
		$assesments = $this->UsersOption->Assesment->find('list');
		$options = $this->UsersOption->Option->find('list');
		$this->set(compact('users', 'assesments', 'options'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->UsersOption->id = $id;
		if (!$this->UsersOption->exists()) {
			throw new NotFoundException(__('Invalid users option'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UsersOption->delete()) {
			$this->Flash->success(__('The users option has been deleted.'));
		} else {
			$this->Flash->error(__('The users option could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	public function user_option_insert(){
	header('Content-Type: application/json');	
        $this->autoLayout = false;
        $this->autoRender= false;
    	if($this->request->is('post')) {            
            //print_r($this->request->data);die;		
            	//$req_data_2 = json_decode($this->request->data, true);			
    		$user_id = $this->request->data['user_id'];

    		$this->loadModel('Feedback');
            $feedback_data = array('Feedback' => array(
                'user_id' => $user_id,                
                'category_id' => 1
            ));

            $this->Feedback->create();
            $this->Feedback->save($feedback_data);
		$assessment_data = json_decode($this->request->data['assesment_data'], true);	
	        foreach($assessment_data as $user_ans){
		#die(json_encode(['success' => true, 'msg'=>$user_ans]));	
	            $user_data['UsersOption'] = [
	            	'feedback_id' => $this->Feedback->id,
	                'user_id' => $user_id,
	                'assesment_id' => $user_ans['assesment_id'],
	                'option_id' => $user_ans['option_id']
	            ];		
	            $this->UsersOption->create();
	            $this->UsersOption->save($user_data);

	        }
	        die(json_encode(['success' => true, 'msg'=>'Thank you, your feedback has been submitted.']));
    	} else
    		die(json_encode(['success' => false, 'msg'=>'Something went wrong, please try again']));       

    }
    public function admin_self_assesment($question_id = null, $from = null, $to = null){ 
    	 $question = $this->UsersOption->Assesment->find('list');
    	 $this->set(compact('question','question_id'));
    }
    public function admin_option_graph_data($assessment_id = null, $from = null, $to = null, $user_id = null){
    	$this->autoRender = false;  
        $graphData = $this->UsersOption->avgFeedBackSelfAssessment($assessment_id, $from, $to, $user_id);
        $query = [
        		'conditions' => [
        			'Option.assesment_id' => $assessment_id
        		]
        	];
        $option_list = $this->UsersOption->Option->find('list', $query);  
        $modified_ans = array();      
        if(!empty($graphData)) {        
	        foreach ($graphData as $key => $new_answer) {	        	
		        $month = date('F', strtotime($new_answer['UsersOption']['created']));
		        if(empty($data[$month]))
		            $data[$month] = array();

		        if ($new_answer['UsersOption']['option_id'] == 1) {
		            $data[$month]['never'] = intval($new_answer[0]['answer_count']);
		            //$modified_ans[$key]['some_time'] = 0;
		            //$modified_ans[$key]['lot_time'] = 0;
		        } elseif ($new_answer['UsersOption']['option_id'] == 2) {
		            //$modified_ans[$key]['never'] = 0;
		            $data[$month]['some_time'] = intval($new_answer[0]['answer_count']);
		            //$modified_ans[$key]['lot_time'] = 0;
		        } else {
		            //$modified_ans[$key]['never'] = 0;
		            //$modified_ans[$key]['some_time'] = 0;
		            $data[$month]['lot_time'] = intval($new_answer[0]['answer_count']);
		        }
		    }
		    
		    foreach($data as $month => $val){
		        $modified_ans[] = array('month' => $month, 'never' => intval(@$val['never']));
		        $modified_ans[] = array('month' => $month, 'some_time' => intval($val['some_time']));
		        $modified_ans[] = array('month' => $month, 'lot_time' => intval($val['lot_time']));
		    }		    
        }
        die(json_encode(array('answer' => $modified_ans,'gItem' => $option_list)));        
    }
    public function admin_carer_response($feedback_id = null)
    {
        $this->loadModel('Category');
        $this->loadModel('User');
        $feedbacksQuestions = $this->UsersOption->find('all', array(
            'recursive' => 0,
            'conditions' => array('UsersOption.feedback_id' => $feedback_id),
            'order' => 'UsersOption.assesment_id ASC'
        ));
        //pr($feedbacksQuestions);die;
        $this->set(compact('feedbacksQuestions', 'category_id'));
        $this->render('/FeedbacksQuestions/admin_carer_response');
    }

}

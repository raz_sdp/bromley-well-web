	$(function(){
		var user = JSON.parse(localStorage.getItem('user')) ;
	    function selfAssess_data (){
			var CFData = JSON.parse(localStorage.getItem('CFData')) ;	
			
			var singleQuestion = $('#self-content .slider-con').clone();
			var selfAssess = $('#selfAssess .radio-con').clone();
			//var images = '<img class="pull-left self-icon" src="img/@icon1.png" style="width:26px !important; height:26px !important; max-width:26px !important;">';
			$('#self-content').empty();	
			$('#selfAssess').empty();

			$('.category_id:first').val(CFData.Categories[1].id);
			$('.category_id:last').val(CFData.Categories[0].id);

			$.each(CFData.Categories[1].questions, function(i,selfContent){
						// radio
						var radioCopy = selfAssess.clone().find('p:first').html(selfContent.question).end();
		//				console.log(radioCopy.find('.qustion_id'))
						radioCopy.find('.qustion_id').prop('name', 'data['+i+'][qustion_id]').val(selfContent.id).end();
						radioCopy.find('input[type="radio"]').prop('name', 'data['+i+'][answer_score]').end();
						$('#selfAssess').append(radioCopy);

				//console.log(selfContent);
			});	

			$.each(CFData.Categories[0].questions, function(i,selfContent){
				var copy = singleQuestion.clone().find('p:first').html(selfContent.question).end();
				copy.find('.qustion_id').prop('name', 'data['+i+'][qustion_id]').val(selfContent.id).end();
				copy.find('.answer_score').prop('name', 'data['+i+'][answer_score]').end();
				copy.find('.answer_cScore').prop('name', 'data['+i+'][answer_cScore]').end();
				copy.find('.answer').prop('name', 'data['+i+'][answer]').end();


				$('#self-content').append(copy);

				//console.log(selfContent);
			});
	}
	
	selfAssess_data();
	$.get(ROOT +'/notifications_users/g/'+ data.login.id ,function(CFData){
            localStorage.setItem("CFData", JSON.stringify(CFData.all_data));
            selfAssess_data();
     },'json'); 

	// TODO: must change the button class or id
	$('.Submitbtn').on('click', function(){
		$.post(ROOT + '/feedbacks/feedback_submission/' + user.id,$(this).closest('.tab-pane').find('input, textarea').serialize(), function(data){
			if(data.success)
				alert(data.msg);
			window.location.href = ROOT + '/CRF/home.html';
		}, 'json');
	});
	$('.sidespass').on('click', function(){
		$('.self p').hide();
	});
	$('.faltu').on('click', function(){
		$('.self p').show();
	});
})
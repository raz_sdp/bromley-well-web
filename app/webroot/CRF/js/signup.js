$(function(){
     $('div#signUpForm form').validate({
        errorPlacement: function() {
            return false;
        }
     });
	$('div#signUpForm form').on('submit', function(e) {
		e.preventDefault();
        if(! $(this).valid()) return false;
        var url = ROOT + '/users/register';
        var formData = $('div#signUpForm form').serialize();
        var successFn = function(data) {
                if (data.success) {
                    localStorage.setItem("user", JSON.stringify(data.user));
                    $.get(ROOT +'/notifications_users/g/'+ data.user.id ,function(CFData){

                    localStorage.setItem("CFData", JSON.stringify(CFData.all_data));
                    window.location.href = 'home.html';
                    },'json');                   

                } else {
                    alert(data.msg);
                }
    };
        $.post(url, formData, successFn, 'json');
});
})
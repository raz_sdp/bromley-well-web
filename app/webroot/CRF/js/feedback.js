$(function(){
	var user = JSON.parse(localStorage.getItem('user')) ;
    function feedback_data (){
		var CFData = JSON.parse(localStorage.getItem('CFData')) ;	
		$('#Complaint_user_id').val(user.id);
		$('#Complaint_user_carerid').val(user.carerid);
		
		
		 var sliderContent = $('#sliderCon .s-con').clone();
		 var responsContetn = $('.YoungCarer .t-con').clone();
		 //var images = '<img class="pull-left self-icon" src="img/@icon.png">';
		//	var submit_link = window.location.href = 'http://localhost/carers/Carers/cake/CRF/home.html';
			$('#sliderCon').empty();
			$('.YoungCarer').empty();

		$('.category_id:first').val(CFData.Categories[2].id);

		
		$.each(CFData.Categories[2].questions, function(i,selfContent){
			if(selfContent.ans_type == 'numeric'){

				var sliderScroe = sliderContent.clone().find('p:first').html(selfContent.question).end();
	//				console.log(radioCopy.find('.qustion_id'))
					sliderScroe.find('.qustion_id').prop('name', 'data['+i+'][qustion_id]').val(selfContent.id).end();
					sliderScroe.find('.answer_score').prop('name', 'data['+i+'][answer_score]').end();
					$('#sliderCon').append(sliderScroe);

			//console.log(selfContent);
			} else if(selfContent.ans_type == 'text') {

					var textField = responsContetn.clone().find('p:first').html(selfContent.question).end();
	//				console.log(radioCopy.find('.qustion_id'))
					textField.find('.qustion_id').prop('name', 'data['+i+'][qustion_id]').val(selfContent.id).end();
					textField.find('.answer').prop('name', 'data['+i+'][answer]').end();
					$('.YoungCarer').append(textField);

					console.log(selfContent);
			}

		});

}
	feedback_data();
	$.get(ROOT +'/notifications_users/g/'+ data.login.id ,function(CFData){
            localStorage.setItem("CFData", JSON.stringify(CFData.all_data));
            feedback_data();
     },'json'); 
	
	$('#feedback_btn').on('click', function(){
		$.post(ROOT + '/feedbacks/feedback_submission/' + user.id,$(this).closest('.tab-pane').find('input, textarea').serialize(), function(data){
			if(data.success)
				alert(data.msg);
			window.location.assign(ROOT + '/CRF/home.html');
		}, 'json');
	});
	
	$('#complainForm').on('submit', function(e){
		e.preventDefault();
		$.post(ROOT + '/complaints/complaint_entry',$(this).closest('.tab-pane').find('input, textarea').serialize(), function(data){
			if(data.success)
				alert(data.msg);
			window.location.assign(ROOT + '/CRF/home.html');
		}, 'json');
	});
});
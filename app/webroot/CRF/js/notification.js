$(function(){
	var user = JSON.parse(localStorage.getItem("user"));
	function notification_data (){
		var CFData = JSON.parse(localStorage.getItem('CFData')) ;
		$('#notification').empty();
		var colors = ['orange','purple','green','pink','blue','cyan'] ;
		
		$.each(CFData.Notification, function(i,notification){
			if (notification.is_deleted || notification.user_deleted) return;
			var cls = colors[i%5];
			$('#notification').append('<div class=""> <span class="date-color">'+ notification.modified +'</span><span id="delete_notification_'+ notification.id +'" class="cross"></span><p>'+ notification.notification +'</p> <hr class="hr-colr '+ cls +'"></div>')
			console.log(notification);
		});
	}
	notification_data();

	$.get(ROOT +'/notifications_users/g/'+ data.login.id ,function(CFData){
            localStorage.setItem("CFData", JSON.stringify(CFData.all_data));
            notification_data();
     },'json');   
	
	
	$('.cross').on('click',function(){
		if (confirm('Do you really want to delete the notification?')) {
			var notification_id = $(this).prop('id').split('_').pop();

			// remove it from local storage
			
			var notifications = _.filter(CFData.Notification, function(notification){
				return notification.id != notification_id;
			});

			CFData.Notification = notifications;
			localStorage.setItem('CFData', JSON.stringify(CFData));

			var data = {
				"data[NotificationsUser][user_id]" : user.id,
				"data[NotificationsUser][carerid]" : user.carerid,
				"data[NotificationsUser][notification_id]" : notification_id
			};
			$.post(ROOT+ '/notifications_users/notification_delete', data);
			$(this).parent().remove();
			alert('Successfully Deleted');
		};
		
		

	})
})
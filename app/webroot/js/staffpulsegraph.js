$(function () {
    function getData(question_id) {
        //alert(question_id);
        var loc = window.location.href.split('/');
        if (question_id === null) {
            question_id = loc.pop();
        } else {
            loc.pop();
        }
        var category_id = loc.pop();
        var user_id = loc.pop();
        var start_date = $('#from').data('mysql');
        var end_date = $('#to').data('mysql');
        var url = ROOT + 'admin/feedbacks_questions/users_feedback' + '/' + user_id + '/' + category_id + '/' + question_id + '/' +start_date + '/' + end_date; //user_id, category_id, question_id
        //var url = ROOT + 'admin/feedbacks_questions/users_feedback' + '/' + user_id + '/' + category_id + '/' + question_id; //user_id, category_id, question_id
        $.get(url,
            function (pulses) {
                var series = null;
                var title = null;
                if (category_id == 1) {
                    series = [
                        { valueField: "answer_score", name: "My current Score" },
                        { valueField: "answer_int", name: "I would like to score" }
                    ];
                    title = "Young Carers Assessment";
                // } else if (category_id == 2) {
                //     /*console.log(pulses.answer);*/
                //     series = [
                //         { valueField: "never", name: "Never" },
                //         { valueField: "some_time", name: "Some of the time" },
                //         { valueField: "lot_time", name: "A lot of the time" }
                //     ]
                } else {
                    series = { valueField: "answer_int", name: "I would like to score" };
                    title = "Young Carer Feedback Form";
                }
                $("#chartContainer").dxChart({
                    dataSource: pulses.answer,
                    commonSeriesSettings: {
                        argumentField: "submission"
                    },
                    commonPaneSettings: {
                        border: {
                            visible: true
                        }
                    },
                    commonAxisSettings: {
                        grid: {
                            visible: true
                        }
                    },
                    series: series,
                    tooltip: {
                        enabled: true
                    },
                //    title: title,
                    legend: {
                        verticalAlignment: "bottom",
                        horizontalAlignment: "center"
                    }
                });
                update_pulses_graph(pulses.answer);
            },
            'json');
    }

    function update_pulses_graph(pulses) {
        $('#chartContainer').dxChart('instance').option({
            dataSource: pulses
        });
    }

    getData(null);
    $('#question').on('change',
        function () {
            getData($(this).val());
            //getData($('#question').val());
        });
    $('#show').on('click',
        function () {
            //getData($(this).val());
            getData($('#question').val());
        });
});
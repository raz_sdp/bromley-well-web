$(function () {
    function getData(question_id) {
        //alert(question_id);
        var loc = window.location.href.split('/');
        if (question_id === null) {
            question_id = loc.pop();
        } else {
            loc.pop();
        }
        var category_id = loc.pop();
        var user_id = loc.pop();
        var start_date = $('#from').data('mysql');
        var end_date = $('#to').data('mysql');
        var url = ROOT + 'admin/feedbacks_questions/users_feedback' + '/' + user_id + '/' + category_id + '/' + question_id + '/' +start_date + '/' + end_date; //user_id, category_id, question_id
        $.get(url,
            function (pulses) {
                // var series = null;
                // if (category_id == 1) {
                //     series = [
                //         { valueField: "answer_score", name: "My current Score" },
                //         { valueField: "answer_int", name: "I would like to score" }
                //     ];
                // } else if (category_id == 2) {
                //     /*console.log(pulses.answer);*/
                //     series = [
                //         { valueField: "never", name: "Never" },
                //         { valueField: "some_time", name: "Some of the time" },
                //         { valueField: "lot_time", name: "A lot of the time" }
                //     ]
                // } else {
                //     series = { valueField: "answer_int", name: "I would like to score" };
                // }
                highAverage = 2.9,
                lowAverage = 1.1;
                //console.log(valueField);
                $("#caringBar").dxChart({
                    dataSource: pulses.answer,
                    commonSeriesSettings: {
                        argumentField: "submission",
                        valueField: 'answer_data',
                        //name: 'answer_data',
                        type: "bar"
                    },
                    palette: 'soft',
                    customizePoint: function() {
                    //    console.log(this);
                        if(this.value == 'A lot of the time') {
                            return { color: '#ff4500', hoverStyle: { color: '#ff4500' } };
                        } else if(this.value == 'Some of the time') {
                            return { color: '#00ced1', hoverStyle: { color: '#00ced1' } };
                        }
                    },
                    customizeLabel: function() {
                        //console.log(this.value);
                        // if (this.value == 'A lot of the time') {
                        //     return {
                        //         visible: true,
                        //     //    backgroundColor: '#ff4500',
                        //         customizeText: function () {
                        //             return 'A lot of the time'
                        //         }
                        //     }
                        // } else if(this.value == 'Some of the time'){
                        //      return {
                        //         visible: true,
                        //     //    backgroundColor: '#920031',
                        //         customizeText: function () {
                        //             return 'Some of the time'
                        //         }
                        //     }
                        // } else {
                        //       return {
                        //         visible: true,
                        //     //    backgroundColor: '#00ced1',
                        //         customizeText: function () {
                        //             return 'Never'
                        //         }
                        //     }
                        // }
                    },
                    valueAxis: {
                        min: 0,
                        label: {
                            customizeText: function() {
                                return this.valueText // + '&#176F'
                            }
                        },
                        constantLines: [{
                        //     label: {
                        //         text: 'Low Average'
                        //     },
                        //     width: 2,
                        //     value: lowAverage,
                        //     color: '#00ced1',
                        //     dashStyle: 'dash'
                        // }, {
                        //     label: {
                        //         text: 'High Average'
                        //     },
                        //     width: 2,
                        //     value: highAverage,
                        //     color: '#ff4500',
                        //     dashStyle: 'dash'
                        // 
                        }]
                    },
                    series: [{}],
                    title: {
                        //text: "How Caring Affects Me"
                    },
                    legend: {
                        visible: false
                    }
                });
            //    update_pulses_graph(pulses.answer);
            },
            'json');
    }

    // function update_pulses_graph(pulses) {
    //     $('#caringBar').dxChart('instance').option({
    //         dataSource: pulses
    //     });
    // }

    getData(null);
    $('#question').on('change',
        function () {
            getData($(this).val());
            //getData($('#question').val());
        });
      $('#show').on('click',
        function () {
            //getData($(this).val());
            getData($('#question').val());
        });
});
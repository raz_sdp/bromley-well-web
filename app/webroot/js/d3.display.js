 $(function(){ 
  var fill = d3.scale.category20();
  var width = $(window).width();
  var w = 700;
  if(width <1000) w = 700;
  d3.layout.cloud().size([w, 800])
      .words(allWords)
      .padding(5)
      .rotate(function() { return (Math.random() > .5 ? 1 : -1) * 60 * Math.random(); })
      //.rotate(function() { return Math.floor(Math.random() * -60) + (60);})
      .font("Impact")
      .fontSize(function(d) { return d.size; })
      .on("end", draw)
      .start();

  function draw(words) {
    d3.select("div#wordcloud").append("svg")
       .attr("width", w)
       .attr("height", 800)
      // .attr("x", function(d) { return ~~(Math.random() * 2) * 60; })
      // .attr("y", function(d) { return ~~(Math.random() * 2) * -60; })
      .append("g")
        .attr("transform", "translate("+(w/2)+",400)")
      .selectAll("text")
        .data(words)
      .enter().append("text")
        .style("font-size", function(d) { return d.size + "px"; })
        .style("font-family", "Impact")
        .style("fill", function(d, i) { return fill(i); })
        .attr("text-anchor", "middle")
        .attr("transform", function(d) {
          return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
        })
        .text(function(d) { return d.text; });
  }
});
$(function() {
    function getData(department_id, position_id) {
        var url = ROOT + 'admin/feedbacks_questions/graph_data';
        $.get(url,
        function(pulses) {
            //var dataSource = pulses; //alert(pulses);
            
        $("#chartIncidentGraph").dxChart({
            dataSource: pulses.submission,
         
            series: {
                argumentField: "month",
                valueField: "result",
                name: "Responses",
                type: "bar",
                color: '#920031'
            },
            title: "No. of Responses by Month"
        });
        update_pulses_graph(pulses.submission);
        },
        'json');
    }
    function update_pulses_graph(pulses) {
        $('#chartIncidentGraph').dxChart('instance').option({
            dataSource: pulses
        });
    }
    getData();
    $('#go').on('click',
    function() {
        getData($('#department_id').val(), $('#position_id').val());
    });
});
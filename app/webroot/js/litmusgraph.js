$(function() {
    var monthNames = [ "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December" ];
    var dt = new Date(); 
    function currentdate(){        
        var d = dt.getDate();
        var m = monthNames[dt.getMonth()];
        var y = dt.getFullYear();
		var dateString = m + ', ' + y;
        //if(d===1) var dateString = m + ' ' +  d + 'st,' + ' ' + y;
        //else if(d===2) var dateString = m + ' ' +  d + 'nd,' + ' ' + y;
        //else if(d===3) var dateString = m + ' ' +  d + 'rd,' + ' ' + y;
        //else var dateString = m + ' ' +  d + 'th,' + ' ' + y;
        return dateString;
    }

    function getQuarter() {
        var q = [1,2,3,4];
        var quater = q[Math.floor(dt.getMonth() / 3)];
        var y = dt.getFullYear();
        var quaterString = 'Quarter ' +  quater + ', ' + y;
        return quaterString;
    }

    function getData(department_id, position_id) {
        var loc = window.location.href.split('/');
        if(question_id === null) {
            question_id = loc.pop();
        } else {
            loc.pop();
        }
        var category_id = loc.pop();
        var user_id = loc.pop();
        var url = ROOT + 'admin/feedbacks_questions/users_feedback' + '/' + user_id + '/' + category_id + '/' + question_id; //user_id, category_id, question_id
        $.get(url,
        function(litmus) {
            //alert(litmus);
            //$('#litmusPieChartContainer').empty();
            $("#litmusPieChartContainer").dxPieChart({
                dataSource: litmus,
                series: {
                    argumentField: 'c',
                    valueField: 'p',
                    type: 'doughnut',
                    label: {
                        visible: true,
                        connector: {
                            visible: true
                        },
                        format: 'largeNumber',
                        precision: 0
                    }
                },
                title: 'Total No. of Responses',
                 legend: {
                    verticalAlignment: "bottom",
                    horizontalAlignment: "center",
                    itemTextPosition: "right"
                },

                palette: 'Ocean',
                customizeLabel: function(point) {
                    if (point.value > 500000000) {
                        return {
                            backgroundColor: 'deepskyblue'
                        }
                    }
                }
            });
            update_litmus_graph(litmus);
        },
        'json');
    }

    function showData(department_id, position_id){ 
        var url = ROOT + 'litmustestsUsers/issues';
        if (department_id) url += '/' + department_id + '/' + position_id;
        $.get(url, function(issues){
            $.each(issues, function(i,issue){
                if(issue.type == 'monthly'){
                    $('#currentMnth').empty();
                    $('#currentMnth').append('<p>' +  currentdate() + '</p><p>' + issue.issues + ' responses </p>');
                }
                if(issue.type == 'quarterly'){
                    $('#quarter').empty();
                    $('#quarter').append('<p>' +  getQuarter() + '</p><p>' + issue.issues + ' responses </p>');
                }
                if(issue.type == 'yearly'){
                    $('#yearly').empty();
                    $('#yearly').append('<p> Year to Date ' + dt.getFullYear() + '</p><p>' +issue.issues + ' responses </p>');
                }
                if(issue.type == 'total'){
                    $('#total').empty();
                    $('#total').append('<p> Total responses,' + '</p><p>' +issue.issues + ' responses </p>');
                }
               
            });
        },'json');
    }

    function update_litmus_graph(litmus) {
        $('#litmusPieChartContainer').dxPieChart('instance').option({
            dataSource: litmus
        });
    }

    showData();
    getData(null);
    $('#question').on('change',
        function() {
            getData($(this).val());
        });
});
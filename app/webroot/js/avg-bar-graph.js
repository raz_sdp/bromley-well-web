/**
 * Created by Raaz on 6/21/14.
 */
$(function() {
    //$.get(ROOT + 'litmustests_users/area',
    function getData(question_id) {
        var loc = window.location.href.split('/');
        
        var start_date = $('#from').data('mysql');
        var end_date = $('#to').data('mysql');

        var category_id = $('#js_category_id').val();
        if(!category_id) {
            category_id = loc.pop();
        }
        if(category_id=='3') {   
        console.log(question_id)          
            if(!question_id) {
                question_id = $('#js_question_id').val();
            }     

            var user_id = $('#js_user_id').val();
            if(user_id) {
                var url = ROOT + 'admin/feedbacks_questions/avg_feedback' + '/' + category_id + '/' + question_id + '/' + start_date + '/' + end_date+'/'+user_id; //category_id, question_id
            } else {
                var url = ROOT + 'admin/feedbacks_questions/avg_feedback' + '/' + category_id + '/' + question_id + '/' + start_date + '/' + end_date; //category_id, question_id
            }        
        } else {
            if(question_id === null) {
                question_id = loc.pop();
            } else {
                loc.pop();
            }
            var category_id = loc.pop();
            var url = ROOT + 'admin/feedbacks_questions/avg_feedback' + '/' + category_id + '/' + question_id + '/' + start_date + '/' + end_date; //category_id, question_id            
        }
        
        $.get(url,
            function(litmus) {
                var dataSource = litmus; //alert(litmus);
                if(category_id == 1){
                    var series = [{ valueField: "answer_score", name: "My current Score" },{ valueField: "answer_int", name: "I would like to score" }];
                    var palette_name = 'Ocean';
                    var title = "Average response by Month";
                }else if (category_id == 2) {
                    var title = "No. of Responses by Month";
                    /*console.log(pulses.answer);*/
                    series = [
                        { valueField: "never", name: "Never" },
                        { valueField: "some_time", name: "Some of the time" },
                        { valueField: "lot_time", name: "A lot of the time" }
                    ];
                }else{
                    var series = { valueField: "answer_int", name: "My response" };
                    var palette_name = 'Ocean_new';
                    var title = "Average response by Month";
                }
                $("#avg-bar-graph").dxChart({
                    dataSource: litmus.answer,
                    commonSeriesSettings: {
                        argumentField: "month",
                        type: "bar",
                        hoverMode: "allArgumentPoints",
                        selectionMode: "allArgumentPoints",
                        label: {
                            visible: true,
                            format: "fixedPoint",
                            precision: 2
                        }
                    },
                    series: series,
                    legend: {
                        verticalAlignment: "bottom",
                        horizontalAlignment: "center",
                        itemTextPosition: "right"
                    },
                    title: title,
                    palette: palette_name
                    // tooltip: {
                    //     enabled: true,
                    //     customizeText: function() {
                    //         return this.percentText + " - " + this.valueText;
                    //     }
                    // }
                });
                update_litmus_graph(litmus.answer);
            },
            'json');
    }
    function update_litmus_graph(litmus) {
        $('#avg-bar-graph').dxChart('instance').option({
            dataSource: litmus
        });
    }
    getData(null);
    $('#question').on('change',
        function() {
            getData($(this).val());
        });
    $('#show').on('click',
        function () {
            //getData($(this).val());
            getData($('#question').val());
        });
});
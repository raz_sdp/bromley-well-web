/**
 * Created by Raaz on 6/21/14.
 */
$(function() {
    //$.get(ROOT + 'litmustests_users/area',
    function getData(question_id) {
        var loc = window.location.href.split('/');
        if(question_id === null) {
            //question_id = loc.pop();
            question_id = $('#question_id').val();
        } else {
            loc.pop();
        }
        var category_id = 2;
        var start_date = $('#from').data('mysql');
        var end_date = $('#to').data('mysql');
        var user_id = $('#user_id').val();
        if(user_id!='') {
            var url = ROOT + 'admin/users_options/option_graph_data' + '/' + question_id + '/' + start_date + '/' + end_date+'/'+user_id; //category_id, question_id
        } else {
            var url = ROOT + 'admin/users_options/option_graph_data' + '/' + question_id + '/' + start_date + '/' + end_date; //category_id, question_id
        }
        $.get(url,
            function(litmus) {
                var dataSource = litmus; //alert(litmus);                
                var item = Object.values(litmus.gItem);

                var title = "No. of Responses by Month";
                /*console.log(pulses.answer);*/
                series = [
                    { valueField: "never", name: item[0] },
                    { valueField: "some_time", name: item[1] },
                    { valueField: "lot_time", name: item[2] }
                ];
                $("#assessment-avg-bar-graph").dxChart({
                    dataSource: litmus.answer,
                    commonSeriesSettings: {
                        argumentField: "month",
                        type: "bar",
                        hoverMode: "allArgumentPoints",
                        selectionMode: "allArgumentPoints",
                        label: {
                            visible: true,
                            format: "fixedPoint",
                            precision: 2
                        }
                    },
                    series: series,
                    legend: {
                        verticalAlignment: "bottom",
                        horizontalAlignment: "center",
                        itemTextPosition: "right"
                    },
                    title: title,
                    //palette: palette_name
                    // tooltip: {
                    //     enabled: true,
                    //     customizeText: function() {
                    //         return this.percentText + " - " + this.valueText;
                    //     }
                    // }
                });
                update_litmus_graph(litmus.answer);
            },
            'json');
    }
    function update_litmus_graph(litmus) {
        $('#assessment-avg-bar-graph').dxChart('instance').option({
            dataSource: litmus
        });
    }
    getData(null);
    $('#question').on('change',
        function() {
            getData($(this).val());
        });
    $('#show').on('click',
        function () {
            //getData($(this).val());
            getData($('#question').val());
        });
});
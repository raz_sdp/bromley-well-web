$(function() {
    function getData() {
        var url = ROOT + 'admin/feedbacks_questions/graph_data';
        $.get(url,
        function(pulses) {
            var dataSource = pulses; //alert(pulses);
         // console.log(pul.avgavg);
            var avgIncident = new Array();
            $.each(pulses.avg, function(key,sumData){
               var total = sumData;
                avgIncident.push({
                    'result' : total,
                    'month' : sumData.key
                });
               console.log(total);
            });//console.log(avgIncident);                

           //return update_pulses_graph(avgIncident);
            
        $("#chartIncident").dxChart({
            dataSource: avgIncident,
            series: {
                argumentField: "month",
                valueField: "result",
                name: "Avg.Score",
                type: "bar",
                color: '#920031'
            },
            title: "Avg. Overall Score by Month"
        });
        update_pulses_graph(avgIncident);
        },
        'json');
    }
    function update_pulses_graph(pulses) {
        console.log(pulses);
        $('#chartIncident').dxChart('instance').option({
            dataSource: pulses
        });
    }
    getData();
    $('#go').on('click',
    function() {
        getData($('#department_id').val(), $('#position_id').val());
    });
});
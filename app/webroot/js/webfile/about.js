$(function(){
	var CFData = JSON.parse(localStorage.getItem('CFData')) ;	

	$('.text-content p').html(CFData.Information[0].about_company.replace("\n","<br>")) ;	
	$("#contactUs").attr("href", "mailto:" + CFData.Information[0].contact_email);
	
	$('.readmore').on('click',function(){
		if ($(this).hasClass('AboutUs')) {
			$('.text-content p').html(CFData.Information[0].about_company.replace("\n","<br>")) ;
			$(this).text('Read our Terms of Use').removeClass('AboutUs');
		} else {
			$('.text-content p').html(CFData.Information[0].term_of_use.replace("\n","<br>")) ;
			$(this).text('About Us').addClass('AboutUs');
		}
	});
	
})
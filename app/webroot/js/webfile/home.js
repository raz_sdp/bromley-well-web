$(function(){
	var user = localStorage.getItem("user");
	var intervalTimer = null;
	var isActive_data =  {
		success : false,
		msg : "No internet connectivity is available, please try again later."
	};

	var checkUserActivation = function(){

		var url = ROOT + '/users/check_enability/' + user.id + '/' + user.carerid;
		
		$.get(url, function(data){
			isActive_data = data;
			localStorage.setItem("isActive_data", JSON.stringify(data));
		}, 'json');
	};

	if (!user) {
		window.location.href = 'index.html';
	} else {
		user = JSON.parse(user);
		checkUserActivation();
		intervalTimer = setInterval(checkUserActivation, 30000);
	}


	$('.hexagon a').on('click', function(e){
		e.preventDefault();
		if (isActive_data.success) {
			window.location.href = $(this).attr('href');
		} else {
			alert(isActive_data.msg);
		}
	});
});
$(function(){
     $('div#loginForm form').validate({
        errorPlacement: function() {
            return false;
        }
     });
	$('div#loginForm form').on('submit', function(e) {
		e.preventDefault();
        if(! $(this).valid()) return false;

        var url = ROOT + '/users/login';
        var formData = $('div#loginForm form').serialize();

        var successFn = function(data) {
                if (data.success) {
                    localStorage.setItem("user", JSON.stringify(data.login));
                    $.get(ROOT +'/notifications_users/g/'+ data.login.id ,function(CFData){
                    localStorage.setItem("CFData", JSON.stringify(CFData.all_data));  
                    window.location.href = 'home';                                     
                    },'json');                   
                    
                } else {
                    alert(data.msg);
                }
    };
        $.post(url, formData, successFn, 'json');
});
})
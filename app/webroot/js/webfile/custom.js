function customRadio() {
        var radioButton = $('input[type="radio"]');
        $(radioButton).each(function() {
            $(this).wrap("<span class='custom-radio'></span>");
            if ($(this).is(':checked')) {
                $(this).parent().addClass("selected");
            }
        });
        $(radioButton).click(function() {
            if ($(this).is(':checked')) {
                $(this).parent().addClass("selected");
            }
            $(this).closest('.text-center').find('input[type="radio"]').not(this).each(function() {
                $(this).parent().removeClass("selected");
            });
        });
    }
$(function() {
    
    $(document).ready(function() {
        customRadio();
    });
    $('.addrespon').on('click', function() {
            $(this).hide().prev('.YoungCarerTextArea').show();
            // set current textarea value to "Cancel" button data value, so that it can be restored when cancel btn is clicked
            $(this).prev().find('.cancel-respon').data('oldValue', $(this).prev().find('.answer').val());
            return false;
        });
        $('.cancel-respon, .save-respon ').on('click', function() {
        $(this).parent().parent().hide().next().show();

        // Cancel - restore old value
        if($(this).hasClass('cancel-respon'))
            $(this).parent().prev('.answer').val($(this).data('oldValue'));

        var tickClass = 'tick';
        if( $(this).parent().parent().next('.addrespon').hasClass('how') )
            tickClass = 'tickHow';

        if($.trim( $(this).parent().prev('.answer').val() ).length > 0){
            $(this).parent().parent().next(".addrespon").find('span').removeClass("glyphicon-plus");
            $(this).parent().parent().next('.addrespon').addClass(tickClass);
        } else {
            $(this).parent().prev('.answer').val('');
            $(this).parent().parent().next(".addrespon").find('span').addClass("glyphicon-plus");
            $(this).parent().parent().next('.addrespon').removeClass(tickClass);
        }
        return false;
    });
    $(".slider").slider({
        range: "max",
        min: 1,
        max: 10,
        value: 1,
        slide: function(event, ui) {
           $(event.target).next().val(ui.value);
        }
    });
    
    //$("#amount").val($("#slider-range-max").slider("value"));
    //reload
   
})
var autoSubmitOnUploadComplete = false;
var isUploading = false;
$(function() {

  $('#DocumentFilex').on('change', function(e) {
      readfiles(this.files);
    });
  var holder = $('.file-inputbg'), // document.getElementById('holder'),
  tests = {
    filereader: typeof FileReader != 'undefined',
    dnd: 'draggable' in document.createElement('span'),
    formdata: !!window.FormData,
    progress: "upload" in new XMLHttpRequest
  },
  support = {
    filereader: $('#filereader'),
    formdata: $('#formdata'),
    progress: $('#progress')
  },
  acceptedTypes = {
    'files/pdf': true
  },
  progress = $('#uploadprogress'),
  fileupload = $('#upload'); //document.getElementById('upload');
  "filereader formdata progress".split(' ').forEach(function(api) {
    if (tests[api] === false) {
      support[api].addClass('fail');
    } else { // FFS. I could have done el.hidden = true, but IE doesn't support
      // hidden, so I tried to create a polyfill that would extend the
      // Element.prototype, but then IE10 doesn't even give me access
      // to the Element object. Brilliant.
      support[api].addClass('hidden');
    }
  });
  function previewfile(file) {
    if (tests.filereader === true && acceptedTypes[file.type] === true) {
      var reader = new FileReader();
      reader.onload = function(event) {
        var image = new Image();
        image.src = event.target.result;
        image.width = 250; // a fake resize
        $('.preview').empty().append($(image));
      };
      reader.readAsDataURL(file);
    } else {
      holder.append('<p>Uploaded ' + file.name + ' ' + (file.size ? (file.size / 1024 | 0) + 'K': '') + '</p>' );
      console.log(file);
    }
  }
  function beforeSendHandler(e){
    isUploading = true;
  }
  function progressHandlingFunction(e){
    isUploading = true;
    $('.up').show();
    if (e.lengthComputable) {
      var complete = (e.loaded / e.total * 100 | 0);
      progress.val(complete).html(complete);
    }
  }

  function completeHandler(e){
    //console.log(JSON.parse(e));
    var res = JSON.parse(e);
    $('.up').hide();
    $('#DocumentFilename').val(res.filename);
    isUploading = false;
    progress.val(100).html(100);
    if(autoSubmitOnUploadComplete=== true)
      $('.dndFileUpload').submit();

  }
  function errorHandler(e){
    isUploading = false;
    $('.up').hide();
  }
  function readfiles(files) {
    //debugger;
    var formData = tests.formdata ? new FormData() : null;
    //formData.append('data[Document][file_upload]', '1');
    //for (var i = 0; i < files.length; i++) {
      if (tests.formdata) formData.append('data[Document][filex]', files[0]);
      //formData.append('data[Document][title]', $('#DocumentTitle').val());
      formData.append('data[Document][file_upload]', '1');
      //formData.append('data[Document][company_id]', '1');
      previewfile(files[0]);
    //} // now post a new XHR request
    if (tests.formdata) {

      $.ajax({
        url: location.href,  //Server script to process data
        type: 'POST',
        xhr: function() {  // Custom XMLHttpRequest
            var myXhr = $.ajaxSettings.xhr();
            if(myXhr.upload){ // Check if upload property exists
                myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
            }
            return myXhr;
        },
        //Ajax events
        beforeSend: beforeSendHandler,
        success: completeHandler,
        error: errorHandler,
        // Form data
        data: formData,
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false
    });

    /*
      var xhr = new XMLHttpRequest();
      xhr.open('POST', location.href);
      xhr.onload = function() {
        progress.value = progress.innerHTML = 100;
        alert('Uploaded Successfully. Please refresh.');
      };
      if (tests.progress) {
        xhr.upload.onprogress = function(event) {
          if (event.lengthComputable) {
            var complete = (event.loaded / event.total * 100 | 0);
            progress.value = progress.innerHTML = complete;
          }
        }
      }
      xhr.send(formData);*/

    }
  }

  if (tests.dnd) {
    holder.on('dragover', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).addClass('hover');
    }).on('dragend', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).removeClass('hover');
    }).on('drop', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).removeClass('hover');
      //readfiles(e.dataTransfer.files);
      readfiles(e.originalEvent.dataTransfer.files);
    });
  } else {
    fileupload.removeClass('hidden').find('input').on('change', function(e) {
      readfiles(this.files);
    });
  }


  // restrict form submit while uploading..
  $('.dndFileUpload').on('submit', function(e){
    if(isUploading === true) {
      $('.uploading').show();
      e.preventDefault();
      autoSubmitOnUploadComplete = true;
    }
  });
});
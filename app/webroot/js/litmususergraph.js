$(function () {
    //$.get(ROOT + 'litmustests_users/area',
    function getData(question_id) {
        var loc = window.location.href.split('/');
        if (question_id === null) {
            question_id = loc.pop();
        } else {
            loc.pop();
        }
        var category_id = loc.pop();
        var user_id = loc.pop();
        var start_date = $('#from').data('mysql');
        var end_date = $('#to').data('mysql');
        var url = ROOT + 'admin/feedbacks_questions/users_feedback' + '/' + user_id + '/' + category_id + '/' + question_id + '/' +start_date + '/' + end_date; //user_id, category_id, question_id
        //var url = ROOT + 'admin/feedbacks_questions/users_feedback' + '/' + user_id + '/' + category_id + '/' + question_id; //user_id, category_id, question_id
        $.get(url,
            function (litmus) {
                var series = null; //alert(litmus);
                var palette_name = null;
                var visibility = null;
                var title =  null;
                if (category_id == 1) {
                    series = [
                        { valueField: "answer_score", name: "My current Score" },
                        { valueField: "answer_int", name: "I would like to score" }
                    ];
                    palette_name = 'Ocean';
                    visibility = true;
                    title = "Young Carers Assessment";

                // } else if (category_id == 2) {
                //     /*console.log(pulses.answer);*/
                //     series = [
                //         { valueField: "never", name: "Never" },
                //         { valueField: "some_time", name: "Some of the time" },
                //         { valueField: "lot_time", name: "A lot of the time" }
                //     ];
                //     visibility = false;
                } else {
                    series = { valueField: "answer_int", name: "I would like to score" };
                    palette_name = 'Ocean_new';
                    visibility = true;
                    title = "Young Carer Feedback Form";

                }
                $("#litmusFullStackedBar").dxChart({
                    dataSource: litmus.answer,
                    commonSeriesSettings: {
                        argumentField: "submission",
                        type: "bar",
                        hoverMode: "allArgumentPoints",
                        selectionMode: "allArgumentPoints",
                         label: {
                             visible: visibility,
                        //     visible: true,
                             format: "fixedPoint",
                             precision: 0
                         }
                    },
                    commonAxisSettings: {
                        grid: {
                            visible: true
                        }
                    },
                    series: series,
                    legend: {
                        verticalAlignment: "bottom",
                        horizontalAlignment: "center",
                        itemTextPosition: "right"
                    },
                //    title: title,
                    palette: palette_name
                    // tooltip: {
                    //     enabled: true,
                    //     customizeText: function() {
                    //         return this.percentText + " - " + this.valueText;
                    //     }
                    // }
                });
                update_litmus_graph(litmus.answer);
            },
            'json');
    }

    function update_litmus_graph(litmus) {
        $('#litmusFullStackedBar').dxChart('instance').option({
            dataSource: litmus
        });
    }

    getData(null);
    $('#question').on('change',
        function () {
            getData($(this).val());
        });
    $('#show').on('click',
        function () {
            //getData($(this).val());
            getData($('#question').val());
        });
});
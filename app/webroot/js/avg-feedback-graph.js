/**
 * Created by Raaz on 6/21/14.
 */
$(function () {
    function getData(question_id) {
        //alert(question_id);
        var loc = window.location.href.split('/');
        if(question_id === null) {
            question_id = loc.pop();
        } else {
            loc.pop();
        }
        var category_id = loc.pop();
        var start_date = $('#from').data('mysql');
        var end_date = $('#to').data('mysql');
        var url = ROOT + 'admin/feedbacks_questions/avg_feedback' + '/' + category_id + '/' + question_id + '/' + start_date + '/' + end_date; //category_id, question_id
        //var url = ROOT + 'admin/feedbacks_questions/avg_feedback'  + '/' + category_id + '/' + question_id; //category_id, question_id
        console.log(category_id);
        $.get(url,
            function (pulses) {
                console.log(pulses);
                var dataSource = pulses;
                if(category_id == 1){
                    var series = [{ valueField: "answer_score", name: "My current Score" },{ valueField: "answer_int", name: "I would like to score" }];
                }
                else if(category_id == 3){
                    var series = { valueField: "feedback_count", name: "My response" };
                    var title = "Number of responses by Month";
                }
                else{
                    var series = { valueField: "answer_int", name: "My response" };
                    var title = "Average response by Month";
                }
                $("#avg-chartContainer").dxChart({
                    dataSource: pulses.answer,
                    commonSeriesSettings: {
                        argumentField: "month"
                    },
                    commonPaneSettings: {
                        border: {
                            visible: true
                        }
                    },
                    commonAxisSettings: {
                        grid: {
                            visible: true
                        }
                    },
                    series: series,
                    tooltip:{
                        enabled: true
                    },
                    title: title,
                    legend: {
                        verticalAlignment: "bottom",
                        horizontalAlignment: "center"
                    }
                });
                update_pulses_graph(pulses.answer);
            },
            'json');
    }
    function update_pulses_graph(pulses) {
        $('#avg-chartContainer').dxChart('instance').option({
            dataSource: pulses
        });
    }
    getData(null);
    $('#question').on('change',
        function() {
            getData($(this).val());
            //getData($('#question').val());
        });
    $('#show').on('click',
        function () {
            //getData($(this).val());
            getData($('#question').val());
        });
});
$(function() {
    //$.get(ROOT + 'litmustests_users/area',
    function getData(department_id, position_id) {
        var url = ROOT + 'litmustests_users/area';
        if (department_id) url += '/' + department_id + '/' + position_id;
        $.get(url,
        function(litmus) {
            var dataSource = litmus; //alert(litmus);
            if (department_id) return update_litmus_graph(litmus);

            $("#litmuschartContainer").dxChart({
            title: "No. of Responses by Month",
            dataSource: dataSource,
            commonSeriesSettings: {
                type: "stackedArea",
                argumentField: "yr"
            },
            argumentAxis:{
                valueMarginsEnabled: false,
                 grid:{
                    visible: true
                }
            },
            series: [
                { valueField: "y", name: "YES" },
                { valueField: "n", name: "NO" }
            ],
            palette: 'Ocean',
            legend: {
                verticalAlignment: "bottom",
                horizontalAlignment: "center"
            }
        });
        update_litmus_graph(litmus);
        },
        'json');
    }
    function update_litmus_graph(litmus) {
        $('#litmuschartContainer').dxChart('instance').option({
            dataSource: litmus
        });
    }
    getData();
    $('#go').on('click',
    function() {
        getData($('#department_id').val(), $('#position_id').val());
    });
});
<div class="prequalifyingQuestions form">
<?php echo $this->Form->create('PrequalifyingQuestion'); ?>
	<fieldset>
		<legend><?php echo __('Add Prequalifying Question'); ?></legend>
	<?php
		echo $this->Form->input('question');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Prequalifying Questions'), array('action' => 'index')); ?></li>
	</ul>
</div>

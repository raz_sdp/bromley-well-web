<div class="prequalifyingQuestions view">
<h2><?php echo __('Prequalifying Question'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($prequalifyingQuestion['PrequalifyingQuestion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo h($prequalifyingQuestion['PrequalifyingQuestion']['question']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($prequalifyingQuestion['PrequalifyingQuestion']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($prequalifyingQuestion['PrequalifyingQuestion']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Prequalifying Question'), array('action' => 'edit', $prequalifyingQuestion['PrequalifyingQuestion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Prequalifying Question'), array('action' => 'delete', $prequalifyingQuestion['PrequalifyingQuestion']['id']), null, __('Are you sure you want to delete # %s?', $prequalifyingQuestion['PrequalifyingQuestion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Prequalifying Questions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prequalifying Question'), array('action' => 'add')); ?> </li>
	</ul>
</div>

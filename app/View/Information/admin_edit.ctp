<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
		<h2><?php echo __('Admin Edit Information'); ?></h2>
<?php echo $this->Form->create('Information',array('class'=>'form-horizontal')); ?>
	<fieldset>
		
	<?php
		echo $this->Form->input('id');
		?>
	<!--<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">About Us</label>
	    <div class="col-sm-9">
	    <?php /*echo $this->Form->input('about_company',array('label' => false,'class'=>'form-control')); */?>
    	</div>
    </div>	-->
    <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">Manager's Phone No</label>
	    <div class="col-sm-9">
	    <?php echo $this->Form->input('manager_phone',array('label' => false,'class'=>'form-control')); ?>
    	</div>
    </div>
    <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">Email for Contact Us</label>
	    <div class="col-sm-9">
	    <?php echo $this->Form->input('contact_email',array('label' => false,'class'=>'form-control')); ?>
    	</div>
    </div>	
    <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">Term of Use</label>
	    <div class="col-sm-9">
	    <?php echo $this->Form->input('term_of_use',array('label' => false,'class'=>'form-control')); ?>
	    <button type="submit" class="btn submit-green s-c">Submit</button>
    	</div>
    </div>
	</fieldset>
<?php echo $this->Form->end(); ?>
</div>
</div>
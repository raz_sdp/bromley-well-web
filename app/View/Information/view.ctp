<div class="information view">
<h2><?php echo __('Information'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($information['Information']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('About Company'); ?></dt>
		<dd>
			<?php echo h($information['Information']['about_company']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($information['Information']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($information['Information']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php echo $this->element('menu');?>
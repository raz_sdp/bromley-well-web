<?php echo $this->element('menu'); ?>
<div class="index col-md-10 col-sm-10">
    <div class="white">
        <div class="paging pull-right">

            <ul class="pagination pull-right">
                <?php
                echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
                echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
                ?>
            </ul>
            <p class="text-right">
                <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Showing {:start} to {:end} of {:count} entries')
                ));
                ?>
            </p>


        </div>
        <h2><?php echo __('Staff Alerts'); ?></h2>
        <table cellpadding="0" cellspacing="0" class="table">
            <tr>
                <!--                <th>--><?php //echo $this->Paginator->sort('id'); ?><!--</th>-->
                <th><?php echo $this->Paginator->sort('message'); ?></th>
                <th><?php echo $this->Paginator->sort('created', 'Date of Creation'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php foreach ($staffalerts as $staffalert): ?>
                <tr>
                    <!--                    <td>--><?php //echo h($staffalert['Staffalert']['id']); ?><!--&nbsp;</td>-->
                    <td><?php echo h($staffalert['Staffalert']['message']); ?>&nbsp;</td>
                    <td><?php echo date_format(date_create($staffalert['Staffalert']['created']), 'd M Y H:i'); ?>
                        &nbsp;</td>
                    <td><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $staffalert['Staffalert']['id'], $this->params->pass[0]), array('class' => 'btn btn-primary'), __('Are you sure you want to delete?')); ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>



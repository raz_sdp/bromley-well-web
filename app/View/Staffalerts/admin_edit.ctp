<div class="staffalerts form">
<?php echo $this->Form->create('Staffalert'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Staffalert'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('message');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Staffalert.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Staffalert.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Staffalerts'), array('action' => 'index')); ?></li>
	</ul>
</div>

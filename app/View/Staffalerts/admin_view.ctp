<div class="staffalerts view">
<h2><?php echo __('Staffalert'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($staffalert['Staffalert']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Message'); ?></dt>
		<dd>
			<?php echo h($staffalert['Staffalert']['message']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($staffalert['Staffalert']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Staffalert'), array('action' => 'edit', $staffalert['Staffalert']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Staffalert'), array('action' => 'delete', $staffalert['Staffalert']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $staffalert['Staffalert']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Staffalerts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Staffalert'), array('action' => 'add')); ?> </li>
	</ul>
</div>

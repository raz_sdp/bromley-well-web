<div class="staffalerts form">
<?php echo $this->Form->create('Staffalert'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Staffalert'); ?></legend>
	<?php
		echo $this->Form->input('message');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Staffalerts'), array('action' => 'index')); ?></li>
	</ul>
</div>

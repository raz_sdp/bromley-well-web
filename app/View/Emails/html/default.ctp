<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <tr>
        <td align="center" valign="top" bgcolor="#fe580d">
            <table border="0" cellpadding="10" cellspacing="0"  id="emailContainer">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                            <tr>
                                <td align="center" valign="top">
                                   <h3><font color="#fff">Are you a Young Carer?</h3>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php foreach ($questions as $key => $data): ?>
    <tr>
        <td align="left" valign="top" bgcolor="#fff">
            <table border="0" cellpadding="10" cellspacing="0"  id="emailContainer">
                <tr>
                    <td align="left" valign="top" style="padding:10px;">
                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                            <tr>
                                <td align="left" valign="middle">
								<img src="http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/icon1.png"  style="vertical-align: middle;" alt=""/>
                                <?php echo $data['PrequalifyingQuestion']['question']; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" bgcolor="#fff">
            <table border="0" cellpadding="10" cellspacing="0"  id="emailContainer">
                <tr>
                     <td align="center" valign="top" width="10%">
	                      <div class="radio radio-align">
						  <label>
						  	 Yes
						  	 <label></label>
						  	  <br>
						    <?php
						    if($body[$key]['answer_score'] == 1){
						    	?>
						    	<img src="<?php echo $this->Html->url('http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/radio-active.png', true); ?>" style="margin-top:10px"  alt=""/>
						    <?php }else{ ?>
						    <img src="<?php echo $this->Html->url('http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/radio.png', true); ?>" style="margin-top:10px"  alt=""/>
							<?php
								}
						    ?>
						  </label>
						</div>
	                    </td>
                    <td align="center" valign="top"  width="10%">
                      <div class="radio radio-align">
					  <label>
					  	No 
                        <label></label>
					  	  <br>
					  	  <?php
						    if($body[$key]['answer_score'] == 2){
						    	?>
						    	<img src="<?php echo $this->Html->url('http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/radio-active.png', true); ?>" style="margin-top:10px"  alt=""/>
						    <?php }else{ ?>
						    <img src="<?php echo $this->Html->url('http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/radio.png', true); ?>" style="margin-top:10px"  alt=""/>
							<?php
								}
						    ?>
					  </label>
					</div>
                    </td>
                </tr>
            </table>
        </td>
        
    </tr>
    <?php endforeach; ?>
    <tr>
        <td style="padding:10px" align="" valign="top" bgcolor="#fff">
            <table border="0" cellpadding="10" cellspacing="0"  id="emailContainer">
                <tr>
                    <td align="" valign="top">
                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                            <tr>
                                <td style="font-weight: bold;" align="" valign="top">Name</td>
                                <td><?php echo $body['name'];?></td>
                            <tr>
                            <tr>	
                                <td style="font-weight: bold;" align="" valign="top">Mobile No</td>
                                <td><?php echo $body['mobile'];?></td>
                            </tr> 
                            <tr>  
                                <td style="font-weight: bold;" align="" valign="top">Email</td>
                                <td><?php echo $body['email'];?></td>
                            </tr>
                            <tr>
                            	<td style="font-weight: bold;" align="" valign="top">Aditional Information</td>
                            </tr>
                            <tr>
                            	<td><?php echo nl2br($body['info']);?></td>

                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div class="assesments form">
<?php echo $this->Form->create('Assesment'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Assesment'); ?></legend>
	<?php
		echo $this->Form->input('question');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Assesments'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Options'), array('controller' => 'options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Option'), array('controller' => 'options', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users Options'), array('controller' => 'users_options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users Option'), array('controller' => 'users_options', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="assesments view">
<h2><?php echo __('Assesment'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($assesment['Assesment']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo h($assesment['Assesment']['question']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($assesment['Assesment']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($assesment['Assesment']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Assesment'), array('action' => 'edit', $assesment['Assesment']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Assesment'), array('action' => 'delete', $assesment['Assesment']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $assesment['Assesment']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Assesments'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Assesment'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Options'), array('controller' => 'options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Option'), array('controller' => 'options', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users Options'), array('controller' => 'users_options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users Option'), array('controller' => 'users_options', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Options'); ?></h3>
	<?php if (!empty($assesment['Option'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Assesment Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($assesment['Option'] as $option): ?>
		<tr>
			<td><?php echo $option['id']; ?></td>
			<td><?php echo $option['assesment_id']; ?></td>
			<td><?php echo $option['title']; ?></td>
			<td><?php echo $option['created']; ?></td>
			<td><?php echo $option['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'options', 'action' => 'view', $option['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'options', 'action' => 'edit', $option['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'options', 'action' => 'delete', $option['id']), array('confirm' => __('Are you sure you want to delete # %s?', $option['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Option'), array('controller' => 'options', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Users Options'); ?></h3>
	<?php if (!empty($assesment['UsersOption'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Assesment Id'); ?></th>
		<th><?php echo __('Option Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($assesment['UsersOption'] as $usersOption): ?>
		<tr>
			<td><?php echo $usersOption['id']; ?></td>
			<td><?php echo $usersOption['user_id']; ?></td>
			<td><?php echo $usersOption['assesment_id']; ?></td>
			<td><?php echo $usersOption['option_id']; ?></td>
			<td><?php echo $usersOption['created']; ?></td>
			<td><?php echo $usersOption['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users_options', 'action' => 'view', $usersOption['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users_options', 'action' => 'edit', $usersOption['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users_options', 'action' => 'delete', $usersOption['id']), array('confirm' => __('Are you sure you want to delete # %s?', $usersOption['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Users Option'), array('controller' => 'users_options', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

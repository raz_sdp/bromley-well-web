<div class="usersOptions form">
<?php echo $this->Form->create('UsersOption'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Users Option'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('assesment_id');
		echo $this->Form->input('option_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('UsersOption.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('UsersOption.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Users Options'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Assesments'), array('controller' => 'assesments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Assesment'), array('controller' => 'assesments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Options'), array('controller' => 'options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Option'), array('controller' => 'options', 'action' => 'add')); ?> </li>
	</ul>
</div>

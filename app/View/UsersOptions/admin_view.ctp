<div class="usersOptions view">
<h2><?php echo __('Users Option'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($usersOption['UsersOption']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersOption['User']['name'], array('controller' => 'users', 'action' => 'view', $usersOption['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Assesment'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersOption['Assesment']['id'], array('controller' => 'assesments', 'action' => 'view', $usersOption['Assesment']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Option'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersOption['Option']['title'], array('controller' => 'options', 'action' => 'view', $usersOption['Option']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($usersOption['UsersOption']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($usersOption['UsersOption']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Users Option'), array('action' => 'edit', $usersOption['UsersOption']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Users Option'), array('action' => 'delete', $usersOption['UsersOption']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $usersOption['UsersOption']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Users Options'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users Option'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Assesments'), array('controller' => 'assesments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Assesment'), array('controller' => 'assesments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Options'), array('controller' => 'options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Option'), array('controller' => 'options', 'action' => 'add')); ?> </li>
	</ul>
</div>

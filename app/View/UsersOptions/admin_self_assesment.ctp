<?php echo $this->Html->script(array('assessment-avg-bar-graph', 'jquery-ui'));?>
<?php 
    echo $this->Html->css(array('jquery-ui-min', 'style-ui'));
    //echo $this->Html->script(array('jquery-ui')); 
?>
<style type="text/css">
    .select-control{
        margin-bottom: -5px;
        padding: 10px 0;
    }
    /*@media print
    {
    .noprint {display:none;}

    div.form, div.index, div.view {width: 100%; float: none;}

    }*/
</style>
<script type="text/javascript">
    $(function (){
        // $('#print').on('click',
        //     function() {
        //         window.print();
        //     });
    $("#from, #to").datepicker({
            dateFormat : 'dd/mm/yy',
            onClose : function(dateText){
                $(this).data('mysql', dateText.split('/').reverse().join('-'));
            }
        });
    });
</script>
<?php echo $this->element('menu');?>
<input type="hidden" id="user_id" value="<?php echo @$this->params['pass'][2]?>">
<input type="hidden" id="question_id" value="<?php echo @$this->params['pass'][1]?>">

<div class="index col-md-10 col-sm-10">
    <div class="white">
        <h2><?php echo __(''); ?></h2>
        <div class="form-horizontal">
            <div class="form-group">
                <div class="inline-block">
                <div class="col-sm-12">
    <div class="row fo-to">
        <div class="col-sm-3">
             <div class="form-group form">
                <label for="inputEmail3" class="col-sm-4 control-label">From</label>
                <div class="col-sm-7">
                    <?php
                    $fr =  $from === null ? strtotime('-30 days') :strtotime($from);
                echo $this->Form->input('from', array('placeholder' => date('d/m/Y', $fr),'type' => 'text','label'=>false, 'data-mysql' => date('Y-m-d', $fr), 'class'=>'form-control'));
                     ?>
                  
                </div>
             </div>
        </div> 
        <div class="col-sm-3">
             <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">To</label>
                <div class="col-sm-7">
                    <?php
                    $t = $to === null ? strtotime('today') : strtotime($to);
                    echo $this->Form->input('to', array('placeholder' => date('d/m/Y', $t ), 'type' => 'text','label'=>false, 'data-mysql' => date('Y-m-d', $t),'class'=>'form-control'));
                ?> 
                  
                </div>
                <button class="col-sm-2 btn btn-info" id="show">Go</button>
             </div>
        </div> 
    </div>     
</div>
                <div class="qus">
                    <label for="inputEmail3" class="col-sm-1 control-label">Question</label>
                    <div class="col-sm-11">
                        <?php
                        echo $this->Form->input('question', array('options' => $question,
                                'selected' => $question_id, 'label'=>false,'class'=>'form-control'
                            )
                        );
                        ?>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!-- <span class="select-val col-sm-offset-1"></span> -->
        <!-- Graph Start -->
        <div class="pulsesUsers">
            <!--        avg line graph-->
<!--            <div id="avg-chartContainer"></div>-->
            <!--        avg bar graph-->
            <div id="assessment-avg-bar-graph"></div>
        </div>
    </div>
</div>
<!-- Graph End -->

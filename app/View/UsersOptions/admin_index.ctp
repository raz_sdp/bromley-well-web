<div class="usersOptions index">
	<h2><?php echo __('Users Options'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('assesment_id'); ?></th>
			<th><?php echo $this->Paginator->sort('option_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($usersOptions as $usersOption): ?>
	<tr>
		<td><?php echo h($usersOption['UsersOption']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($usersOption['User']['name'], array('controller' => 'users', 'action' => 'view', $usersOption['User']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($usersOption['Assesment']['id'], array('controller' => 'assesments', 'action' => 'view', $usersOption['Assesment']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($usersOption['Option']['title'], array('controller' => 'options', 'action' => 'view', $usersOption['Option']['id'])); ?>
		</td>
		<td><?php echo h($usersOption['UsersOption']['created']); ?>&nbsp;</td>
		<td><?php echo h($usersOption['UsersOption']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $usersOption['UsersOption']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $usersOption['UsersOption']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $usersOption['UsersOption']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $usersOption['UsersOption']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Users Option'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Assesments'), array('controller' => 'assesments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Assesment'), array('controller' => 'assesments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Options'), array('controller' => 'options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Option'), array('controller' => 'options', 'action' => 'add')); ?> </li>
	</ul>
</div>

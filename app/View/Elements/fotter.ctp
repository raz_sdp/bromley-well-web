<div class="footer-section">
				<div class="row">
					<div class="footer-con text-center">
						<small>The Bromley Well Limited is registered in England as a company limited by guarantee</small>
						<small>Registered office: 1 Beech Avenue, Sherwood Rise, Nottingham NG7 7LJ</small>
						<small>Registered No. 3123142 | Registered Charity No. 1050779</small>
						<small>© 2014 Carers Federation</small>
						<!-- <small><a href="#" class="logout text-center">Logout</a>	</small> -->
					</div>	
				</div>
			</div>
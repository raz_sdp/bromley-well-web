
<div class="actions col-md-2 col-sm-2">
	<h5  id="menu"><?php echo $this->Html->link(__('Dashboard'), array('controller' => 'users' ,'action' => '
		dashboard', 'admin' => true)); ?></h5>
	<?php 
		$controller = $this->params['controller'];
		//print_r($controller);
		$action = $this->params['action'];
		//print_r($action);
		$method = @$this->params['pass'][0];
		//print_r($method);
	?>
	<ul class="nav-menu">
		<li  class="node"><?php echo $this->Html->link(__('Carers'), array('controller' => 'users' ,'action' => '
		index', 'admin' => true)); ?></li>	
		<li  class="node<?php if($controller == 'thoughts') echo ' selected';?>"><?php echo $this->Html->link(__('Thoughts'), array('controller' => 'thoughts' ,'action' => '
		index', 'admin' => true)); ?></li>
		<li class="videonault"><?php echo $this->Html->link(__('Young Carers Assessment'), array('controller' => 'users_options', 'action' => 'self_assesment', 1,1)); ?></li>
		
		<li class="real-time"><?php echo $this->Html->link(__('How Caring Affects Me'), array('controller' => 'feedbacks_questions', 'action' => 'admin_caring', 2, 17)); ?></li>

		<li class="document"><?php echo $this->Html->link(__('Young Carer Feedback Form'), array('controller' => 'feedbacks_questions', 'action' => 'admin_young', 3, 26)); ?></li>
		<?php
			if($controller == 'feedbacks_questions' && ($action == 'admin_young' || $action == 'admin_wordcloud_young')){
		?>
		<li class="document-sub a-s<?php if($controller == 'feedbacks_questions' && $action == 'admin_young') echo ' selected';?>"><?php echo $this->Html->link(__('View Chart'), array('controller' => 'feedbacks_questions', 'action' => 'admin_young', 3, 26)); ?></li>
                <li class="wordcloud a-s<?php if($controller == 'feedbacks_questions' && $action == 'admin_wordcloud_young') echo ' selected';?>"><?php echo $this->Html->link(__('View Word Cloud'), array('controller' => 'feedbacks_questions', 'action' => 'admin_wordcloud_young', 32)); ?></li>
            <?php } ?>
<!--		<li class="notification">--><?php //echo $this->Html->link(__('Complaints'), array('controller' => 'complaints', 'action' => 'index', 'admin' => true, 'complaint')); ?><!--</li>-->
<!--		--><?php
//			if($controller == 'complaints' && $method == 'complaint' && ($action =='admin_index' || $action=='admin_view' || $action=='admin_add')){
//		?>
<!--		<li class="notification-sub a-s--><?php //if($controller == 'complaints' && ($action =='admin_index' || $action=='admin_view') && $method=='complaint') echo ' selected';?><!--">--><?php //echo $this->Html->link(__('Complaints'), array('controller' => 'complaints', 'action' => 'index', 'admin' => true ,'complaint')); ?><!--</li>-->
<!--		--><?php //} ?>
		<li class="staff_pulse"><?php echo $this->Html->link(__('DOCUMENTS'), array('controller' => 'documents', 'action' => 'index', 'document')); ?></li>
		<?php
			if($controller == 'documents' && $method == 'document' && ($action =='admin_index' || $action=='admin_edit' || $action=='admin_add')){
		?>
		<li class="staff_pulse-sub a-s<?php if($controller == 'documents' && ($action =='admin_index' || $action=='admin_edit') && $method=='document') echo ' selected';?>"><?php echo $this->Html->link(__('Documents'), array('controller' => 'documents', 'action' => 'index', 'document')); ?> </li>
		<li class="add a-s<?php if($controller == 'documents' && $action =='admin_add') echo ' selected';?>"><?php echo $this->Html->link(__('Add Documents'), array('controller' => 'documents', 'action' => 'add', 'admin' => true, 'document')); ?> </li>
		<?php } ?>
		<li class="litmus_test"><?php echo $this->Html->link(__('NOTIFICATION CENTER'), array('controller' => 'notifications', 'action' => 'index', 'admin' => true, 'notification')); ?></li>
		<?php
			if($controller == 'notifications' && $method == 'notification' && ($action =='admin_index' || $action=='admin_edit' || $action=='admin_add')){
		?>
		<li class="litmus_test-sub a-s<?php if($controller == 'notifications' && ($action =='admin_index' || $action=='admin_edit')) echo ' selected';?>"><?php echo $this->Html->link(__('Notification'), array('controller' => 'notifications', 'action' => 'index', 'admin' => true , 'notification')); ?> </li>
		<li class="add a-s<?php if($controller == 'notifications' && $action == 'admin_add') echo ' selected';?>"><?php echo $this->Html->link(__('Add Notification'), array('controller' => 'notifications', 'action' => 'add', 'admin' => true , 'notification')); ?> </li>
		<?php } ?>
		<li class="admin_setting"><?php echo $this->Html->link(__('ADMIN SETTINGS'), array('controller' => 'users', 'action' => 'user','settings')); ?></li>
		<?php
			$controllerLists = array(
				'users', 'information'
			);
			if(in_array($controller, $controllerLists)  && in_array($method == 'settings', $controllerLists) && ($action == 'admin_edit' || $action == 'admin_view' || $action=='admin_add' || $action=='admin_index' || $action=='admin_user')){
		?>
	    <li class="users a-s<?php if($controller =='users'  && in_array($method == 'settings', $controllerLists) && ($action =='admin_user' || $action == 'admin_edit' || $action=='admin_view' || $action == 'admin_view')) echo ' selected';?>"><?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'user', 'admin' => true, 'settings')); ?></li>
		<li class="add_user a-s<?php if($controller =='users'  && in_array($method == 'settings', $controllerLists) && $action =='admin_add') echo ' selected';?>"><?php echo $this->Html->link(__('Add Users'), array('controller' => 'users', 'action' => 'add', 'admin' => true, 'settings')); ?></li>
		<li class="about_us a-s<?php if($controller=='information' && in_array($method == 'settings', $controllerLists) && $action =='admin_edit') echo ' selected';?>"><?php echo $this->Html->link(__('About Us'), array('controller' => 'information', 'action' => 'edit', 'admin' => true, 'settings', 1)); ?> 
		</li>
		<li class="logout"><?php echo $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout')); ?> 
		</li>
		<?php } ?>
		<!-- <li class="admin_setting"><?php echo $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout', 'admin' => true)); ?></li> -->
	</ul>
</div>
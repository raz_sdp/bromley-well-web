<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>

</head>

<style type="text/css">
body{
	padding: 5%;
	background: url('./img/bg.png') no-repeat;
	height:auto;
	/*width : 100%;*/
	min-height: 864px;
	background-size : 100% 100%;
}
h3{
	/*background: #fe580d;*/
	padding: 20px 0;
	color: #fff;
	margin-top: 0;
	margin-bottom: 0
}

label{

}
.radio-align label {
    color: #308fdd;
    font-size: 19px;
    width: 100px;
}
.custom-radio input[type="radio"] {
    cursor: pointer;
    margin: 1px;
    opacity: 0;
    outline: medium none;
    position: absolute;
    z-index: 2;
}
.radio-align input[type="radio"] {
    display: inline-block;
    text-align: center;
}
.custom-radio {
    background: url("http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/radio.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
    display: inline-block;
    height: 50px;
    position: relative;
    top: 3px;
    width: 50px;
    z-index: 1;
}

.custom-radio.selected {
    background: url("http://carers-custom.appinstitute.co.uk/app/webroot/CRF/img/radio-active.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
}
.self-text {
    background: url("./img/icon1.png") no-repeat scroll left top rgba(0, 0, 0, 0);
    display: inline-flex;
    font-size: 19px;
    padding-left: 35px;
}

@media only screen and (min-device-width: 320px) and (max-device-width: 480px) { 
 }
</style>
<body>
<?php echo $this->fetch('content'); ?>
</body>
</html>
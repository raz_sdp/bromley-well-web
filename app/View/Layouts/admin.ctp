<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(array('bootstrap.min','bootstrap-theme', 'style', 'upload_file','print'));
		echo $this->Html->script(array('jquery-2.1.1','bootstrap.min','jquery.textareaCounter.plugin','globalize.min', 'dx.chartjs','custom','jquery-dateFormat.min'));
         echo $this->Html->script(array());
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<script>
		var ROOT = '<?php echo $this->Html->url('/', true); ?>';
	   	var HERE = '<?php echo $this->here; ?>';
	</script>
</head>
<body>
	<div class="container-fluid">
		<div id="row">
			<div id="header"  class ="noprint">
				
				<div id="logo-right" class="">
					<?php echo $this->Html->image('bw-logo.png', array('alt' => 'logo')); ?>
				</div> 
				<div id="logo-left">
					<?php //echo $this->Html->image('cf-logo.png', array('alt' => 'logo-left')); ?>
				</div>
			</div>
			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>

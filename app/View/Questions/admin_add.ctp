<div class="questions form">
<?php echo $this->Form->create('Question'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Question'); ?></legend>
	<?php
		echo $this->Form->input('category_id');
		echo $this->Form->input('question');
		echo $this->Form->input('ans_type', array('label' => 'Answer Type','options' => $ans_type_options,));		
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu');?>
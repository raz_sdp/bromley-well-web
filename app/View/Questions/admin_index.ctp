<script type="text/javascript">
// category selection drop down change fn
	$(function(){
		$('#category').on('change', function(){
			location.href = ROOT + 'admin/questions/index/' + $(this).val();
		});
	});
</script>

<div class="questions index">
	<h2><?php echo __('Questions'); ?></h2>
    <div><?php echo $this->Html->link('Add new Question', array('controller' => 'questions', 'action' => 'add', 'admin' => true))?></div>
    <?php
		echo $this->Form->input('category', array('options' => $category,
		'selected' => $category_id
		)
		);
	?>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('question'); ?></th>			
			<th><?php echo $this->Paginator->sort(
				'ans_type',
				'Answer Type',
  				array('escape' => false)
  				); ?></th>

			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($questions as $question): ?>
	<tr>
		<td><?php echo h($question['Question']['question']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['ans_type']); ?>&nbsp;</td>
		<td><?php echo date_format(date_create($question['Question']['created']),'d-m-Y g:i A');?>
		</td>
		
		<td class="actions">
		
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $question['Question']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $question['Question']['id']), null, __('Are you sure you want to delete # %s?', $question['Question']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu');?>
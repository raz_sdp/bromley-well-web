<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
		<div class="paging pull-right">
			
			    <ul class="pagination pull-right">
					<?php
					echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
					echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
					echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
					?>
			    </ul>
				<p class="text-right">
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Showing {:start} to {:end} of {:count} entries')
				));
				?>	
				</p>
			
			
		</div>
	<h2><?php echo __('Notifications'); ?></h2>
    <!-- <div><?php echo $this->Html->link('Add new Notification', array('controller' => 'notifications', 'action' => 'add', 'admin' => true))?></div> -->
	<table cellpadding="0" cellspacing="0" class="table">
	<tr>
			
			<th><?php echo $this->Paginator->sort('notification'); ?></th>
			
			<th><?php echo $this->Paginator->sort('is_sent', 'Sent'); ?></th>
		
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($notifications as $notification): ?>
	<tr>
		
		<td><?php echo h($notification['Notification']['notification']); ?>&nbsp;</td>
	
		<td><?php 
		if(!empty($notification['Notification']['is_sent'])) echo 'Yes';
		else echo 'No'; 
		?>&nbsp;</td>
		<td><?php
			if ($this->Time->isToday($notification['Notification']['created'])) echo 'Today';
			else if($this->Time->wasYesterday($notification['Notification']['created'])) echo 'Yestderday';
			else echo date_format(date_create($notification['Notification']['created']), 'd/m/Y');
		?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('notification' ,'action' => 'edit', $notification['Notification']['id']),array('class'=>'btn btn-info')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $notification['Notification']['id']), array( 'class' => 'btn btn-primary'), __('Are you sure you want to delete %s?', $notification['Notification']['notification'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	

	
</div>
</div>
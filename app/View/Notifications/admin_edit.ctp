<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
		<h2><?php echo __('Admin Edit Notification'); ?></h2>
<?php echo $this->Form->create('Notification',array('class'=>'form-horizontal col-md-6')); ?>
	<fieldset>
		
		<?php echo $this->Form->input('id') ;?>
		<div class="form-group">
		    <label for="inputEmail3" class="col-sm-3 control-label">Notification</label>
		    <div class="col-sm-9 showText">
		    	<?php 
		    	echo $this->Form->input('notification',array('class'=>'form-control textarea','label'=>false,'id'=>'textarea', 'type'=>'textarea', 'div' => false));
		    	?>
		    	<!--  <textarea name="data[Notification][notification]" class="form-control textarea" rows="6"></textarea> -->
                <div id="showData1">max 500</div>

		      
		    </div>
    	</div>
    	<div class="form-group">
		    <label for="inputEmail3" class="col-sm-3 control-label">Send It Now</label>
		    <div class="col-sm-3">
		   <?php echo $this->Form->input('is_sent', array('label' => false)); ?>
		     <button type="submit" class="btn submit-green s-c">Submit</button>
	    	</div>
    	</div>	
    	
	
	</fieldset>
<?php echo $this->Form->end(); ?>
</div>
</div>
<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
<h2><?php echo __('Complaint'); ?></h2>
	<dl>
		<!-- <dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($complaint['Complaint']['id']); ?>
			&nbsp;
		</dd> -->
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($complaint['User']['name'], array('settings', 'controller' => 'users', 'action' => 'view', $complaint['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Complaint'); ?></dt>
		<dd>
			<?php echo h($complaint['Complaint']['complaint']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php
			if ($this->Time->isToday($complaint['Complaint']['created'])) echo 'Today';
			else if($this->Time->wasYesterday($complaint['Complaint']['created'])) echo 'Yestderday';
			else echo date_format(date_create($complaint['Complaint']['created']), 'd/m/Y');
			?>
			&nbsp;
		</dd>
	</dl>
</div>
</div>
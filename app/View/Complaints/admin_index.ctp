<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white" style="background:transparent">
<div class="row">
		<div class="col-md-12 feedback-left" >
			<div class="paging pull-right">
			
			    <ul class="pagination pull-right">
					<?php
					echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
					echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
					echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
					?>
			    </ul>
				<p class="text-right">
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Showing {:start} to {:end} of {:count} entries')
				));
				?>	
				</p>
			
			
		</div>
		<h2><?php echo __('Complaints'); ?></h2>
		<div id="messages" class="feedback-table tables-desgin">
			<table class="table ">
				<tr>
				<th><?php echo $this->Paginator->sort('use_id', 'NAME'); ?></th>
				<th style="text-align:right"><?php echo $this->Paginator->sort('created', 'CREATED'); ?></th>
				</tr>
			<?php foreach ($complaints as $complaint):?>
			  <tr>
			  	<td>
				  	<div class="checkbox problem-detail">
				      <label>
				        <input type="checkbox" id=<?php echo 'id_' .$complaint['Complaint']['id']; ?> value=<?php echo $complaint['Complaint']['id'];?>>  <?php 
				        	echo $complaint['User']['name']; 
				        ?>
				      </label>
				    </div>
			    </td>			  	
			  	<td style="text-align:right"><?php if ($this->Time->isToday($complaint['Complaint']['created'])) echo 'Today';
					else if($this->Time->wasYesterday($complaint['Complaint']['created'])) echo 'Yestderday';
					else echo date_format(date_create($complaint['Complaint']['created']), 'd/m/Y'); ?></td>
			  </tr>
			<?php endforeach; ?>
			</table>
		</div>
		</div>
		<div class="col-md-5 feedback-right">
			<div class="feedback-tab">
				<div>
								
						<span>
                            <?php
                            #echo $this->Time->nice(date("Y-m-d H:i:s"));
                            ?>
                        </span>
				
				<div class="close pull-right">X</div>
				</div>
				<div class="feedback-tab-part">	
					
					<!-- <div class="pull-right">
						<button type="button" class="btn btn-primary btn-sm">Share</button>
						<button type="button" id ="print" class="btn btn-primary btn-sm">Print</button>					
					</div> -->
					<h2>COMPLAINT</h2>
					
					<!-- <div class="btn-group com_pos_the_id">
						<button type="button" class="btn btn-gray btn-sm">Department A</button>
						<button type="button" class="btn btn-gray btn-sm">Position A</button>	
						<button type="button" class="btn btn-gray btn-sm">Theme A</button>		
					</div> -->
				</div>
				<div class="issue">
					<h4>CARER</h4>
					<p></p>
				</div>
				<div class="solution">
					<h4>COMPLAINT</h4>
					<p></p>
				</div>
				<!-- <div class="contact">
					<h4>CONTACT</h4>
					<p></p>
					<div id="video"></div>
				</div>
				<div class="problem_video">
					<h4>VIDEO</h4>
					<div id="video"></div>
				</div> -->
			</div>
		</div>
	</div>
</div>
</div>

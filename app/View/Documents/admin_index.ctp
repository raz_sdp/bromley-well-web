<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
		<div class="paging pull-right">
			
			    <ul class="pagination pull-right">
					<?php
					echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
					echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
					echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
					?>
			    </ul>
				<p class="text-right">
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Showing {:start} to {:end} of {:count} entries')
				));
				?>	
				</p>
			
			
		</div>
	<h2><?php echo __('Documents'); ?></h2>
    <!-- <div><?php echo $this->Html->link('Add new Document', array('controller' => 'documents', 'action' => 'add', 'admin' => true))?></div> -->
	<table cellpadding="0" cellspacing="0" class="table">
	<tr>
			
			<th><?php echo $this->Paginator->sort('filename'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($documents as $document): ?>
	<tr>
		
        <td><?php
            if(!empty($document['Document']['filename']))
                echo $this->Html->image('/img/pdf_icon.png',array('alt'=>'pdf')); 	echo $this->Html->link(
                $document['Document']['title'],
                '/files/documents/' . $document['Document']['filename'],
                array('target' => '_blank')
            );
            ?>&nbsp;</td>
		<td><?php 
			if ($this->Time->isToday($document['Document']['created'])) echo 'Today';
			else if($this->Time->wasYesterday($document['Document']['created'])) echo 'Yestderday';
			else echo date_format(date_create($document['Document']['created']), 'd/m/Y');
		?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('document' ,'action' => 'edit', $document['Document']['id']), array( 'class' => 'btn btn-primary')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $document['Document']['id']), array( 'class' => 'btn btn-primary'), __('Are you sure you want to delete %s?', $document['Document']['title'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
<!-- 	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Showing {:start} to {:end} of {:count} entries')
	));
	?>	</p>
	<ul class="pagination">
		<?php
		echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
		echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
		echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
	?>
	</ul> -->
</div>
</div>
<?php echo $this->Html->script(array('document_upload'));?>
<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
		<h2><?php echo __('Admin Add Document'); ?></h2>
<?php echo $this->Form->create('Document', array('type' => 'file','class'=> 'dndFileUpload form-horizontal')); ?>
	<fieldset>
			<div class="col-sm-12">
				<div class="form-group row">
				    <label for="inputEmail3" class="col-sm-1 control-label">Title</label>
				    <div class="col-sm-4">
				    <?php echo $this->Form->input('title',array('label' => false,'class'=>'form-control')); ?>
				    
			    	</div>
		    	</div>
		    </div>	
				<div class="col-sm-6 col-sm-offset-1">
					<article class="file-inputbg">
							  <div id="holder">
							  	 <label>Drop file here</label>
							  Or
							<span>Upload</span>
					<?php					
					echo $this->Form->input('filex', array('div'=>false, 'type' => 'file', 'class'=>'upload','label'=>false));
					?>
							  </div> 
							  <p id="upload" class="hidden"><label>Drag &amp; drop not supported, but you can still upload via this input field:<br><input type="file"></label></p>
							  <div class="preview"></div>
							</article>
							<p class="up" style="display:none;">Upload progress %: <progress id="uploadprogress" min="0" max="100" value="0">0</progress></p>
					<?php

					echo $this->Form->input('filename', array('type' => 'hidden'));

				?>
			</div>
				</fieldset>
				<div class="col-sm-offset-1" style="padding-left:15px">
				<span class="uploading">Uploading... Please wait.</span>
				<button class="btn submit-green s-c">Submit</button>
			</div>
<?php echo $this->Form->end(); ?>
</div>
</div>
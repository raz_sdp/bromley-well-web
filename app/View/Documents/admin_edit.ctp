<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
		<h2><?php echo __('Admin Edit Document'); ?></h2>
<?php echo $this->Form->create('Document',array('class'=>'form-horizontal col-md-6')); ?>
	<fieldset>
		<?php echo $this->Form->input('id') ;?>
		<div class="form-group">
		    <label for="inputEmail3" class="col-sm-3 control-label">Title</label>
		    <div class="col-sm-9">
		    <?php echo $this->Form->input('title',array('label' => false,'class'=>'form-control')); ?>
		     <button type="submit" class="btn btn-success s-c">Submit</button>
	    	</div>
    	</div>
	
	</fieldset>
<?php echo $this->Form->end(); ?>
</div>
</div>
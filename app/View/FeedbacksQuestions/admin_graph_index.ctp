<?php echo $this->Html->script(array('staffgraph', 'staffpulsegraph',  'litmususergraph','staffincidentgraph', 'staffincident', 'caring_graph', 'assessment_graph','jquery-ui'));?>
<?php 
    echo $this->Html->css(array('jquery-ui-min', 'style-ui'));
    //echo $this->Html->script(array('jquery-ui')); 
?>
    <style type="text/css">
        .select-control{
            margin-bottom: -5px;
            padding: 10px 0;
        }
        /*@media print
        {
        .noprint {display:none;}

        div.form, div.index, div.view {width: 100%; float: none;}

        }*/
    </style>
    <script type="text/javascript">
        $(function (){
            // $('#print').on('click',
            //     function() {
            //         window.print();
            //     });
            $("#from, #to").datepicker({
                dateFormat : 'dd/mm/yy',
                onClose : function(dateText){
                    $(this).data('mysql', dateText.split('/').reverse().join('-'));
                }
            });
        });
    </script>
    <?php echo $this->element('menu');?>
    <div class="index col-md-10 col-sm-10">
    <div class="white">
        <h2 class="text-center"><?php 
        if($category_id == 1){
            echo __('Young Carers Assessment'); 
        }elseif($category_id == 3){
            echo __('Young Carer Feedback Form');
        }else{
            echo __('How Caring Affects Me');
        }
        ?></h2>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="inline-block">
                <div class="col-sm-12">
    <div class="row fo-to">
        <div class="col-sm-3">
             <div class="form-group form">
                <label for="inputEmail3" class="col-sm-4 control-label">From</label>
                <div class="col-sm-7">
                    <?php
                    $fr =  $from === null ? strtotime('-30 days') :strtotime($from);
                echo $this->Form->input('from', array('placeholder' => date('d/m/Y', $fr),'type' => 'text','label'=>false, 'data-mysql' => date('Y-m-d', $fr), 'class'=>'form-control'));
                     ?>
                  
                </div>
             </div>
        </div> 
        <div class="col-sm-3">
             <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">To</label>
                <div class="col-sm-7">
                    <?php
                    $t = $to === null ? strtotime('today') : strtotime($to);
                    echo $this->Form->input('to', array('placeholder' => date('d/m/Y', $t ), 'type' => 'text','label'=>false, 'data-mysql' => date('Y-m-d', $t),'class'=>'form-control'));
                ?> 
                  
                </div>
                <button class="col-sm-2 btn btn-info" id="show">Go</button>
             </div>
        </div> 
    </div>     
</div>
            </div>
            <div class="qus">
            <label for="inputEmail3" class="col-sm-1 control-label">Question</label>
            <div class="col-sm-11">
                <?php
                    echo $this->Form->input('question', array('options' => $questions,
                    'selected' => $question_id, 'label'=>false,'class'=>'form-control',
                    //'empty' => 'All Submissions'
                    )
                    );
                ?>
            </div>
            </div>
        </div>
    </div>
<!--     <?php if($category_id == 1) {?>
<span class="select-val col-sm-offset-1"></span>
<?php }else { ?>
<div class="faltu"></div>
<?php } ?> -->
    <!-- Graph Start -->
    <div class="pulsesUsers">
            <!-- <div id="chartIncidentGraph"></div>
            <div id="chartIncident"></div> -->
            <?php if($category_id == 3) { ?>

                <div id="chartContainer"></div>
                <div id="litmusFullStackedBar"></div>
            <?php } ?>
            <?php if($category_id == 2) { ?>

            <div id="caringBar"></div>
            <div id="legend-fake">
                <div><span style="background:#920031"></span><small>Never</small></div>
                <div><span style="background:#ff4500"></span><small>Some of the time</small></div>
                <div><span style="background:#00ced1"></span><small>A lot of the time</small></div>
            </div>
            <?php } ?>
        <?php if($category_id == 1) { ?>

            <div id="assessnmentBar"></div>
            <div id="legend-fake">
                <div><span style="background:#34b575"></span><small><div id="option-1"><?php echo @$option_list[1]?></div></small></div>
                <div><span style="background:#cf9200"></span><small><div id="option-2"><?php echo @$option_list[2]?></div></small></div>
                <div><span style="background:#a61313"></span><small><div id="option-3"><?php echo @$option_list[3]?></div></small></div>
            </div>
        <?php } ?>
        </div>
</div>
</div>
    <!-- Graph End -->



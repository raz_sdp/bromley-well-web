<div class="feedbacksQuestions view">
<h2><?php echo __('Feedbacks Question'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($feedbacksQuestion['FeedbacksQuestion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($feedbacksQuestion['Question']['id'], array('controller' => 'questions', 'action' => 'view', $feedbacksQuestion['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Feedback'); ?></dt>
		<dd>
			<?php echo $this->Html->link($feedbacksQuestion['Feedback']['id'], array('controller' => 'feedbacks', 'action' => 'view', $feedbacksQuestion['Feedback']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answer'); ?></dt>
		<dd>
			<?php echo h($feedbacksQuestion['FeedbacksQuestion']['answer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answer Int'); ?></dt>
		<dd>
			<?php echo h($feedbacksQuestion['FeedbacksQuestion']['answer_int']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answer Score'); ?></dt>
		<dd>
			<?php echo h($feedbacksQuestion['FeedbacksQuestion']['answer_score']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($feedbacksQuestion['FeedbacksQuestion']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($feedbacksQuestion['FeedbacksQuestion']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php echo $this->element('menu');?>
<?php echo $this->Html->script(array( 'avg-feedback-graph','avg-bar-graph'));?>
<style type="text/css">
    .select-control{
        margin-bottom: -5px;
        padding: 10px 0;
    }
    /*@media print
    {
    .noprint {display:none;}

    div.form, div.index, div.view {width: 100%; float: none;}

    }*/
</style>
<script type="text/javascript">
    $(function (){
        $('#print').on('click',
            function() {
                window.print();
            });
    });
</script>
<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
    <div class="white">
        <h2><?php echo __(''); ?></h2>
        <div class="form-horizontal">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-1 control-label">Question</label>
                <div class="col-sm-3">
                    <?php
                    echo $this->Form->input('question', array('options' => $question,
                            'selected' => $question_id, 'label'=>false,'class'=>'form-control'
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
        <!-- Graph Start -->
        <div class="pulsesUsers">
            <!--        avg line graph-->
            <div id="avg-chartContainer"></div>
            <!--        avg bar graph-->
            <div id="avg-bar-graph"></div>
        </div>
    </div>
</div>
<!-- Graph End -->




<!--<script type="text/javascript">
 var obj = <?php /*echo json_encode($category_id); */?>;
// category selection drop down change fn
	$(function(){
		$('#question').on('change', function(){
			location.href = ROOT + 'admin/feedbacks_questions/response/' + obj + '/' + $(this).val();
		});
	});
</script>
<?php /*echo $this->element('menu');*/?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
	<h2><?php /*echo __('Response'); */?></h2>
	<div class="form-horizontal">
	 <div class="form-group">
	    <label for="inputEmail3" class="col-sm-1 control-label">Question</label>
	    <div class="col-sm-3">
	    	 <?php
/*		echo $this->Form->input('question', array('options' => $question,
		'selected' => $question_id, 'label'=>false,'class'=>'form-control'
		)
		);
	*/?>
	    </div>
	  </div>
   </div>
	<table cellpadding="0" cellspacing="0" class="table">
	<tr>
			<th><?php /*echo $this->Paginator->sort('user_id'); */?></th>
			<?php /*if($category_id != '2') { */?>
				<th><?php /*echo $this->Paginator->sort('answer', 'Text Answer'); */?></th>
					<?php /*if($category_id === '1'){ */?>
						<th><?php /*echo $this->Paginator->sort('answer_score', 'Current Score'); */?></th>
					<?php /*}
				 } */?>
			<th><?php /*echo $this->Paginator->sort('answer_int', 'Answer'); */?></th>
			<th><?php /*echo $this->Paginator->sort('created'); */?></th>
			<th class="actions"><?php /*echo __('Actions'); */?></th>
	</tr>
	<?php
/*	//pr($feedbacksQuestions);
	 foreach ($feedbacksQuestions as $feedbacksQuestion): */?>
	<tr>
		<td><?php /*echo h($feedbacksQuestion['Feedback']['User']['name']); */?>&nbsp;</td>
		<?php /*if($category_id != '2') { */?>
				<td><?php /*echo h($feedbacksQuestion['FeedbacksQuestion']['answer']); */?></td>
					<?php /*if($category_id === '1'){ */?>
						<td><?php /*echo h($feedbacksQuestion['FeedbacksQuestion']['answer_score']); */?></td>
					<?php /*}
				 } */?>
		<td><?php /*echo h($feedbacksQuestion['FeedbacksQuestion']['answer_int']); */?></td>
		<td><?php /*echo date_format(date_create($feedbacksQuestion['FeedbacksQuestion']['created']),'d-m-Y g:i A');*/?>
		</td>
		
		<td class="actions">

			<?php /*echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $feedbacksQuestion['FeedbacksQuestion']['id']),array('class'=>'btn btn-info'), null, __('Are you sure you want to delete this?', $feedbacksQuestion['FeedbacksQuestion']['id'])); */?>
		</td>
	</tr>
<?php /*endforeach; */?>
	</table>
	<p>
	<?php
/*	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	*/?>	</p>
	<ul class="pagination">
	<?php
/*		echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
		echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
		echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
	*/?>
	</ul>
</div>
</div>-->
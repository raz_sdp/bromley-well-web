<script type="text/javascript">
	var user = <?php echo json_encode($user_id); ?>;
	var obj = <?php echo json_encode($category_id); ?>;
	// category selection drop down change fn
	$(function(){
		$('#question').on('change', function(){
			location.href = ROOT + 'admin/feedbacks_questions/carer_response/' + user + '/' + obj + '/' + $(this).val();
		});
	});
</script>
<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
	<!-- <h2><?php echo __('Response'); ?></h2> -->
	<br/>
	<p><b><?php echo ($category['Category']['name'] . ' - ' . 'Carer'. ' : ' . $user_obj['User']['name'] .' Date Submitted '. date_format(date_create($feedbacksQuestions[0]['FeedbacksQuestion']['created']), 'd/m/Y')); ?> </b></p>
	<div class="form-horizontal">
	 <div class="form-group">
	   <!--  <label for="inputEmail3" class="col-sm-1 control-label">Question</label> -->
	    <div class="col-sm-3">
	    <!-- 	 <?php
		echo $this->Form->input('question', array('options' => $question,
		'selected' => $question_id, 'label'=>false,'class'=>'form-control'
		)
		);
	?>  -->   
	    </div>
	  </div>
   </div>
   <!-- <span class="select-val col-sm-offset-1"></span> -->
	<?php //print_r($feedbacksQuestions);
	foreach ($feedbacksQuestions as $key => $feedbacksQuestion): ?>
	<h5><b><?php echo h($feedbacksQuestion['Question']['question']); ?></b></h5>
	<?php if(!empty($feedbacksQuestion['FeedbacksQuestion']['answer_int'])) { ?>
	<div><?php /*if($key%2 === 0){
		echo $this->Html->image('my score/'.$feedbacksQuestion['FeedbacksQuestion']['answer_int'].'.png', array('height'=> 40));
	} elseif($key%2 === 1){
		echo $this->Html->image('target/'.$feedbacksQuestion['FeedbacksQuestion']['answer_int'].'.png', array('height'=> 40));
	}*/
        echo $this->Html->image('my score/'.$feedbacksQuestion['FeedbacksQuestion']['answer_int'].'.png', array('height'=> 40));
	?></div>
	 <?php } else{ ?>
	<div><?php echo h($feedbacksQuestion['FeedbacksQuestion']['answer']); ?></div>
	<?php } ?>
	<?php endforeach; ?>
	<!-- <p> -->
	<br/>
	<?php if(!empty($feedbacksQuestions[0]['Feedback']['video'])){    
		//echo 'Video Submission:'
		?>
	     <object codebase="http://www.apple.com/qtactivex/qtplugin.cab#version=7,3,0,0" width="240" height="320" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" class=" quicktime">
         <param value="<?php echo $this->Html->url('/files/videos/'. $feedbacksQuestions[0]['Feedback']['video']); ?>" name="src">
         <embed src="<?php echo $this->Html->url('/files/videos/'. $feedbacksQuestions[0]['Feedback']['video']); ?>" type="video/quicktime" controller="true" scale="aspect" width="240" height="320" airplay="allow" posterframe="" autostart="false" cache="true" bgcolor="white" aggressivecleanup="false" style="width: 240px; height: 320px;">
         <param value="true" name="controller">
      </object>
      <?php  }  ?>
	<!-- </p> -->
</div>
</div>
<?php echo $this->Html->script(array('jquery-1.8.2.min', 'globalize.min', 'dx.chartjs', 'staffincidentgraph','staffincident'));?>
    <style type="text/css">
        .select-control{
            margin-bottom: -5px;
            padding: 10px 0;
        }
        /*@media print
        {
        .noprint {display:none;}

        div.form, div.index, div.view {width: 100%; float: none;}

        }*/
    </style>
    <script type="text/javascript">
        $(function (){
            $('#print').on('click',
                function() {
                    window.print();
                });
        });
    </script>
    <!-- Graph Start -->
    <div class="pulsesUsers index">
        <div class="white">
            <div id="chartIncidentGraph"></div>
            <div id="chartIncident"></div>
        </div>
    </div>

    <!-- Graph End -->


<?php //echo $this->element('menu');?>
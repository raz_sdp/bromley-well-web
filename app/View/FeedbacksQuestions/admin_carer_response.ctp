<style type="text/css">
      .option-box{
        position: relative;
    }
    .option{
        position: absolute;
        top: 50%;
        left: 18%;
        width: 270px;
        word-wrap: break-word;
        transform: translate(-18%, -50%);
        color: #ffffff;
        font-size: 17px;
    }
    .bg-image {
        max-height: 200px;
    }
    .white{
        overflow:auto;
    }
</style>
<script type="text/javascript">
    var user = <?php echo json_encode($user_id); ?>;
    var obj = <?php echo json_encode($category_id); ?>;
    // category selection drop down change fn
    $(function () {
        $('#question').on('change', function () {
            location.href = ROOT + 'admin/feedbacks_questions/carer_response/' + user + '/' + obj + '/' + $(this).val();
        });
    });
</script>
<?php echo $this->element('menu'); ?>
<div class="index col-md-10 col-sm-10">
    <div class="white">
        <!-- <h2><?php echo __('Response'); ?></h2> -->
        <br/>

        <span>
            <b><?php echo('Young Carers Assessment' . ' - ' . 'Carer' . ' : ' . $feedbacksQuestions[0]['User']['name'] . ' Date Submitted ' . date_format(date_create($feedbacksQuestions[0]['UsersOption']['created']), 'd/m/Y')); ?> </b>
        </span>
        <span class="pull-right">
            <a href="<?php echo $this->Html->url('/admin/feedbacks/detail_view/'.$feedbacksQuestions[0]['User']['id'])?>">Back to the Submissions</a>
        </span>

        <!-- <span class="select-val col-sm-offset-1"></span> -->
        <?php
        foreach ($feedbacksQuestions as $feedbacksQuestion): ?>

            <h4 style="margin-left: 4%"><b><?php echo h($feedbacksQuestion['Assesment']['title']); ?></b></h4>
            
            <div class="col-md-12 option-box">
                <?php
                echo $this->Html->image('assessment/' . $feedbacksQuestion['UsersOption']['option_id'] . '.png', array('class'=> 'bg-image'));
                ?>
                <div class="option text-justify">
                     <?php
                     echo AppHelper::getOptionById($feedbacksQuestion['UsersOption']['assesment_id'],$feedbacksQuestion['UsersOption']['option_id']);
                     ?>
                </div>
            </div>

            <div class="clearfix"></div>

        <?php endforeach; ?>

    </div>
</div>
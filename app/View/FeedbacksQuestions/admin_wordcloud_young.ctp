<script type="text/javascript">
    var allWords = <?php echo json_encode($allWords); ?>;
    //var obj = <?php echo json_encode($user_id); ?>;
// category selection drop down change fn
	$(function(){
		$('#question').on('change', function(){
			location.href = ROOT + 'admin/feedbacks_questions/wordcloud_young/' + $(this).val();
		});
	});
</script>
<?php
echo $this->Html->script(array('d3', 'd3.layout.cloud','d3.display'), array('inline' => false));
?>

<!-- Graph Start -->
<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
		<div class= "form-horizontal monjachay">
		<div class="form-group">
		    <label for="inputEmail3" class="col-sm-1 control-label">Question</label>
		    <div class="col-sm-11">
		    <?php
				echo $this->Form->input('question', array('options' => $questions,
				'selected' => $question_id,'label'=>false,'class'=>'form-control',
				)
				);
			?>
	    	</div>
	    </div>
    	</div>
<!-- <span class="select-val col-sm-offset-1"></span> -->
        <div id ="wordcloud">

        </div>

    </div>
</div>

<!-- Graph End -->


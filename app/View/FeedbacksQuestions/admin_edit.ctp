<div class="feedbacksQuestions form">
<?php echo $this->Form->create('FeedbacksQuestion'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Feedbacks Question'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('question_id');
		echo $this->Form->input('feedback_id');
		echo $this->Form->input('answer');
		echo $this->Form->input('answer_int');
		echo $this->Form->input('answer_score');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu');?>
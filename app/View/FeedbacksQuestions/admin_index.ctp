<div class="feedbacksQuestions index">
	<h2><?php echo __('Feedbacks Questions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('question_id'); ?></th>
			<th><?php echo $this->Paginator->sort('feedback_id'); ?></th>
			<th><?php echo $this->Paginator->sort('answer'); ?></th>
			<th><?php echo $this->Paginator->sort('answer_int'); ?></th>
			<th><?php echo $this->Paginator->sort('answer_score'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($feedbacksQuestions as $feedbacksQuestion): ?>
	<tr>
		<td><?php echo h($feedbacksQuestion['FeedbacksQuestion']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($feedbacksQuestion['Question']['id'], array('controller' => 'questions', 'action' => 'view', $feedbacksQuestion['Question']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($feedbacksQuestion['Feedback']['id'], array('controller' => 'feedbacks', 'action' => 'view', $feedbacksQuestion['Feedback']['id'])); ?>
		</td>
		<td><?php echo h($feedbacksQuestion['FeedbacksQuestion']['answer']); ?>&nbsp;</td>
		<td><?php echo h($feedbacksQuestion['FeedbacksQuestion']['answer_int']); ?>&nbsp;</td>
		<td><?php echo h($feedbacksQuestion['FeedbacksQuestion']['answer_score']); ?>&nbsp;</td>
		<td><?php echo h($feedbacksQuestion['FeedbacksQuestion']['created']); ?>&nbsp;</td>
		<td><?php echo h($feedbacksQuestion['FeedbacksQuestion']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $feedbacksQuestion['FeedbacksQuestion']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $feedbacksQuestion['FeedbacksQuestion']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $feedbacksQuestion['FeedbacksQuestion']['id']), null, __('Are you sure you want to delete # %s?', $feedbacksQuestion['FeedbacksQuestion']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu');?>
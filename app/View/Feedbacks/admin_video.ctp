<script type="text/javascript">
// category selection drop down change fn
	$(function(){
		$('#category').on('change', function(){
			location.href = ROOT + 'admin/feedbacks/video/' + $(this).val();
		});
	});
</script>

<div class="feedbacks index">
	<h2><?php echo __('Feedbacks'); ?></h2>
    <?php
		echo $this->Form->input('category', array('options' => $category,
		'selected' => $category_id
		)
		);
	?>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('video'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($feedbacks as $feedback): ?>
	<tr>
		<td><?php
   if(!empty($feedback['Feedback']['video'])){
         ?>
	     <object codebase="http://www.apple.com/qtactivex/qtplugin.cab#version=7,3,0,0" width="240" height="320" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" class=" quicktime">
         <param value="<?php echo $this->Html->url('/files/videos/'. $feedback['Feedback']['video']); ?>" name="src">
         <embed src="<?php echo $this->Html->url('/files/videos/'. $feedback['Feedback']['video']); ?>" type="video/quicktime" controller="true" scale="aspect" width="240" height="320" airplay="allow" posterframe="" autostart="false" cache="true" bgcolor="white" aggressivecleanup="false" style="width: 240px; height: 320px;">
         <param value="true" name="controller">
      </object>
      <?php
         }
		?>&nbsp;</td>
		<td><?php echo date_format(date_create($feedback['Feedback']['created']),'d-m-Y g:i A');?>
		</td>
		
		<td class="actions">
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $feedback['Feedback']['id']), null, __('Are you sure you want to delete # %s?', $feedback['Feedback']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu');?>
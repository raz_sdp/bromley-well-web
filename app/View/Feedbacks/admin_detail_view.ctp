<script type="text/javascript">
    var obj = <?php echo json_encode($id); ?>;
    // category selection drop down change fn
    $(function(){
        $('#category').on('change', function(){
            location.href = ROOT + 'admin/feedbacks/detail_view/' +obj + '/' + $(this).val();
        });
    });
</script>
<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
    <div class="white">
        <div class="paging pull-right">

            <ul class="pagination pull-right">
                <?php
                echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
                echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
                ?>
            </ul>
            <p class="text-right">
                <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Showing {:start} to {:end} of {:count} entries')
                ));
                ?>
            </p>


        </div>
        <h2><?php if(!empty($feedbacks[0]['User']['name'])) echo __( $feedbacks[0]['User']['name'].'\'s List of Submissions'); else echo __('List of Submissions'); ?></h2>
        <div class="form-horizontal">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-1 control-label">Category</label>
                <div class="col-sm-3">
                    <?php
                    echo $this->Form->input('category', array('options' => $category,
                            'selected' => $category_id,'label'=>false,'class'=>'form-control',
                            'empty' => 'All Submissions'
                        )
                    );
                    ?>
                </div>
                <div class="pull-right something">
                    <?php if ($category_id == '3') {
                        echo $this->Html->link('View Word Cloud', array('controller' => 'feedbacks', 'action' => 'wordcloud', 'admin' => true, $id, $category_id), array('class' => 'btn btn-info'));
                    }
                    ?>
                    <?php
                    if( $category_id == '1' ) {
                        echo $this->Html->link(' View Chart', array('controller' => 'users_options', 'action' => 'self_assesment', 'admin' => true, $category_id, 1, $id), array('class'=>'btn btn-info'));
                    }elseif( $category_id == '2' ){
                        echo $this->Html->link(' View Chart', array('controller' => 'feedbacks_questions', 'action' => 'graph_index', 'admin' => true, $id, $category_id, 17), array('class'=>'btn btn-info'));
                    }elseif( $category_id == '3' ) {
                        echo $this->Html->link(' View Chart', array('controller' => 'feedbacks_questions', 'action' => 'young', 'admin' => true, $category_id, 26, $id), array('class'=>'btn btn-info'));
                    }
                    ?>
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table">
            <tr>

                <th><?php echo $this->Paginator->sort('category'); ?></th>             
                <th><?php echo $this->Paginator->sort('created'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php foreach ($feedbacks as $feedback):
                ?>
                <tr>

                    <td><?php echo h($feedback['Category']['name']); ?>&nbsp;</td>
                    <td><?php
                        if ($this->Time->isToday($feedback['Feedback']['created'])) echo 'Today';
                        else if($this->Time->wasYesterday($feedback['Feedback']['created'])) echo 'Yestderday';
                        else echo date_format(date_create($feedback['Feedback']['created']), 'd/m/Y');
                        ?>&nbsp;</td>
                    <td class="actions">
                        <!-- 	<?php echo $this->Html->link(__('Video'), array('controller' => 'feedbacks' ,'action' => 'video_response', $feedback['Feedback']['user_id'])); ?> -->
                        <?php
                        if($category_id === '1' || $feedback['Feedback']['category_id'] === '1'){
                            echo $this->Html->link(__('View Detail'), array('controller' => 'users_options' ,'action' => 'carer_response', $feedback['Feedback']['id']),array('class'=>'btn btn-info'));
                        } elseif($category_id === '2' || $feedback['Feedback']['category_id'] === '2'){
                            echo $this->Html->link(__('View Detail'), array('controller' => 'feedbacks_questions' ,'action' => 'carer_response_caring', $feedback['Feedback']['id']),array('class'=>'btn btn-info'));
                        } elseif($category_id === '3' || $feedback['Feedback']['category_id'] === '3'){
                            echo $this->Html->link(__('View Detail'), array('controller' => 'feedbacks_questions' ,'action' => 'young_carer_response', $feedback['Feedback']['id']),array('class'=>'btn btn-info'));
                        }
                        ?>
                        <?php
                        // echo $this->Form->postLink(__('Delete'), array('controller' => 'feedbacks' ,'action' => 'delete_submission', $feedback['Feedback']['id']), array('class' => 'btn btn-primary'), __('Are you sure you want to delete %s?', $feedback['Category']['name']));
                        ?>

                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>


<script type="text/javascript">
    var allWords = <?php echo json_encode($allWords); ?>;
    var obj = <?php echo json_encode($user_id); ?>;
    // category selection drop down change fn
    $(function () {
        $('#category').on('change', function () {
            location.href = ROOT + 'admin/feedbacks/wordcloud/' + obj + '/' + $(this).val();
        });
    });
</script>
<?php
echo $this->Html->script(array('d3', 'd3.layout.cloud', 'd3.display'), array('inline' => false));
?>

<!-- Graph Start -->
<?php echo $this->element('menu'); ?>
<div class="index col-md-10 col-sm-10">
    <div class="white">
        <div class="form-horizontal monjachay">
            <div class="form-group">
                <!-- <div>
                <?php if ($category_id == '3') {
                    echo $this->Html->link('View Word Cloud', array('controller' => 'feedbacks', 'action' => 'wordcloud', 'admin' => true, $id, $category_id));
                }
                ?>
            </div> -->
            </div>
        </div>

        <div id="wordcloud">

        </div>

    </div>
</div>

<!-- Graph End -->


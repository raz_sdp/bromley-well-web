<script type="text/javascript">
var obj = <?php echo json_encode($id); ?>;
// category selection drop down change fn
	$(function(){
		$('#category').on('change', function(){
			location.href = ROOT + 'admin/feedbacks/video_response/' +obj + '/' + $(this).val();
		});
	});
</script>

<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
	<h2><?php echo __('Video'); ?></h2>
	<div class="form-horizontal">
	<div class="form-group">
		    <label for="inputEmail3" class="col-sm-1 control-label">Category</label>
		    <div class="col-sm-3">
		    <?php
				echo $this->Form->input('category', array('options' => $category,
				'selected' => $category_id,'label'=>false,'class'=>'form-control'
				)
				);
			?>
		    
	    	</div>
    	</div>	
    </div>	
	<table cellpadding="0" cellspacing="0" class="table">
	<tr>
			<th><?php echo $this->Paginator->sort('video'); ?></th>
			<th><?php echo $this->Paginator->sort('category'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<!-- <th class="actions"><?php echo __('Actions'); ?></th> -->
	</tr>
	<?php foreach ($feedbacks as $feedback): ?>
	<tr>
		<td><?php
   if(!empty($feedback['Feedback']['video'])){
         ?>
	     <object codebase="http://www.apple.com/qtactivex/qtplugin.cab#version=7,3,0,0" width="240" height="320" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" class=" quicktime">
         <param value="<?php echo $this->Html->url('/files/videos/'. $feedback['Feedback']['video']); ?>" name="src">
         <embed src="<?php echo $this->Html->url('/files/videos/'. $feedback['Feedback']['video']); ?>" type="video/quicktime" controller="true" scale="aspect" width="240" height="320" airplay="allow" posterframe="" autostart="false" cache="true" bgcolor="white" aggressivecleanup="false" style="width: 240px; height: 320px;">
         <param value="true" name="controller">
      </object>
      <?php
         }
		?>&nbsp;</td>
		<td>
		<?php echo $feedback['Category']['name'];?>
		</td>
		<td><?php
		// echo date_format(date_create($feedback['Feedback']['created']),'d-m-Y g:i A');
		if ($this->Time->isToday($feedback['Feedback']['created'])) echo 'Today';
		else if($this->Time->wasYesterday($feedback['Feedback']['created'])) echo 'Yestderday';
		else echo date_format(date_create($feedback['Feedback']['created']), 'd/m/Y');
		?>
		</td>
		
		<!-- <td class="actions">
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete',  $feedback['Feedback']['id']), array( 'class' => 'btn btn-primary'), __('Are you sure you want to delete the video submitted by %s?', $feedback['User']['name']));  ?>
		</td> -->
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
		<?php
		echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
		echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
		echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
	?>
	</ul>
</div>
</div>
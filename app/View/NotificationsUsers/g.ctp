<?php
header("Content-type: application/json; charset=utf-8");
/**
 * Created by PhpStorm.
 * User: Raaz
 * Date: 6/4/14
 * Time: 4:28 PM
 */
$category = array();
$documents = Hash::extract($document_data, '{n}.Document');
foreach($documents as $key => $new_doc){
    $documents[$key]['file'] = $this->Html->url('/files/documents/'. $new_doc['filename'], true);
}
$information = Hash::extract($information_data, '{n}.Information');
$result_notification = array();
foreach($notification_data as $key => $new){
    // if($new['NotificationsUser']['is_deleted'] == '1'){
    //     unset($new['Notification'], $new['NotificationsUser']);
    // } else {
    //     $notification_user['is_read'] = $new['NotificationsUser']['is_read'];
    //     $notification_user['created'] = $new['NotificationsUser']['created'];
    //     $result_notification[] = am($new['Notification'],$notification_user);
    // }
    if($new['NotificationsUser']['is_deleted'] === null){
        $notification_user['user_deleted'] = false;
    }else {
        $notification_user['user_deleted'] = $new['NotificationsUser']['is_deleted'];
    }
    $notification_user['is_read'] = $new['NotificationsUser']['is_read'];
    $notification_user['created'] = @$new['NotificationsUser']['created'];
    $result_notification[] = am($new['Notification'],$notification_user);
}
//print_r($result_notification); exit;
foreach($category_data as $single_cat){
    $category[] = am($single_cat['Category'], array('questions' => $single_cat['Question']));
}
$all_data= am(
    array('Categories'=> $category),
    array('Document'=> $documents),
    array('Information'=> $information),
    array('Notification' => $result_notification),
    array('Whats_Happening' => [['link' => '']]),
    array('download_time' =>  strtotime(date("Y-m-d H:i:s")))
);
//print_r($all_data); exit;
die(json_encode(array('success' => true, 'all_data' => $all_data)));

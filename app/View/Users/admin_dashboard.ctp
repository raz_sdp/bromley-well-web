<?php echo $this->element('menu');?>

<div class="index col-md-10 col-sm-10">
    <div class="white">
        <div class="row dashbrd">
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'index'))?>">
                    <div class="d-feedback t-color">
                        <!-- <div class="staff t-color"> -->

                        <span>carers</span>
                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('staffPulse-icon.png') ?>
                            </div>
                            <p class="count-num"><?php echo $carer; ?></p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'thoughts', 'action' => 'index'))?>">
                    <div class="d-feedback t-color">
                        <!-- <div class="staff t-color"> -->

                        <span>Thoughts</span>
                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('tree.png') ?>
                            </div>
                            <p class="count-num"><?php echo @$thoughts; ?></p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'users_options', 'action' => 'self_assesment', 1, 1))?>">
                    <div class="videovault t-color">
                        <!-- <div class="realtime t-color"> -->

                        <span>Young Carers Assessment</span>
                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('video-icon.png') ?>
                            </div>
                            <p class="count-num"><?php  echo $self_assessment; ?></p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'feedbacks_questions', 'action' => 'admin_caring', 2, 17))?>">
                    <!-- <div class="d-feedback t-color"> -->
                    <div class="realtime t-color">

                        <span>How Caring Affects Me</span>
                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('litmusTest-icon.png') ?>
                            </div>
                            <p class="count-num"><?php echo $caring; ?></p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'feedbacks_questions', 'action' => 'admin_young', 3, 32))?>">
                    <!-- <div class="realtime t-color"> -->
                    <div class="document t-color">

                        <span>Young Carer Feedback Form</span>
                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('real-analytics-icon.png') ?>
                            </div>
                            <p class="count-num"><?php echo $young; ?></p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'documents', 'action' => 'index', 'admin' => true, 'document'))?>">
                    <div class="staff t-color">
                        <span>documents</span>
                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('document-icon.png') ?>
                            </div>
                            <p class="count-num"><?php echo $document; ?></p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'notifications', 'action' => 'index', 'admin' => true , 'notification'))?>">
                    <div class="litmustest t-color">
                        <span>NOTIFICATION CENTER</span>
                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('notification-icon.png') ?>
                            </div>
                            <p class="count-num"><?php echo $notification ?></p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'information', 'action' => 'edit', 'admin' => true, 'settings', 1))?>">
                    <div class="adminsetting t-color">
                        <span>Admin Settings</span>
                        <div class="all-desc">
                            <div class="pull-right">
                                <?php echo $this->Html->image('admin-settings-icon.png') ?>
                            </div>
                            <p class="count-num"><?php // echo $notification; ?></p>
                        </div>
                    </div>
                </a>
            </div>

        </div>
    </div>
</div>
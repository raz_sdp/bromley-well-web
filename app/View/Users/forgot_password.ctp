<style type="text/css">
body{
  background: #fff;
}

#admin-login{
  margin-top: 100px;
}
  #header{
    display: none;
  }
  .frgt-pwd{
    width: 350px;
    margin: auto;
  }
</style>
<div class="container dashboard">
  <div class="row"  id="admin-login">
    <h3>Reset Password</h3>
    <p>If you've forgotten the username and password to your account, you can retrieve it by confirming your e-mail address corresponds with your account.</p>
  <div class="frgt-pwd">
  <?php echo $this->Form->create('User',array('class'=>'form-group'));?>
  <fieldset>
  <h2>Action for Young Carers</h2>
  <?php
    echo $this->Form->input('email',array('type' => 'email','class'=>'form-control','label'=>false,'placeholder'=>'Email','div'=>array('class'=>'form-group')));?>    
    <button type="submit" class="btn btn-primary pull-right">Submit</button>
    <?php echo $this->Form->end(); ?>
  </fieldset>
  </div>
  </div>
</div>
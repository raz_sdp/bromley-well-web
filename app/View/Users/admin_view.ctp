<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
<h2><?php echo __('User'); ?></h2>
	<dl>
		<!-- <dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd> -->
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mobile'); ?></dt>
		<dd>
			<?php echo h($user['User']['mobile']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Carer Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['carerid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php
			if ($this->Time->isToday($user['User']['created'])) echo 'Today';
			else if($this->Time->wasYesterday($user['User']['created'])) echo 'Yestderday';
			else echo date_format(date_create($user['User']['created']), 'd/m/Y');
			?>
			&nbsp;
		</dd>
	</dl>
</div>
</div>
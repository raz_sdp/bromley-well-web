<style type="text/css">
body{
  background: #fff;
}

#admin-login{
  margin-top: 100px;
}
  #header{
    display: none;
  }
  .frgt-pwd{
    width: 350px;
    margin: auto;
  }
</style>
<div class="container dashboard">
  <div class="row"  id="admin-login">
   
  <div class="frgt-pwd">
  <?php echo $this->Form->create('User',array('class'=>'form-group'));?>
  <fieldset>
     <h3>Change Password</h3>
  <h2>Action for Bromley Well</h2>
  <?php
    echo $this->Form->input('new_password', array('type' => 'password','class'=>'form-control','label'=>false,'placeholder'=>'New Password','div'=>array('class'=>'form-group')));
    echo $this->Form->input('confirm_password', array('type' => 'password', 'class'=>'form-control','label'=>false,'placeholder'=>'Confirm Password','div'=>array('class'=>'form-group')));
    ?>    
    <button type="submit" class="btn btn-primary pull-right">Reset</button>
    <?php echo $this->Form->end(); ?>
  </fieldset>
  </div>
  </div>
</div>
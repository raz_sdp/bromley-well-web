<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
		<h2><?php echo __('Admin Add User'); ?></h2>
    <?php $option = array('admin'=>'Admin', 'user' => 'User'); ?>
<?php echo $this->Form->create('User',array('class'=>'form-horizontal col-md-6')); ?>
	<fieldset>
		<div class="form-group">
		    <label for="inputEmail3" class="col-sm-3 control-label">Name</label>
		    <div class="col-sm-9">
		    <?php echo $this->Form->input('name',array('label' => false,'class'=>'form-control')); ?>
	    	</div>
    	</div>
    	<div class="form-group">
		    <label for="inputEmail3" class="col-sm-3 control-label">Password</label>
		    <div class="col-sm-9">
		    <?php echo $this->Form->input('password',array('label' => false,'class'=>'form-control','type'=>'password')); ?>
	    	</div>
    	</div>
    	<div class="form-group">
		    <label for="inputEmail3" class="col-sm-3 control-label">Mobile</label>
		    <div class="col-sm-9">
		    <?php echo $this->Form->input('mobile',array('label' => false,'class'=>'form-control')); ?>
	    	</div>
    	</div>
    	<div class="form-group">
		    <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
		    <div class="col-sm-9">
		    <?php echo $this->Form->input('email',array('label' => false,'class'=>'form-control')); ?>
		    <button type="submit" class="btn submit-green s-c">Submit</button>
	    	</div>
    	</div>
	<?php
		
        echo $this->Form->input('role', array('value' => 'admin', 'type' => 'hidden'));
        
		//echo $this->Form->input('carerid');
	?>
	</fieldset>
<?php echo $this->Form->end(); ?>
</div>
</div>
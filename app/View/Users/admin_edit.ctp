<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
	<div class="white">
<?php echo $this->Form->create('User',array('class'=>'form-horizontal col-md-6')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit User'); ?></legend>
		<?php echo $this->Form->input('id'); ?>
		<div class="form-group">
		    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		    	<?php echo $this->Form->input('name',array('class'=>'form-control','label'=>false)); ?>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
		    <div class="col-sm-10">
		    	<?php echo $this->Form->input('passwordx', array('class'=>'form-control','label' => false,'type' => 'Password')); ?>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
		    <div class="col-sm-10">
		    	<?php echo $this->Form->input('mobile', array('class'=>'form-control','label' => false)); ?>
		    </div>
		</div>
		<div class="form-group">
		    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
		    <div class="col-sm-10">
		    	<?php echo $this->Form->input('email', array('class'=>'form-control','label' => false)); ?>
		    	<button type="submit" class="btn btn-primary s-c">Submit</button>
		    	<?php echo $this->Form->input('role', array('type' => 'hidden'));?>
		    </div>
		</div>
	
	</fieldset>
<?php echo $this->Form->end(); ?>
</div>
</div>

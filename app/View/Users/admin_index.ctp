
<script type="text/javascript">
    //fn for from(calender)-to(calender) to show transaction records for a specified records
    $(function () {
        $("#go").on('click', function () {
            var keyword = $.trim($('#keyword').val());
            var loc = location.href;//.substr(ROOT.length)
            if (loc.indexOf('index') === -1) {
                location.href = loc + '/index/keyword:' + keyword;
            } else if (loc.indexOf('keyword') === -1) {
                location.href = loc + '/keyword:' + keyword;
            } else {
                location.href = loc.replace(/(index).*/, '$1/keyword:' + keyword);
            }
        })
    });
</script>
<?php echo $this->element('menu'); ?>
<div class="index col-md-10 col-sm-10">
    <div class="white">
        <div class="paging pull-right">

            <ul class="pagination pull-right">
                <?php
                echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
                echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
                ?>
            </ul>
            <p class="text-right">
                <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Showing {:start} to {:end} of {:count} entries')
                ));
                ?>
            </p>


        </div>
        <h2><?php echo __('Carers'); ?></h2>

        <div class="form-horizontal">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-1 control-label">Keyword</label>

                <div class="col-sm-3">
                    <?php echo $this->Form->input('titlex', array('id' => 'keyword', 'label' => false, 'class' => 'form-control')); ?>
                </div>
                <div class="col-sm-1">
                    <?php
                    echo $this->Form->input('Go', array('id' => 'go', 'class' => 'btn btn-info', 'type' => 'button', 'label' => false));
                    ?>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">                
                <tr>
                    <th class="fixed-width"><?php echo $this->Paginator->sort('name'); ?></th>
                    <th class="fixed-width"><?php echo $this->Paginator->sort('mobile'); ?></th>
                    <th class="fixed-width"><?php echo $this->Paginator->sort('carerid'); ?></th>
                    <th class="fixed-width"><?php echo $this->Paginator->sort('is_enabled', 'Enabled'); ?></th>
                    <th class="fixed-width"><?php echo $this->Paginator->sort('created'); ?></th>
                    <th class="actions enable"><?php echo __('Actions'); ?></th>
                </tr>                                
                <?php foreach ($users as $user): ?>
                    <tr>
                        <td class="fixed-width"><?php echo h($user['User']['name']); ?>&nbsp;</td>
                        <td class="fixed-width"><?php echo h($user['User']['mobile']); ?>&nbsp;</td>
                        <td class="fixed-width"><?php echo h($user['User']['carerid']); ?>&nbsp;</td>
                        <?php if (!empty($user['User']['is_enabled'])) { ?>
                            <td class="fixed-width"><?php echo h('Yes'); ?></td>
                        <?php } else { ?>
                            <td class="fixed-width"><?php echo h('No'); ?></td>
                        <?php } ?>
                        &nbsp;
                        <td class="fixed-width"><?php
                            if ($this->Time->isToday($user['User']['created'])) echo 'Today';
                            else if ($this->Time->wasYesterday($user['User']['created'])) echo 'Yestderday';
                            else echo date_format(date_create($user['User']['created']), 'd/m/Y');
                            ?>&nbsp;</td>
                        <td class="actions">
                            <?php
                            if ($user['User']['is_enabled'] == 0) {
                                echo $this->Html->link(__('Enable'), array('action' => 'enable_carer', $user['User']['id']), array('class' => 'btn btn-info'));
                            } else {
                                echo $this->Html->link(__('Disable'), array('action' => 'disable_carer', $user['User']['id']), array('class' => 'btn btn-info'));
                            }
                            ?>
                            <?php // echo $this->Html->link(__('Video'), array('controller' => 'feedbacks' ,'action' => 'video_response', $user['User']['id']),array('class'=>'btn btn-info')); ?>
                            <?php echo $this->Html->link(__('Submissions'), array('controller' => 'feedbacks', 'action' => 'detail_view', $user['User']['id']), array('class' => 'btn btn-info')); ?>
                            <?php echo $this->Html->link(__('Staff Alerts'), array('controller' => 'staffalerts', 'action' => 'index', $user['User']['id']), array('class' => 'btn btn-info')); ?>
                            <?php echo $this->Html->link(__('Thoughts'), array('controller' => 'thoughts', 'action' => 'index', $user['User']['id']), array('class' => 'btn btn-info')); ?>
                            <?php echo $this->Html->link(__('Edit'), array('carers', 'action' => 'edit', $user['User']['id']), array('class' => 'btn btn-info')); ?>
                            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-primary'), __('Are you sure you want to delete %s?', $user['User']['name'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>                
            </table>
        </div>
        <!-- 	<p>
	<?php
        echo $this->Paginator->counter(array(
            'format' => __('Showing {:start} to {:end} of {:count} entries')
        ));
        ?>	</p>
	<ul class="pagination">
		<?php
        echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
        echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
        ?>
	</ul> -->
    </div>
</div>

<style type="text/css">
    .fixed-width{
        width: 6%;
    }
</style>
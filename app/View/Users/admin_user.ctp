<?php echo $this->element('menu');?>
<div class="index col-md-10 col-sm-10">
   <div class="white">
   	<div class="paging pull-right">
			
			    <ul class="pagination pull-right">
					<?php
					echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
					echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
					echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
					?>
			    </ul>
				<p class="text-right">
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Showing {:start} to {:end} of {:count} entries')
				));
				?>	
				</p>
			
			
		</div>
	<h2><?php echo __('Users'); ?></h2>
    <!-- <div><?php echo $this->Html->link('Add new User', array('controller' => 'users', 'action' => 'add', 'admin' => true))?></div> -->
	<table cellpadding="0" cellspacing="0" class="table">
	<tr>
			
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('mobile'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($users as $user): ?>
	<tr>
		
		<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['mobile']); ?>&nbsp;</td>
		<td><?php
			if ($this->Time->isToday($user['User']['created'])) echo 'Today';
			else if($this->Time->wasYesterday($user['User']['created'])) echo 'Yestderday';
			else echo date_format(date_create($user['User']['created']), 'd/m/Y');
		?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('settings','action' => 'edit', $user['User']['id']), array( 'class' => 'btn btn-info')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array( 'class' => 'btn btn-primary'), __('Are you sure you want to delete %s?', $user['User']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<!-- <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Showing {:start} to {:end} of {:count} entries')
	));
	?>	</p>
	<ul class="pagination">
		<?php
		echo $this->Paginator->prev(' < ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> < </a>', array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
		echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a'));
		echo $this->Paginator->next(' > ', array('tag' => 'li', 'disabledTag' => 'li', 'escape' => false), '<a href="#"> > </a>', array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'li', 'escape' => false));
	?>
	</ul> -->
</div>
</div>

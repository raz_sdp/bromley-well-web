<?php
App::uses('AppModel', 'Model');

/**
 * NotificationsUser Model
 *
 * @property User $User
 * @property Notification $Notification
 */
class NotificationsUser extends AppModel
{


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Notification' => array(
            'className' => 'Notification',
            'foreignKey' => 'notification_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    // public function getNotification($user_id,$download_time)
    // {
    //     $conditions = array();
    //     if (strtotime($download_time) != strtotime("1970-01-01 00:00:00")) {
    //         $download_time = $download_time;
    //     } else {
    //         $conditions = array('Notification.is_deleted' => 0);
    //     }
    //     $conditions = am(
    //         $conditions,
    //         //array('NotificationsUser.user_id' => $user_id),
    //         //array('NotificationsUser.is_deleted' => 0),
    //         array('Notification.modified >=' => $download_time)
    //     );
    //     //$this->Behaviors->attach('Containable');
    //     // $query = array(
    //     //     'recursive' => 2,
    //     //     'contain' => array(
    //     //         /*'Feedback' => array(
    //     //             'Question'
    //     //         ),*/
    //     //         'Notification' => array(
    //     //             'fields' => array('Notification.id', 'Notification.notification', 'Notification.is_deleted'),
    //     //         ),
    //     //     ),
    //     //     'conditions' => $conditions,
    //     // );
    //     //return $this->find('all', $query);
    //     $data = $this->find('all', array(
    //         'joins' => array(
    //             array(
    //                  'table' => 'users',
    //                         'alias' => 'User',
    //                         'type' => 'LEFT',
    //                         'conditions' => 'User.id = Booking.requester'
    //                 ),
    //             ),
    //         ),
    //     ));
    // }
}

<?php
App::uses('AppModel', 'Model');
/**
 * FeedbacksQuestion Model
 *
 * @property Question $Question
 * @property Feedback $Feedback
 */
class FeedbacksQuestion extends AppModel {


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Question' => array(
            'className' => 'Question',
            'foreignKey' => 'question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Feedback' => array(
            'className' => 'Feedback',
            'foreignKey' => 'feedback_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public function feedbackData(){
        $this->Behaviors->attach('Containable');
        $query = array(
            'recursive' => 2,
            'fields' =>array('(ROUND(((SUM(FeedbacksQuestion.answer_int)/ (COUNT(FeedbacksQuestion.id))/COUNT(DISTINCT FeedbacksQuestion.feedback_id))),2)) AS avg_result','COUNT(DISTINCT FeedbacksQuestion.feedback_id) as submission','MONTHNAME(FeedbacksQuestion.created) AS month'),
            'conditions'=>array(
                //'YEAR(FeedbacksQuestion.created) = YEAR(CURDATE())',
            ),
            'group' => array('MONTH(FeedbacksQuestion.created)'),
            //'order' => array('YEAR(FeedbacksQuestion.created)'),
            'contain' => array(
                'Question' => array(
                    //'Category' => array('conditions' => array('Category.id' => 3))
                ),
            ),
        );
        return $this->find('all', $query);
    }
    public function usersFeedback($user_id, $category_id, $question_id, $from = null, $to = null){
        $fields = array();
        $fields = $category_id == 1 ? array('FeedbacksQuestion.answer_int','FeedbacksQuestion.answer_score') : array('FeedbacksQuestion.answer_int');
        $fields = am(
            $fields, 'FeedbacksQuestion.created'
        );
        $conditions = array(
            'FeedbacksQuestion.question_id' => $question_id,
            'Feedback.category_id' => $category_id,
            'Feedback.user_id' => $user_id,
        );
        // $from = '2014-06-23';
        // $to = '2014-06-26';
        if(!empty($from) && !empty($to)){
            //$from = $from;
            $conditions = am(
                $conditions,
                array('DATE(FeedbacksQuestion.created) BETWEEN ? AND ? ' => array($from, $to)));
        }
        $query = array(
            'recursive' => 0,
            'fields' => $fields,
            'conditions'=> $conditions,
            'order' => 'Feedback.created'
        );
        return $this->find('all', $query);
    }

    /**
     * @param $category_id
     * @param $question_id
     * @return array
     */
    public function avgFeedback($category_id, $question_id, $from = null, $to = null, $user_id = null){
        $fields = $category_id == 3 ? array('AVG(FeedbacksQuestion.answer_int) as answer_int','COUNT(FeedbacksQuestion.id) as feedback_count') : array('AVG(FeedbacksQuestion.answer_int) as answer_int');
        $fields = am(
            $fields, 'FeedbacksQuestion.created'
        );
        $conditions = array(
            'FeedbacksQuestion.question_id' => $question_id,
            'Feedback.category_id' => $category_id,
            'YEAR(Feedback.created) = YEAR(CURDATE())',
        );
        // $from = '2014-06-23';
        // $to = '2014-06-26';
        if(!empty($from) && !empty($to)){
            //$from = $from;
            $conditions = am(
                $conditions,
                array('DATE(Feedback.created) BETWEEN ? AND ? ' => array($from, $to)));
        }
        if(!empty($user_id)){            
            $conditions = am(
                $conditions,
                array('Feedback.user_id' => $user_id));
        }
        $query = array(
            'recursive' => 0,
            'fields' => $fields,
            'conditions'=> $conditions,
            'group'=> array('MONTH(FeedbacksQuestion.created)'),
        );
        return $this->find('all', $query);
    }

    /**
     * @param $category_id
     * @param $question_id
     * @return array
     */
    public function avgFeedbackCat2($category_id, $question_id, $from = null, $to = null){
        $conditions = array(
                'FeedbacksQuestion.question_id' => $question_id,
                'Feedback.category_id' => $category_id,
                'YEAR(Feedback.created) = YEAR(CURDATE())',
            );
        // $from = '2014-06-23';
        // $to = '2014-06-26';
        if(!empty($from) && !empty($to)){
            //$from = $from;
            $conditions = am(
                $conditions,
                array('DATE(Feedback.created) BETWEEN ? AND ? ' => array($from, $to)));
        }
        $query = array(
            'recursive' => 0,
            'fields' => array('COUNT(FeedbacksQuestion.answer_int) as answer_count','FeedbacksQuestion.created', 'FeedbacksQuestion.answer_int'),
            'conditions'=> $conditions,
            'group'=> array('FeedbacksQuestion.answer_int', 'MONTH(FeedbacksQuestion.created)'),
            'order' => array('FeedbacksQuestion.created' => 'ASC'),
        );
        return $this->find('all', $query);
    }
}

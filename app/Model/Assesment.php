<?php
App::uses('AppModel', 'Model');
/**
 * Assesment Model
 *
 * @property Option $Option
 * @property UsersOption $UsersOption
 */
class Assesment extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Option' => array(
			'className' => 'Option',
			'foreignKey' => 'assesment_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UsersOption' => array(
			'className' => 'UsersOption',
			'foreignKey' => 'assesment_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}

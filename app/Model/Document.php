<?php
App::uses('AppModel', 'Model');
/**
 * Document Model
 *
 * @property User $User
 */
class Document extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		)
	);

    public function getDocument($conditions){
        $query = array(
            'recursive' => -1,
            'fields'=> array('Document.id', 'Document.title', 'Document.filename','Document.is_deleted'),
            'conditions' => $conditions,
            'order' => 'Document.created DESC'
        );
        return $this->find('all', $query);
    }
}

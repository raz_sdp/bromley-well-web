<?php
App::uses('AppModel', 'Model');

/**
 * DeviceToken Model
 *
 * @property User $User
 */
class DeviceToken extends AppModel
{


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public function getEnableUser()
    {
        $query = array(
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => array(
                        'User.id = DeviceToken.user_id',
                        'User.is_enabled' => 1,
                    )
                ),
            ),
            'fields' => array('DeviceToken.device_token', 'DeviceToken.stage', 'DeviceToken.device_type', 'DeviceToken.user_id'),
        );
        return $this->find('all', $query);
    }
}

<?php
App::uses('AppModel', 'Model');
/**
 * UsersOption Model
 *
 * @property User $User
 * @property Assesment $Assesment
 * @property Option $Option
 */
class UsersOption extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Assesment' => array(
			'className' => 'Assesment',
			'foreignKey' => 'assesment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Option' => array(
			'className' => 'Option',
			'foreignKey' => 'option_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function avgFeedBackSelfAssessment($assesment_id  = null, $from = null, $to = null, $user_id = null){
		$conditions = array(
            'UsersOption.assesment_id' => $assesment_id,            
            'YEAR(UsersOption.created) = YEAR(CURDATE())',
        );
        
        if(!empty($from) && !empty($to)){
            //$from = $from;
            $conditions = am(
                $conditions,
                array('DATE(UsersOption.created) BETWEEN ? AND ? ' => array($from, $to)));
        }
        if(!empty($user_id)) {
            $conditions = am(
                $conditions,
                array('UsersOption.user_id' => $user_id)
            );
        }

		$query = [
			'recursive' => -1,
			'fields' => [
				'COUNT(UsersOption.option_id) as answer_count','UsersOption.option_id','UsersOption.created'
			],
			'conditions' => $conditions,
			'group' => ['UsersOption.option_id','MONTH(UsersOption.created)'],
			'order' => array('UsersOption.created' => 'ASC'),
		];
		return $this->find('all', $query);
	}
    public function usersFeedback($user_id, $question_id, $from = null, $to = null){
        $fields = array('UsersOption.option_id','UsersOption.created');
        $conditions = array(
            'UsersOption.assesment_id' => $question_id,
            'UsersOption.user_id' => $user_id,
        );
        // $from = '2014-06-23';
        // $to = '2014-06-26';
        if(!empty($from) && !empty($to)){
            $conditions = am(
                $conditions,
                array('DATE(UsersOption.created) BETWEEN ? AND ? ' => array($from, $to)));
        }
        $query = array(
            'recursive' => -1,
            'fields' => $fields,
            'conditions'=> $conditions,
            'order' => 'UsersOption.created'
        );
        return $this->find('all', $query);
    }
}

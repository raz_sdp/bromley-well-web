<?php
App::uses('AppModel', 'Model');
/**
 * Feedback Model
 *
 * @property User $User
 * @property Category $Category
 * @property Question $Question
 */
class Feedback extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Question' => array(
			'className' => 'Question',
			'joinTable' => 'feedbacks_questions',
			'foreignKey' => 'feedback_id',
			'associationForeignKey' => 'question_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);
    /*public function feedbackData(){
        $this->Behaviors->attach('Containable');
        $query = array(
            'recursive' => 2,
            'fields' =>array('Feedback.id','Feedback.user_id','Feedback.video'),
            'conditions'=>array('YEAR(Feedback.created) = YEAR(CURDATE())'),
            'contain'=>array(
                'Question'=>array(
                    'fields'=>array('Question.id','Question.question'),
                    'Category'=>array(),
                ),
            ),
        );
        return $this->find('all', $query);
    }*/
    public function getWordCloudData($user_id, $category_id){
        $this->Behaviors->attach('Containable');
        $query = array(
            'recursive' => 2,
            'fields' =>array('Feedback.id','Feedback.user_id','Feedback.video'),
            'conditions'=>array(
                'Feedback.user_id'=>$user_id,
                'Feedback.category_id'=>$category_id,
            ),
            'contain'=>array(
                'Question'=>array(
                    'fields'=>array('Question.id','Question.question'),
                    'conditions'=>array(
                        'Question.category_id'=>$category_id,
                    ),
                ),
            ),
        );
        return $this->find('all', $query);
    }
}

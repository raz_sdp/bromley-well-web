<?php
App::uses('AppModel', 'Model');

/**
 * Information Model
 *
 */

class Information extends AppModel
{
	public $validate = array(
		'about_company' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		),
		'term_of_use' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		),
		'contact_email' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		),
		'manager_phone' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		)

	);
    public function getInformation($download_time)
    {
        $query = array(
            'recursive' => -1,
            'fields' => array('Information.about_company', 'Information.manager_phone', 'Information.contact_email', 'Information.term_of_use', 'Information.is_deleted'),
            'conditions' => array('modified >=' => $download_time),
        );
        return $this->find('all', $query);
    }

}

<?php
App::uses('AppModel', 'Model');
/**
 * PrequalifyingQuestion Model
 *
 */
class PrequalifyingQuestion extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'prequalifying _questions';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'question';

}

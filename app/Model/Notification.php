<?php
App::uses('AppModel', 'Model');
/**
 * Notification Model
 *
 * @property User $User
 */
class Notification extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'notifications_users',
			'foreignKey' => 'notification_id',
			'associationForeignKey' => 'user_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

	public $validate = array(
		'notification' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
		)
	);


	public function getNotification($user_id, $download_time = null) {
		$this->recursive = -1;
        $conditions = array();
        if (strtotime($download_time) != strtotime("1970-01-01 00:00:00")) {
            $download_time = $download_time;
        } else {
            $conditions = array('Notification.is_deleted' => 0);
        }
        $conditions = am(
            $conditions,
            //array('NotificationsUser.user_id' => $user_id),
            array('Notification.is_sent' => 1),
            array('Notification.modified >=' => $download_time)
        );
        //$this->Behaviors->attach('Containable');
        // $query = array(
        //     'recursive' => 2,
        //     'contain' => array(
        //         /*'Feedback' => array(
        //             'Question'
        //         ),*/
        //         'Notification' => array(
        //             'fields' => array('Notification.id', 'Notification.notification', 'Notification.is_deleted'),
        //         ),
        //     ),
        //     'conditions' => $conditions,
        // );
        //return $this->find('all', $query);
        $data = $this->find('all', array(
            'joins' => array(
                array(
                     'table' => 'notifications_users',
                            'alias' => 'NotificationsUser',
                            'type' => 'LEFT',
                            'conditions' => array("Notification.id = NotificationsUser.notification_id AND NotificationsUser.user_id = '$user_id'")
                    ),
                ),
            'conditions' => $conditions,
            'fields' => array('Notification.*', 'NotificationsUser.is_deleted', 'NotificationsUser.is_read'),
            'order' => 'Notification.created DESC'
            )
        );
	return $data;
    }

}

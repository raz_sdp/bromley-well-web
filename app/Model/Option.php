<?php
App::uses('AppModel', 'Model');
/**
 * Option Model
 *
 * @property Assesment $Assesment
 */
class Option extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Assesment' => array(
			'className' => 'Assesment',
			'foreignKey' => 'assesment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : boomly

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2018-07-13 20:38:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `assesments`
-- ----------------------------
DROP TABLE IF EXISTS `assesments`;
CREATE TABLE `assesments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of assesments
-- ----------------------------
INSERT INTO `assesments` VALUES ('1', 'Friends', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `assesments` VALUES ('2', 'Free time and fun', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `assesments` VALUES ('3', 'Health and body', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `assesments` VALUES ('4', ' Smoking, drugs and\r\n            alcohol', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `assesments` VALUES ('5', ' School work', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `assesments` VALUES ('6', ' Bullying', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `assesments` VALUES ('7', ' Emotional life', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `assesments` VALUES ('8', ' How does caring affect\r\n            my life', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `assesments` VALUES ('9', ' Happy and safe at\r\n            home', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `assesments` VALUES ('10', 'Cooking at home', '2018-07-13 15:37:55', '2018-07-13 15:37:55');

-- ----------------------------
-- Table structure for `options`
-- ----------------------------
DROP TABLE IF EXISTS `options`;
CREATE TABLE `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assesment_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of options
-- ----------------------------
INSERT INTO `options` VALUES ('1', '1', 'I am happy with the\r\n                friendships I have', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `options` VALUES ('2', '1', 'I have some friends but\r\nwould like more', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `options` VALUES ('3', '1', 'Iâ€™m not happy with the\r\nfriendships I have', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `options` VALUES ('4', '2', 'I have lots of free time\r\n                to do what I want and\r\n                opportunities to have\r\n                fun', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `options` VALUES ('5', '2', 'I have some time to\r\nmyself to do what I want\r\nand have fun but would\r\nprefer more', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `options` VALUES ('6', '2', 'I have little or no time\r\nfor myself, all my time is\r\ntaken up caring or\r\nwatching over the\r\nperson I care for\r\n', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `options` VALUES ('7', '3', 'I feel healthy, I have a\r\n                healthy lifestyle and a\r\n                balanced diet', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `options` VALUES ('8', '3', 'I have some small health\r\nproblems, would like to\r\nbe fitter and eat a bit\r\nmore healthily', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `options` VALUES ('9', '3', 'I have some health\r\nproblems, I donâ€™t do\r\nmuch exercise, and my\r\ndiet isnâ€™t great', '2018-07-13 15:37:53', '2018-07-13 15:37:53');
INSERT INTO `options` VALUES ('10', '4', 'I have good knowledge\r\n                about these issues', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('11', '4', 'I would like to know\r\nmore about these issues', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('12', '4', 'I donâ€™t know much at all\r\nabout these issues', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('13', '5', 'I do well in school, and\r\n                always manage to do my\r\n                homework on time', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('14', '5', 'I am able to do my\r\nschoolwork and\r\nhomework but it is\r\ndifficult and I would like\r\nto better', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('15', '5', 'I feel like I am not doing\r\nwell at school and find\r\nhomework hard', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('16', '6', 'I never feel bullied\r\n                either in school or\r\n                outside of school', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('17', '6', 'People can be mean to\r\nme sometimes, and\r\nsometimes it makes me\r\nfeel bullied', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('18', '6', 'I feel bullied a lot of the\r\ntime and donâ€™t know\r\nwhat to do about it', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('19', '7', 'I cope well with my\r\n                emotions and\r\n                understand how I feel', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('20', '7', 'Sometimes I find it\r\ndifficult to understand\r\nand control my\r\nemotions, my mood can\r\nchange a lot', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('21', '7', 'I find it difficult to\r\nunderstand my\r\nemotions, I can get\r\nstressed, angry or\r\nanxious easily', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('22', '8', 'Caring is a part of my\r\n                life, but Iâ€™m ok with this,\r\n                and I get to have a break\r\n                to do what I like when I\r\n                need it', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('23', '8', 'Sometimes I get a break\r\nfrom caring and get to\r\ndo what I would like to\r\ndo but I donâ€™t always get\r\nto choose when', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('24', '8', 'Caring stops me doing\r\nthings Iâ€™d like to do', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('25', '9', 'I always feel safe and\r\n                happy at home', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('26', '9', 'I mostly feel happy and\r\nsafe at home\r\n', '2018-07-13 15:37:54', '2018-07-13 15:37:54');
INSERT INTO `options` VALUES ('27', '9', 'I donâ€™t feel safe or\r\nhappy at home', '2018-07-13 15:37:55', '2018-07-13 15:37:55');
INSERT INTO `options` VALUES ('28', '10', 'I can cook and quite\r\n                enjoy it', '2018-07-13 15:37:55', '2018-07-13 15:37:55');
INSERT INTO `options` VALUES ('29', '10', 'I can cook a few things\r\nbut would like to feel\r\nmore confidentI can cook a few things\r\nbut would like to feel\r\nmore confident', '2018-07-13 15:37:55', '2018-07-13 15:37:55');
INSERT INTO `options` VALUES ('30', '10', 'I canâ€™t cook and donâ€™t\r\nknow how to cook', '2018-07-13 15:37:55', '2018-07-13 15:37:55');

-- ----------------------------
-- Table structure for `users_options`
-- ----------------------------
DROP TABLE IF EXISTS `users_options`;
CREATE TABLE `users_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `assesment_id` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_options
-- ----------------------------
INSERT INTO `users_options` VALUES ('1', '1', '1', '1', '2018-07-13 16:28:22', '2018-07-13 16:28:22');
INSERT INTO `users_options` VALUES ('2', '2', '2', '2', '2018-07-13 16:28:22', '2018-07-13 16:28:22');
